package com.ait.forsah.UI.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.Listeners.SpinerCityAndRejone;
import com.ait.forsah.Models.CityModel;
import com.ait.forsah.Models.GetBranchesModel;
import com.ait.forsah.Models.RegionResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GetBranchesAdapter extends RecyclerView.Adapter<GetBranchesAdapter.BranchViewHolder>{

    private Context context;
    private List<GetBranchesModel> list ;
    private SpinerCityAndRejone spinerCityAndRejone;
    private List<CityModel> cityModels;
    LanguagePrefManager languagePrefManager;
    List<Integer> cityId;



    public GetBranchesAdapter(Context context, List<GetBranchesModel> list, List<CityModel> cityModels , List<Integer> cityId, SpinerCityAndRejone spinerCityAndRejone) {
        this.context = context;
        this.list = list;
        this.spinerCityAndRejone=spinerCityAndRejone;
        this.cityModels=cityModels;
        languagePrefManager=new LanguagePrefManager(context);
        this.cityId=cityId;

    }

    @NonNull
    @Override
    public BranchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.branch_edite,parent,false);
        return new BranchViewHolder(view);
    }



    public class BranchViewHolder extends RecyclerView.ViewHolder {

        private TextView spinnerCity;
        private TextView spinnerRegion;
        private TextView editTextStreet;
        private TextView editTextHours;
        private TextView deleteItem;


        public BranchViewHolder(@NonNull View itemView) {
            super(itemView);

           spinnerCity = itemView.findViewById(R.id.spin_register_city);
            spinnerRegion = itemView.findViewById(R.id.spin_register_region);
            editTextStreet = itemView.findViewById(R.id.edt_company_street);
            editTextHours = itemView.findViewById(R.id.edt_company_hours);
           deleteItem= itemView.findViewById(R.id.deleteItem);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull BranchViewHolder holder, int position) {


            GetBranchesModel getBranchesModel = list.get(position);
            Log.e("index",cityModels.get(cityId.get(position)).getName()+"");
            Log.e("index",getBranchesModel.getCity_id()+"");
        Log.e("index",cityId.get(position)+"");

        for (int i =0 ;i<cityModels.size() ;i++){

            if (getBranchesModel.getCity_id()==cityModels.get(i).getId()){
                holder.spinnerCity.setText(cityModels.get(i).getName());
            }
        }
            RetroWeb.getClient().create(ServiceApi.class)
                    .getRegion(getBranchesModel.getCity_id(),languagePrefManager.getAppLanguage()).enqueue(new Callback<RegionResponse>() {
                @Override
                public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body().getKey() == 1){
                            for (int i =0;i<response.body().getRegion().size();i++){
                                if (response.body().getRegion().get(i).getId()==getBranchesModel.getRegion()){
                                    holder.spinnerRegion
                                            .setText(response.body().getRegion().get(i).getName());
                                }
                            }


                        }
                    }
                }
                @Override
                public void onFailure(Call<RegionResponse> call, Throwable t) {
                    Toast.makeText(context, "fail", Toast.LENGTH_SHORT).show();
                }
            });



        holder.editTextStreet.setText(getBranchesModel.getStreet());
        holder.editTextHours.setText(getBranchesModel.getWork_hours()+"");

        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //port num  and city name face to reuse leaner
                spinerCityAndRejone.Rejone(position,0,"");
            }
        });




    }




    @Override
    public int getItemCount() {
//        return list.size();
        return list.size();
    }


    }
