package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.Models.Providers.ProviderAdvertsmentDetailsResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProviderDetailsRefuseActivity extends AppCompatActivity {



    SharedPrefManager mSharedPreferences;
    LanguagePrefManager mLanguagePrefManager;


    private ImageButton imgBack;

    private TextView tvAppBar;

    private TextView tvUserNSme;
    private  TextView tv_refuse_phone;
    private TextView tvHoursePrice;
    private TextView tvAbout;
    private TextView tvAge;
    private TextView tvCity;
    private TextView tvRegion;
    private TextView tvDegree;
    private TextView tvExerience;
    private TextView tvSkills;
    private TextView tvhours;
    private TextView tvWorkDays;
    private TextView tvRefuseDetails;
    private ImageView imgProfile;
    private LinearLayout layoutRefuse;
    private LinearLayout phone;
    private int idNotfy;
    private int id_order;
    private LinearLayout layout_refuse;

    int type;

//    private ImageButton imgButton;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final String MYPREFERENCE = "myPref";
    private static final String USER_ID = "idKey";
    private static final String LANG = "langKey";

    private String idUser;
    private String lang;
    private String jobId;
    private String reason;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_details_refuse);

        type=getIntent().getIntExtra("type",0);
        mSharedPreferences = new SharedPrefManager(this);
        mLanguagePrefManager = new LanguagePrefManager(this);
        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.employee_title);







        Intent intent = getIntent();
        Integer i = intent.getIntExtra("status",0);
       // Integer reason = intent.getIntExtra("Reason",8);
        jobId = intent.getStringExtra("jobId");

        idNotfy= intent.getIntExtra("id_notfy",0);
        id_order= intent.getIntExtra("id_order",0);
    //    int value = i.intValue();
//        int value = Integer.parseInt(jobId);


        phone =findViewById(R.id.laPhone);
        Log.e("phoneProvider",type+"");
        if (type==10||type==9){
            phone.setVisibility(View.VISIBLE);
        }

        tv_refuse_phone=findViewById(R.id.tv_refuse_phone);
        tvUserNSme = findViewById(R.id.tv_refuse_name);
        tvHoursePrice = findViewById(R.id.tv_refuse_hour_price);
        tvAbout = findViewById(R.id.tv_refuse_about);
        tvAge = findViewById(R.id.tv_refuse_age);
        tvCity = findViewById(R.id.tv_refuse_city);
        tvRegion = findViewById(R.id.tv_refuse_region);
        tvDegree = findViewById(R.id.tv_refuse_degree);
        tvExerience = findViewById(R.id.tv_refuse_experience);
        tvSkills = findViewById(R.id.tv_refuse_skill);
        tvhours = findViewById(R.id.tv_refuse_hours);
//        tvWorkDays = findViewById(R.id.tv_refuse_work_days);
        layoutRefuse = findViewById(R.id.layout_refuse);
        tvRefuseDetails = findViewById(R.id.tv_refuse_detail);
        imgProfile = findViewById(R.id.img_refuse);
        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        if (reason == 8 && value == 0){
//            layoutRefuse.setVisibility(View.GONE);
//
//        }else {
//            layoutRefuse.setVisibility(View.VISIBLE);
//
//
//        }


        displayData();
    }

    private void displayData() {
        RetroWeb.getClient().create(ServiceApi.class)
                .getProviderAdvDetail(jobId,mSharedPreferences.getUserData().getId()+"",mLanguagePrefManager.getAppLanguage(),idNotfy,id_order).enqueue(new Callback<ProviderAdvertsmentDetailsResponse>() {
            @Override
            public void onResponse(Call<ProviderAdvertsmentDetailsResponse> call, Response<ProviderAdvertsmentDetailsResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        tvUserNSme.setText(response.body().getResult().getName());
                        tvHoursePrice.setText(response.body().getResult().getExpected_salary());
                        tvAbout.setText(response.body().getResult().getAbout());
                        tvAge.setText(String.valueOf(response.body().getResult().getAge()));
                        tvCity.setText(response.body().getResult().getCity());
                        tvRegion.setText(response.body().getResult().getRegion());
                        tvDegree.setText(response.body().getResult().getQualification());
                        tvExerience.setText(response.body().getResult().getPrevious_experience());
                        tvSkills.setText(response.body().getResult().getEducational_courses());


//                        StringBuilder stringBuilder = new StringBuilder();
//                        for (String day : response.body().getResult().getAvilable_days()){
//                            stringBuilder.append(day + ",");
//                            stringBuilder.lastIndexOf(day);
//                        }
//                        tvWorkDays.setText(stringBuilder);
                        if (response.body().getResult().getReason().equals("")){
                            layoutRefuse.setVisibility(View.GONE);
                            tvhours.setVisibility(View.GONE);
                            tv_refuse_phone.setVisibility(View.GONE);
                        }else {
                            tvRefuseDetails.setText(response.body().getResult().getReason());
                            tvhours.setText(response.body().getResult().getHours_no());
                            tv_refuse_phone.setText(response.body().getResult().getPhone());
                        }
//                        if (response.body().getResult().getReason().equals("")||response.body().getResult().getReason()==null){
//                            layoutRefuse.setVisibility(View.GONE);
//                        }
//                        imgProfile.setImageResource(response.body().getResult().getImg());
//                        imgProfile.setImageDrawable(response.body().getResult().getImg());

                        Picasso.get().load(response.body().getResult().getImg()).into(imgProfile);


                        

                    }else {
                        Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderAdvertsmentDetailsResponse> call, Throwable t) {

            }
        });
    }


}
