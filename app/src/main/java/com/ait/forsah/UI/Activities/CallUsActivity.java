package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.SettingResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallUsActivity extends AppCompatActivity {


    private SharedPreferences sharedPreferences;
    private SharedPrefManager sharedPrefManager;
    private static final String MYPREFERENCE = "myPref";
    private static final String LANG = "langKey";
    private String lang;

    LanguagePrefManager languagePrefManager;

    private Button buttonCallUs;
    private EditText editTextCall;
    private TextView tvAppBar;
    private ImageButton imgBack;
    private ImageButton facebook;
    private ImageButton twitter;
    private ImageButton linkedin;
    private TextView email;
    private TextView number;

    private String facebookUrl;
    private String twitterUrl;
    private String linkedinUrl;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_us);

      languagePrefManager =new LanguagePrefManager(this);
        sharedPrefManager = new SharedPrefManager(this);
        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.title_call_us);
        buttonCallUs = findViewById(R.id.btn_call_us);
        editTextCall = findViewById(R.id.edt_call_us);
        imgBack = findViewById(R.id.img_back);
        facebook = findViewById(R.id.img_facebook);
        twitter = findViewById(R.id.img_twitter);
        linkedin = findViewById(R.id.imag_linkedin);
        email = findViewById(R.id.email);
        number = findViewById(R.id.number);


        contact();

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(facebookUrl));
                startActivity(i);
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(twitterUrl));
                startActivity(i);
            }
        });

        linkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(linkedinUrl));
                startActivity(i);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        buttonCallUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                Integer i = intent.getIntExtra("home", 2);
                int value1 = i.intValue();

                if (checkEditTextError(editTextCall,getString(R.string.requerd),getWindow()))
                {
                    return;
                }

                switch (value1) {
                    case 2:
                        RetroWeb.getClient().create(ServiceApi.class)
                                .contactClient(languagePrefManager.getAppLanguage(),editTextCall.getText().toString(),sharedPrefManager.getUserData().getId()+"").enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                if (response.isSuccessful()) {
                                    if (response.body().getKey() == 1) {
                                        editTextCall.setText("");
                                        Toast.makeText(getApplicationContext(), getString(R.string.sucess),Toast.LENGTH_SHORT).show();
                                        onBackPressed();
                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(),response.body().getMsg(),Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                Log.e("<<<error", "error");
                            }
                        });
                        break;

                    case 3:
                        RetroWeb.getClient().create(ServiceApi.class)
                                .contactProvider(languagePrefManager.getAppLanguage(),editTextCall.getText().toString(),sharedPrefManager.getUserData().getId()+"").enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                if (response.isSuccessful()) {
                                    if (response.body().getKey() == 1) {
                                        editTextCall.setText("");
                                        Toast.makeText(getApplicationContext(),response.body().getMsg(),Toast.LENGTH_SHORT).show();
                                        onBackPressed();
                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(),response.body().getMsg(),Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                Log.e("<<<error", "error");
                            }
                        });
                        break;
                }
            }
        });
    }

    private void contact() {
        RetroWeb.getClient().create(ServiceApi.class).getSetting(null).enqueue(new Callback<SettingResponse>() {
            @Override
            public void onResponse(Call<SettingResponse> call, Response<SettingResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1 ){
                         facebookUrl = response.body().getSetting().getFacebook();
                         twitterUrl = response.body().getSetting().getTwiter();
                         linkedinUrl = response.body().getSetting().getLinkedin();
                         email.setText(response.body().getSetting().getEmail());
                         number.setText(response.body().getSetting().getNumber());
                    }
                }
            }

            @Override
            public void onFailure(Call<SettingResponse> call, Throwable t) {

            }
        });
    }

    public static boolean checkEditTextError(EditText editText, String message, Window window) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError(message);
            if (editText.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return true;
        }
        return false;
    }




}
