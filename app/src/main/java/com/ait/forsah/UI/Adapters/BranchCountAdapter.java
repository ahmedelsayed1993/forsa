package com.ait.forsah.UI.Adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.Listeners.SpinerCityAndRejone;
import com.ait.forsah.Models.BranchCountModel;

import com.ait.forsah.R;


import java.util.ArrayList;
import java.util.List;


public class BranchCountAdapter extends RecyclerView.Adapter<BranchCountAdapter.BranchViewHolder>{

    private Context context;
    private List<BranchCountModel> list ;
    private SpinerCityAndRejone spinerCityAndRejone;




    public BranchCountAdapter(Context context, List<BranchCountModel> list, SpinerCityAndRejone spinerCityAndRejone) {
        this.context = context;
        this.list = list;
        this.spinerCityAndRejone=spinerCityAndRejone;

    }

    @NonNull
    @Override
    public BranchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.branch_item,parent,false);
        return new BranchViewHolder(view);
    }



    public class BranchViewHolder extends RecyclerView.ViewHolder {

        private Spinner spinnerCity;
        private Spinner spinnerRegion;
        private EditText editTextStreet;
        private EditText editTextHours;


        public BranchViewHolder(@NonNull View itemView) {
            super(itemView);

           spinnerCity = itemView.findViewById(R.id.spin_register_city);
            spinnerRegion = itemView.findViewById(R.id.spin_register_region);
            editTextStreet = itemView.findViewById(R.id.edt_company_street);
            editTextHours = itemView.findViewById(R.id.edt_company_hours);

        }
    }


    @Override
    public void onBindViewHolder(@NonNull BranchViewHolder holder, int position) {

        BranchCountModel model = list.get(position);
        holder.spinnerCity.getSelectedItem();
        holder.editTextHours.setText(list.get(position).getWorkHours());
        holder.editTextStreet.setText(list.get(position).getStreet());

        final ArrayList<String> regionName = new ArrayList<>();
        if (model.getCityName()!=null)
        regionName.add(model.getCityName());
        else{
            regionName.add(context.getString(R.string.city));
        }
        for (int i=0;i<model.getCitySpinner().size();i++){
            regionName.add(model.getCitySpinner().get(i));

        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_activated_1,regionName){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }
        };
        holder.spinnerCity.setAdapter(adapter);
        if (model.getCityName()!=null){
            holder.spinnerCity.setPrompt(model.getCityName());

        }
        holder.spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int positionItem, long l) {
                if (positionItem>0){
                    list.get(position).setCitid(model.getCityId().get(positionItem-1));
                  spinerCityAndRejone.Rejone(model.getCityId().get(positionItem-1),position,regionName.get(positionItem));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (model.getRegionSpinner()!=null) {
            ArrayAdapter<String> adapterRejon = new ArrayAdapter<String>(context,
                    android.R.layout.simple_list_item_activated_1, model.getRegionSpinner()) {
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        // Disable the first item from Spinner
                        // First item will be use for hint
                        return false;
                    } else {
                        return true;
                    }
                }
            };
            holder.spinnerRegion.setAdapter(adapterRejon);
            holder.spinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int positionItem, long l) {
                    if (positionItem > 0) {
                        list.get(position).setRejonid(model.getRjonId().get(positionItem-1));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            holder.editTextStreet.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    list.get(position).setStreet(holder.editTextStreet.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        holder.editTextHours.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                list.get(position).setWorkHours(holder.editTextHours.getText().toString());
//                Log.e("list", list.get(position).getStreet());
//                Log.e("list", list.get(position).getWorkHours());

            }

            @Override
            public void afterTextChanged(Editable s) {



            }
        });
    }




    @Override
    public int getItemCount() {
//        return list.size();
        return list.size();
    }


    }
