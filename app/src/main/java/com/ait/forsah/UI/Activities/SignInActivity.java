package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Models.LoginResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.SharedPrefManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    private static final String MYPREFERENCE = "myPref";
    private static final String LOG_IN = "loginKey";
    private static final String USER_ID = "idKey";
    private static final String USER_IMAGE = "imageKey";
    private static final String USER_NAME = "nameKey";
    private static final String USER_TYPE = "userKey";
    private static final String BRANCH_COUNT = "branchKey";



    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private TextView textViewNewSignIn;
    private TextView textViewForgetPassword;
    private EditText editTextUser;
    private EditText editTextPassword;
    private Button buttonLogin;
    String mDeviceId;

    private RelativeLayout relativeLayout;

    private final boolean user_type = true;


    SharedPrefManager mSharedPreferences;
    protected SharedPrefManager mEditor;


    private AVLoadingIndicatorView avi;

    int value;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        mDeviceId= FirebaseInstanceId.getInstance().getToken();
        mSharedPreferences = new SharedPrefManager(this);
        mEditor=new SharedPrefManager(this);
        Intent intent = getIntent();
       value = intent.getIntExtra("signup",1);

        sharedPreferences = getSharedPreferences(MYPREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();


        relativeLayout = findViewById(R.id.relative_layout);
        buttonLogin = findViewById(R.id.btn_log_in);
        textViewNewSignIn = findViewById(R.id.tv_new_signIn);
        textViewForgetPassword = findViewById(R.id.tv_forget_password);
        editTextUser = findViewById(R.id.edt_sign_in_user_name);
        editTextPassword = findViewById(R.id.edt_sign_in_password);

        final String indicator=getIntent().getStringExtra("indicator");
        avi= findViewById(R.id.avi);
        avi.setIndicator(indicator);


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logIn();
            }
        });

        textViewNewSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                textViewNewSignIn.setClickable(false);
                if (value == 1){
                    startActivity(new Intent(SignInActivity.this,RegisterAsSeekerActivity.class));
                }else {
                    startActivity(new Intent(SignInActivity.this,RegisterAsProviderActivity.class));
                }
            }
        });

        textViewForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignInActivity.this,ResetPasswordActivity.class);
                i.putExtra("type",value);
                startActivity(i);
            }
        });
    }

    private void logIn() {
        if (checkEditTextError(editTextUser,getString(R.string.requerd),getWindow())||
                (checkEditTextError(editTextPassword,getString(R.string.requerd),getWindow())))
        {
            return;
        }else
            {
                if (isNetworkAvailable()){


                    relativeLayout.setVisibility(View.INVISIBLE);
                    avi.smoothToShow();
                    RetroWeb.getClient().create(ServiceApi.class)
                            .login(editTextUser.getText().toString(),editTextPassword.getText().toString(),user_type,mDeviceId)
                            .enqueue(new Callback<LoginResponse>() {
                                @Override
                                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                                    if (response.isSuccessful()){
                                        if (response.body().getKey()==1){
                                            avi.smoothToHide();

                                            if (value==1) {
                                                if (response.body().getClient()!=null){
                                                    mSharedPreferences.setLoginStatus(true);
                                                    mSharedPreferences.setUserData(response.body().getClient());

                                                    Intent i = new Intent(SignInActivity.this, HomeSeekerActivity.class);
                                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(i);

                                                    Toast.makeText(SignInActivity.this, getString(R.string.logINSuccess), Toast.LENGTH_SHORT).show();
                                                    finish();
                                                }else {
                                                    relativeLayout.setVisibility(View.VISIBLE);
                                                    CommonUtil.makeToast(SignInActivity.this, getString(R.string.notfoundAcount));

                                                }
                                            }
                                            else if (value==2) {
                                                if (response.body().getProvider()!=null){
                                                    mSharedPreferences.setLoginStatus(true);
                                                    mSharedPreferences.setUserData(response.body().getProvider());
                                                    Intent i = new Intent(SignInActivity.this, HomeActivity.class);
                                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(i);

                                                    Toast.makeText(SignInActivity.this, getString(R.string.logINSuccess), Toast.LENGTH_SHORT).show();
                                                    finish();

                                                }else {
                                                    relativeLayout.setVisibility(View.VISIBLE);
                                                    CommonUtil.makeToast(SignInActivity.this, getString(R.string.notfoundAcount));


                                                }
                                              //  startActivity(new Intent(SignInActivity.this,HomeActivity.class));
                                            }







                                        }else {
                                            relativeLayout.setVisibility(View.VISIBLE);
                                            avi.smoothToHide();
                                            Toast.makeText(SignInActivity.this, getString(R.string.errorLang), Toast.LENGTH_SHORT).show();

                                        }
                                    }else {
                                        Toast.makeText(SignInActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<LoginResponse> call, Throwable t) {

                                }
                            });
                }else {
                    Toast.makeText(this, "There is no network..", Toast.LENGTH_SHORT).show();
                }
                    
                }
               
    }

    public static boolean checkEditTextError(EditText editText, String message, Window window) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError(message);
            if (editText.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return true;
        }
        return false;
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();

    }

    @Override
    protected void onResume() {
        super.onResume();
        textViewNewSignIn.setClickable(true);


    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}