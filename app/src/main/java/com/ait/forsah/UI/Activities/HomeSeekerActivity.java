package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.Clients.ClientGetOrderModel;
import com.ait.forsah.Models.Clients.ClientGetOrderResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.UI.Adapters.SeekerSearchAdapter;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeSeekerActivity extends AppCompatActivity {


    private TextView textViewProfile;

    private TextView textViewMakeads;

    private TextView textViewMyAds;

    private TextView textViewCall;

    private TextView textViewPolicy;

    private TextView textViewAbout;

    private TextView textViewLogout;

    private TextView tv_home_seeker_name;

    private CircleImageView imageView;

    Dialog myDialog;

    private RecyclerView recyclerView;

    private TextView tvResultData;

    private SeekerSearchAdapter adapter;

    private RelativeLayout relativeLayout;


    private ImageButton imageButtonMenu;

    List<ClientGetOrderModel> list = new ArrayList<>();

    private static final String MYPREFERENCE = "myPref";

    private static final String LOG_IN = "loginKey";

    private static final String USER_ID = "idKey";

    private static final String LANG = "langKey";

    private ImageButton imgNotification;


    SharedPrefManager mSharedPreferences;

    LanguagePrefManager mLanguagePrefManager;


    private String id;

    private String user;

    private String imageUrl;

    private String lang;

    private TextView textView;

    private Button btnEdit;

    private Button btnExit;

    private TextView notficationCont;


    SwipeRefreshLayout swipeAds;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_seeker);



        mSharedPreferences = new SharedPrefManager(this);

        mLanguagePrefManager = new LanguagePrefManager(this);

        tv_home_seeker_name=findViewById(R.id.tv_home_seeker_name);

        myDialog = new Dialog(this);

        imgNotification = findViewById(R.id.imgNotification);

        relativeLayout = findViewById(R.id.seeker_liner_result);

        tvResultData = findViewById(R.id.tv_result__seeker_data);

        recyclerView = findViewById(R.id.recycler_search_seeker_result);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));

        imageView = findViewById(R.id.img_seeker_Profile);

        textViewProfile = findViewById(R.id.tv_seeker_profile);

        textViewMakeads = findViewById(R.id.tv_seeker_make_ads);

        textViewMyAds = findViewById(R.id.tv_seeker_myads);

        textViewCall = findViewById(R.id.tv_seeker_call);

        textViewPolicy = findViewById(R.id.tv_seeker_policy);

        textViewAbout = findViewById(R.id.tv_seeker_about);

        textViewLogout = findViewById(R.id.tv_seeker_Logout);


        notficationCont=  findViewById(R.id.notficationCont);



        getCount();


        imageUrl=mSharedPreferences.getUserData().getImg();

        tv_home_seeker_name.setText(mSharedPreferences.getUserData().getName());

        Picasso.get().load(imageUrl).into(imageView);


        final DrawerLayout drawer2 = findViewById(R.id.drawer_layout_seeker);

        final NavigationView navigationView = findViewById(R.id.nav_view_seeker);

        imageButtonMenu = findViewById(R.id.img_menu);

        imageButtonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer2.openDrawer(GravityCompat.START);

            }
        });


        imgNotification.setOnClickListener(view -> {

            Intent intent = new Intent(HomeSeekerActivity.this, NotificationActivity.class);

            intent.putExtra("type", "client");

            startActivity(intent);

        });

        textViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(HomeSeekerActivity.this, SeekerProfileActivity.class));

                drawer2.closeDrawer(GravityCompat.START);


            }
        });

        textViewMakeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(HomeSeekerActivity.this, AddJobActivity.class));

                drawer2.closeDrawer(GravityCompat.START);


            }
        });

        textViewMyAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(HomeSeekerActivity.this, MyAdvertiseMeantActivity.class);

                startActivity(i);

                drawer2.closeDrawer(GravityCompat.START);
            }
        });

        textViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(HomeSeekerActivity.this, CallUsActivity.class);

                i.putExtra("home", 2);

                startActivity(i);

                drawer2.closeDrawer(GravityCompat.START);

            }
        });

        textViewPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(HomeSeekerActivity.this, PolicyActivity.class);

                i.putExtra("home", 2);

                startActivity(i);

                drawer2.closeDrawer(GravityCompat.START);


            }
        });

        textViewAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(HomeSeekerActivity.this, AboutActivity.class);

                i.putExtra("home", 2);

                startActivity(i);

                drawer2.closeDrawer(GravityCompat.START);

            }
        });

        textViewLogout.setOnClickListener(view -> {


            myDialog.setContentView(R.layout.pop_layout_delet);

            textView = myDialog.findViewById(R.id.tv_hint);

            btnEdit = myDialog.findViewById(R.id.btn_stay);

            btnExit = myDialog.findViewById(R.id.btn_exit);

            btnEdit.setText(this.getString(R.string.exit));

            textView.setText(this.getString(R.string.tostLogOut));

            btnExit.setText(this.getString(R.string.cancel));

            myDialog.show();

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    RetroWeb.getClient().create(ServiceApi.class)
                            .logoutClient(mSharedPreferences.getUserData().getId() + "").enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()) {
                                if (response.body().getKey() == 1) {

                                    mSharedPreferences.setLoginStatus(false);

                                    startActivity(new Intent(HomeSeekerActivity.this, SignInActivity.class));
                                    Toast.makeText(HomeSeekerActivity.this, getString(R.string.logOut), Toast.LENGTH_SHORT).show();

                                    finishAffinity();

                                } else {
                                    Toast.makeText(HomeSeekerActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                        }
                    });

                }
            });

            btnExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    myDialog.dismiss();
                }
            });

        });


        displayResult();


    }


    private void getCount(){

        RetroWeb.getClient().create(ServiceApi.class).
                GetNotifyByClient(mSharedPreferences.getUserData().getId()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {
                        notficationCont.setText(String.valueOf(response.body().getCountNotify()));
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
            }
        });
    }

    private void displayResult() {
        RetroWeb.getClient().create(ServiceApi.class).getClientOrder(mSharedPreferences.getUserData().getId() + "", mLanguagePrefManager.getAppLanguage())
                .enqueue(new Callback<ClientGetOrderResponse>() {
                    @Override
                    public void onResponse(Call<ClientGetOrderResponse> call, Response<ClientGetOrderResponse> response) {



                        if (response.isSuccessful()) {
                            if (response.body().getKey() == 1) {

                                list = response.body().getResult();

                                if (list.size() != 0) {
                                    relativeLayout.setVisibility(View.GONE);


                                    adapter = new SeekerSearchAdapter(HomeSeekerActivity.this,list);

                                    recyclerView.setAdapter(adapter);


                                }
                                else{

                                    relativeLayout.setVisibility(View.VISIBLE);

                                    tvResultData.setText(response.body().getMsg());

                                }

                            } else {
                                Toast.makeText(HomeSeekerActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(HomeSeekerActivity.this, R.string.error + "key Not Equal 1", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ClientGetOrderResponse> call, Throwable t) {


                    }
                });
    }


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = findViewById(R.id.drawer_layout_seeker);

        if (drawer.isDrawerOpen(Gravity.RIGHT)) {

            drawer.closeDrawer(Gravity.RIGHT);

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_seeker, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_seeker_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (list.size()!=0) {
            list.clear();
            adapter.notifyDataSetChanged();

            displayResult();
        }
        tv_home_seeker_name.setText(mSharedPreferences.getUserData().getName());

        Picasso.get().load(imageUrl).into(imageView);

    }


    @Override
    protected void onResume() {
        super.onResume();
        getCount();
//        list.clear();
//        adapter.notifyDataSetChanged();
//        displayResult();
    }



}
