package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;

import java.util.Locale;

public class ChangeLanguageActivity extends AppCompatActivity {

    private static final String MYPREFERENCE = "myPref";
    private static final String LANG = "langKey";

    protected LanguagePrefManager mLanguagePrefManager;

    SharedPrefManager mSharedPreferences;
    protected SharedPrefManager mEditor;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private Button btnArabic;
    private Button btnEnglish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        mLanguagePrefManager = new LanguagePrefManager(this);


//        sharedPreferences = getSharedPreferences(MYPREFERENCE, Context.MODE_PRIVATE);
//        editor = sharedPreferences.edit();


        mSharedPreferences = new SharedPrefManager(this);
        mEditor = new SharedPrefManager(this);


        btnArabic = findViewById(R.id.btn_arabic);
        btnEnglish = findViewById(R.id.btn_english);

        btnArabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLanguagePrefManager.setAppLanguage("ar");
                CommonUtil.setConfig(mLanguagePrefManager.getAppLanguage(), ChangeLanguageActivity.this);
                checkFlow();



            }
        });

        btnEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLanguagePrefManager.setAppLanguage("en");
                CommonUtil.setConfig(mLanguagePrefManager.getAppLanguage(), ChangeLanguageActivity.this);
                checkFlow();
                finish();


            }
        });

    }



    private void checkFlow(){

        if (mSharedPreferences.getLoginStatus()){
            if (mSharedPreferences.getUserData().isProvidor()){
                startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                finish();
            }   else {
                startActivity(new Intent(getApplicationContext(),HomeSeekerActivity.class));
                finish();
            }
        }else {
            startActivity(new Intent(getApplicationContext(),SignupActivity.class));
            finish();

        }
    }


    public void setLocale(String lang) {
        Log.e("kkk", lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(lang)); // API 17+ only.
        res.updateConfiguration(conf, dm);
        //   Log.e("RESDSDSD" , reg);
    }


}
