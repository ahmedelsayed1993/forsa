package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutActivity extends AppCompatActivity {

    private static final String MYPREFERENCE = "myPref";
    private static final String LANG = "langKey";
    private String lang;
    LanguagePrefManager languagePrefManager;
    private TextView textViewAbout;
    private TextView tvAppBar;
    private ImageButton imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);


        languagePrefManager =new LanguagePrefManager(this);

        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.title_about);
        textViewAbout = findViewById(R.id.tv_about);
        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        displayAbout();
    }


    private void displayAbout() {

        Intent intent = getIntent();
        final Integer i = intent.getIntExtra("home", 2);
        int value1 = i.intValue();
        switch (value1){
            case 2:
                RetroWeb.getClient().create(ServiceApi.class)
                        .getAboutUsForClient(languagePrefManager.getAppLanguage()).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful()){
                            if (response.body().getKey() ==1){
                                Log.d("TAG", "This is Client");
                                textViewAbout.setText(response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                    }
                });
                break;

            case 3:
                RetroWeb.getClient().create(ServiceApi.class)
                        .getAboutForProvider(languagePrefManager.getAppLanguage()).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful()){
                            if (response.body().getKey() == 1){
                                Log.d("TAG", "This is provider");
                                textViewAbout.setText(response.body().getMsg());
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        Log.e("<<<error", "error");
                    }
                });
                break;

        }
    }
}
