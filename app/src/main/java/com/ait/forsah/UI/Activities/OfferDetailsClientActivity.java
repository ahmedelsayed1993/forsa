package com.ait.forsah.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.Clients.ClientOrderDetailsModel;
import com.ait.forsah.Models.Clients.ClientOrderDetailsResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.base.ParentActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OfferDetailsClientActivity extends ParentActivity {


    @BindView(R.id.tv_company_activity)
    TextView  tv_company_activity ;


    @BindView(R.id.tv_name)
    TextView  tv_name ;


    @BindView(R.id.hoursNumbers)
    TextView  hoursNumbers ;


    @BindView(R.id.numberBranches)
    TextView  numberBranches ;


    @BindView(R.id.tv_address_head)
    TextView  tv_address_head ;


    @BindView(R.id.tv_region)
    TextView  tv_region;



    @BindView(R.id.tv_street)
    TextView tv_street ;


    @BindView(R.id.tv_hours)
    TextView tv_hours ;


    @BindView(R.id.tv_details)
    TextView  tv_details ;

    @BindView(R.id.btnAcceptance)
    Button btnAcceptance;


    @BindView(R.id.btnPay)
    Button btnPay;

    @BindView(R.id.btnRefuse)
    Button btnRefuse;

    @BindView(R.id.tv_app_bar)
    TextView  tv_app_bar ;

    @BindView(R.id.tv_phone)
    TextView  tvPhone ;


    @BindView(R.id.la_phone)
    LinearLayout laPhone;



    @BindView(R.id.img_profile)
    CircleImageView img_profile;

    ClientOrderDetailsModel mResult;



    private int orderId;



    @Override
    protected void initializeComponents() {
        laPhone.setVisibility(View.GONE);

        orderId=getIntent().getIntExtra("orderId",0);
        tv_app_bar.setText(getString(R.string.infoCompany));


        getAdvetisement();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_offer_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    @OnClick(R.id.img_back)
    void back(){
        onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();

        getAdvetisement();
    }

    private void getAdvetisement() {
        RetroWeb.getClient().create(ServiceApi.class)
                .getOrderDetails(orderId+"",mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ClientOrderDetailsResponse>() {
            @Override
            public void onResponse(Call<ClientOrderDetailsResponse> call, Response<ClientOrderDetailsResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){

                        mResult =response.body().getResult();
                        setDetailes();

                    }else {
                        Toast.makeText(OfferDetailsClientActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(OfferDetailsClientActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ClientOrderDetailsResponse> call, Throwable t) {
                Toast.makeText(OfferDetailsClientActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
            }
        });
    }



    private void setDetailes(){
        tv_name.setText(mResult.getName());
        tv_company_activity.setText((mResult.getActivity()));
        tv_details.setText(mResult.getAbout());
        tv_address_head.setText(mResult.getCity());
        tv_region.setText(mResult.getRegion());
        numberBranches.setText(mResult.getBranch_count()+"");
        tv_street.setText(mResult.getStreet_branch());
        tv_hours.setText(mResult.getWork_hours());
        hoursNumbers.setText(mResult.getWork_hours());
        tvPhone.setText(mResult.getPhone());

        switch (mResult.getStutes()){


            case 0 :
                btnAcceptance.setVisibility(View.VISIBLE);
                btnRefuse.setVisibility(View.VISIBLE);
                break;

            case 1 :
                btnPay.setVisibility(View.VISIBLE);
                break;
            case 2 :
                btnAcceptance.setVisibility(View.GONE);
                btnRefuse.setVisibility(View.GONE);
                laPhone.setVisibility(View.VISIBLE);



                default:
                    laPhone.setVisibility(View.VISIBLE);

                    break;
        }
        Picasso.get()
                .load(mResult.getImg())
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .into(img_profile);
    }


    @OnClick(R.id.btnRefuse)
    void refuse(){
        Intent intent =new Intent(mContext,ReasonRefuseActivity.class);
        intent.putExtra("orderId",orderId);
        startActivity(intent);
    }

    @OnClick(R.id.btnAcceptance)
    void BtnAcceptance(){
        RetroWeb.getClient().create(ServiceApi.class).changeStutes(orderId+"","1","",mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {
                        CommonUtil.makeToast(OfferDetailsClientActivity.this,response.body().getMsg());
                        Intent intent =new Intent(mContext,OrderAcceptActivity.class);
                        intent.putExtra("orderId",orderId+"");
                        intent.putExtra("phone",mResult.getPhone());

                        Log.e("btnAcceptance",orderId+"");
                        startActivity(intent);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.btnPay)
    void  btnPay(){

            Intent intent = new Intent(OfferDetailsClientActivity.this,BankTransferActivity.class);
            intent.putExtra("orderId",orderId+"");
            intent.putExtra("phone",mResult.getPhone());
            intent.putExtra("fk_job",mResult.getFk_job());

            startActivity(intent);
            finish();
    }
}
