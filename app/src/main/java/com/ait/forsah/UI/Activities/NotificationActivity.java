package com.ait.forsah.UI.Activities;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.Listeners.OnItemClickListener;
import com.ait.forsah.Models.Clients.ClientRegisterResponse;
import com.ait.forsah.Models.Providers.ProviderNotificationModel;
import com.ait.forsah.Models.Providers.ProviderNotificationResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.UI.Adapters.NotificationAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.ACTION_DIAL;

public class NotificationActivity extends AppCompatActivity  implements OnItemClickListener {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final String MYPREFERENCE = "myPref";
    private static final String USER_ID = "idKey";
    private static final String LANG = "langKey";
    private static final String JOB_ID = "jobKey";
    Dialog myDialog;

    private RecyclerView recyclerView;
    private NotificationAdapter notificationAdapter;
    private ArrayList<ProviderNotificationModel> list;



    private String id;
    private String lang;


    private List<ProviderNotificationModel> notify;

    SharedPrefManager mSharedPreferences;
    LanguagePrefManager mLanguagePrefManager;

    private AVLoadingIndicatorView avi;

    private  String type;

    private ImageButton imageButtonBack;
    private TextView tvAppBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        mSharedPreferences = new SharedPrefManager(this);
        mLanguagePrefManager= new LanguagePrefManager(this);

        sharedPreferences = getSharedPreferences(MYPREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        id = mSharedPreferences.getUserData().getId()+"";

        notify=new ArrayList<>();

        type = getIntent().getStringExtra("type");
        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.title_notfi);
        final String indicator=getIntent().getStringExtra("indicator");
        avi= findViewById(R.id.avi);
        avi.setIndicator(indicator);


        recyclerView = findViewById(R.id.rc_notification);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false));

        imageButtonBack = findViewById(R.id.img_back);
        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             onBackPressed();
            }
        });

        if (type.equals("provider")){
            getNotifyProvider();
        }else  if (type.equals("client")){
            getNotifyClient();

        }


    }

    private void getNotifyProvider() {
        avi.smoothToShow();
        RetroWeb.getClient().create(ServiceApi.class).getProviderNotification(id,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ProviderNotificationResponse>() {
            @Override
            public void onResponse(Call<ProviderNotificationResponse> call, Response<ProviderNotificationResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        avi.smoothToHide();
                        notify=response.body().getNotify();
                        notificationAdapter = new NotificationAdapter(NotificationActivity.this,notify,type,NotificationActivity.this);
                        recyclerView.setAdapter(notificationAdapter);
                    }else {
                        Toast.makeText(NotificationActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(NotificationActivity.this, R.string.error, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ProviderNotificationResponse> call, Throwable t) {

            }
        });
    }




    private void getNotifyClient() {
        avi.smoothToShow();
        RetroWeb.getClient().create(ServiceApi.class).getNotifaction(id,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ProviderNotificationResponse>() {
            @Override
            public void onResponse(Call<ProviderNotificationResponse> call, Response<ProviderNotificationResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        avi.smoothToHide();
                        notify=response.body().getNotify();
                        notificationAdapter = new NotificationAdapter(NotificationActivity.this,notify,type,NotificationActivity.this);
                        recyclerView.setAdapter(notificationAdapter);
                    }else {
                        Toast.makeText(NotificationActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(NotificationActivity.this, R.string.error, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ProviderNotificationResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

        getUserData(notify.get(position).getClientid());
    }




    private void dialogPhone(String phone,String name){
        myDialog = new Dialog(this);

        myDialog.setContentView(R.layout.dialog_phone);
        myDialog.setCancelable(true);
        TextView tvPhone = myDialog.findViewById(R.id.tv_phone);

        TextView tvNameHint = myDialog.findViewById(R.id.tvNameHint);
        TextView tvName = myDialog.findViewById(R.id.tvName);
        Button btnBackToHome = myDialog.findViewById(R.id.btn_exit);
        Button btnCall = myDialog.findViewById(R.id.btn_call_us);

        btnCall.setVisibility(View.VISIBLE);
        tvNameHint.setVisibility(View.VISIBLE);
        tvName.setVisibility(View.VISIBLE);

        btnBackToHome.setVisibility(View.GONE);


        tvName.setText(name);

        tvPhone.setText(phone);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ACTION_DIAL , Uri.parse("tel:" + tvPhone.getText().toString()));
                startActivity(intent);
            }
        });


        btnBackToHome.setOnClickListener(view -> {
            Intent i = new Intent(NotificationActivity.this, HomeSeekerActivity.class);
            startActivity(i);
            finish();
        });
        myDialog.show();


    }


    private void getUserData(int userId) {

        RetroWeb.getClient().create(ServiceApi.class)
                .getUserData(userId,mLanguagePrefManager.getAppLanguage())
                .enqueue(new Callback<ClientRegisterResponse>() {
                    @Override
                    public void onResponse(Call<ClientRegisterResponse> call, Response<ClientRegisterResponse> response) {


                        if (response.isSuccessful()) {

                            if (response.body().getKey()==1){


                                dialogPhone(response.body().getClient().getPhone(),response.body().getClient().getName());
                                //displayJobTitle();

                            }

                        }


                    }

                    @Override
                    public void onFailure(Call<ClientRegisterResponse> call, Throwable t) {

                    }
                });
    }


}


