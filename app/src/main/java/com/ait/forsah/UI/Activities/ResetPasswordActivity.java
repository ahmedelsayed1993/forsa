package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AppCompatActivity {

    private Button buttonResendPhone;
    private EditText editTextResendPhone;
    private SharedPreferences sharedPreferences;
    private static final String MYPREFERENCE = "myPref";
    private static final String LANG = "langKey";
    private String lang;
    private int value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        Intent intent = getIntent();

        value = intent.getIntExtra("type",1);

        sharedPreferences = getSharedPreferences(MYPREFERENCE, Context.MODE_PRIVATE);
        lang = sharedPreferences.getString(LANG,null);

        buttonResendPhone = findViewById(R.id.btn_resend_phone);
        editTextResendPhone = findViewById(R.id.edt_resend_phone);

        buttonResendPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (value == 1){
                    resendPasswordSeeker();
                }else {
                    resendPassword();
                }
//                Intent i = new Intent(ResetPasswordActivity.this,ResetPassword2Activity.class);
//                startActivity(i);

            }
        });
    }

    private void resendPassword() {
        if (checkEditTextError(editTextResendPhone,getString(R.string.requerd),getWindow())){
            return;
        }else {
            RetroWeb.getClient().create(ServiceApi.class)
                    .reSendCode(editTextResendPhone.getText().toString(),lang)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()){
                                if (response.body().getKey() == 1){
                                    Toast.makeText(ResetPasswordActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                    int code = response.body().getCode();
                                    Intent i = new Intent(ResetPasswordActivity.this,ResetPassword2Activity.class);
                                    i.putExtra("type",value);
                                    i.putExtra("code",code);
                                    startActivity(i);
                                    finish();
                                }else {
                                    Toast.makeText(ResetPasswordActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(ResetPasswordActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                        }
                    });
        }
    }


    private void resendPasswordSeeker() {
        if (checkEditTextError(editTextResendPhone,getString(R.string.requerd),getWindow())){
            return;
        }else {
            RetroWeb.getClient().create(ServiceApi.class)
                    .reSendCodeSecker(editTextResendPhone.getText().toString(),lang)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()){
                                if (response.body().getKey() == 1){
                                    Toast.makeText(ResetPasswordActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                    int code = response.body().getCode();
                                    Intent i = new Intent(ResetPasswordActivity.this,ResetPassword2Activity.class);
                                    i.putExtra("code",code);
                                    startActivity(i);
                                    finish();
                                }else {
                                    Toast.makeText(ResetPasswordActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(ResetPasswordActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                        }
                    });
        }
    }


    public static boolean checkEditTextError(EditText editText, String message, Window window) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError(message);
            if (editText.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return true;
        }
        return false;
    }

}
