package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.ValidationUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReasonRefuseActivity extends AppCompatActivity {


    private TextView tvAppBar;
    private ImageButton imgBack;
    private Button btnSent;
    private int orderId;

    private EditText etReason;
    LanguagePrefManager languagePrefManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reason_refuse);

        languagePrefManager=new LanguagePrefManager(this);
        orderId=getIntent().getIntExtra("orderId",0);
        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.title_refuse);
        imgBack = findViewById(R.id.img_back);
        btnSent=findViewById(R.id.btn_sent);
        etReason= findViewById(R.id.etResons);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });

        btnSent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ValidationUtils.emptyValidation(etReason,getString(R.string.reasons))) {
                    btnSent.setClickable(false);
                    RetroWeb.getClient().create(ServiceApi.class).changeStutes(orderId+"","2",etReason.getText().toString(),languagePrefManager.getAppLanguage()).enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                            if (response.isSuccessful()) {
                                if (response.body().getKey() == 1) {
                                    CommonUtil.makeToast(ReasonRefuseActivity.this,response.body().getMsg());
                                    finish();
                                }else {
                                    btnSent.setClickable(true);

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            btnSent.setClickable(true);
                        }
                    });

                }
            }
        });


    }
}
