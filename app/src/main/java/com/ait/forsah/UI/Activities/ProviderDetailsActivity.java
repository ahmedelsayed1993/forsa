package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.Providers.ProviderAdvertsmentDetailsResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.squareup.picasso.Picasso;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProviderDetailsActivity extends AppCompatActivity {

    private TextView tvAppBar;

    private ImageButton imgBack;


    private TextView tvUserNSme;
    private TextView tvHoursePrice;
    private TextView tvAbout;
    private TextView tvAge;
    private TextView tvCity;
    private TextView tvRegion;
    private TextView tvDegree;
    private TextView tvExerience;
    private TextView tvSkills;
    private TextView tvhours;
//    private TextView tvWorkDays;
    private ImageView imgProfile;
    private LinearLayout linearLayout;

    private Button btnRequest;


    private static final String MYPREFERENCE = "myPref";
    private static final String USER_ID = "idKey";
    private static final String LANG = "langKey";
    private String lang;
    private String idUser;
    private String jobId;

    private TextView tvHint;
    private TextView tv_salary;
    private TextView tv_days;
    private TextView available_days;


    SharedPrefManager mSharedPreferences;
    LanguagePrefManager mLanguagePrefManager;
    private int idNotfy;
    private int id_order;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_details);


        mSharedPreferences = new SharedPrefManager(this);
        mLanguagePrefManager = new LanguagePrefManager(this);

        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.searcher_details);

        tvHint=findViewById(R.id.tv_hint);
        tvHint.setText(getString(R.string.number_of_work_times));
        available_days = findViewById(R.id.available_days);

        lang = mLanguagePrefManager.getAppLanguage();

        idUser = mSharedPreferences.getUserData().getId()+"";
        Intent intent = getIntent();
        Integer i = intent.getIntExtra("status", 1);
        int value = i.intValue();


        jobId = intent.getStringExtra("jobId");
        Log.e("jobId",jobId);

        idNotfy= intent.getIntExtra("id_notfy",0);
        id_order= intent.getIntExtra("id_order",0);

        tvUserNSme = findViewById(R.id.tv_name);
        tvHoursePrice = findViewById(R.id.tv_hour_price);
        tvAbout = findViewById(R.id.tv_accept_about_seeker);
        tvAge = findViewById(R.id.tv_age);
        tvCity = findViewById(R.id.tv_city);
        tvRegion = findViewById(R.id.tv_region);
        tvDegree = findViewById(R.id.tv_degree);
        tvExerience = findViewById(R.id.tv_experience);
        tv_salary = findViewById(R.id.tv_salary);
        tvSkills = findViewById(R.id.tv_skill);
        tvhours = findViewById(R.id.tv_hours);
//        tvWorkDays = findViewById(R.id.tv_work_days);
        imgProfile = findViewById(R.id.img_seeker);
//        linearLayout = findViewById(R.id.liner_phone);
        imgBack = findViewById(R.id.img_back);
        tv_days = findViewById(R.id.tv_days);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnRequest = findViewById(R.id.button_request);
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnRequest.setClickable(false);

                RetroWeb.getClient().create(ServiceApi.class)
                        .addOrder(jobId, idUser,lang).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        btnRequest.setClickable(true);

                        if (response.isSuccessful()) {
                            if (response.body().getKey() == 1) {
                                startActivity(new Intent(getApplicationContext(), OrderSentActivity.class));
                                Toast.makeText(ProviderDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ProviderDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ProviderDetailsActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        btnRequest.setClickable(true);

                    }
                });
            }
        });

        displayData();
    }


    private void displayData() {

        RetroWeb.getClient().create(ServiceApi.class)
                .getProviderAdvDetail(jobId, idUser,lang,idNotfy,id_order).enqueue(new Callback<ProviderAdvertsmentDetailsResponse>() {
            @Override
            public void onResponse(Call<ProviderAdvertsmentDetailsResponse> call, Response<ProviderAdvertsmentDetailsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {
                        response.body().getResult().getId_job();
                        tvUserNSme.setText(response.body().getResult().getUser_name());
                        tvHoursePrice.setText(response.body().getResult().getHours_no());
                        tvAbout.setText(response.body().getResult().getAbout());
                        tvAge.setText(String.valueOf(response.body().getResult().getAge()));
                        tvCity.setText(response.body().getResult().getCity());
                        tvRegion.setText(response.body().getResult().getRegion());
                        tvDegree.setText(response.body().getResult().getQualification());
                        tvExerience.setText(response.body().getResult().getPrevious_experience());
                        tvSkills.setText(response.body().getResult().getEducational_courses());
                        tvhours.setText(response.body().getResult().getHours_no());
                        tv_salary.setText(response.body().getResult().getExpected_salary());
                        tv_days.setText(response.body().getResult().getNumber_days()+"");
                        StringBuilder stringBuilder = new StringBuilder();
                        for (String day : response.body().getResult().getAvilable_days()) {
                            stringBuilder.append(day + "        ");
                            stringBuilder.lastIndexOf(day);
                        }
                        available_days.setText(stringBuilder);
                        Picasso.get().load(response.body().getResult().getImg()).into(imgProfile);

                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderAdvertsmentDetailsResponse> call, Throwable t) {

            }
        });
    }

}


