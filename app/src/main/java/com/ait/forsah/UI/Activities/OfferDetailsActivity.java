package com.ait.forsah.UI.Activities;

import com.ait.forsah.R;
import com.ait.forsah.base.ParentActivity;

public class OfferDetailsActivity extends ParentActivity {
    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_job_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
}
