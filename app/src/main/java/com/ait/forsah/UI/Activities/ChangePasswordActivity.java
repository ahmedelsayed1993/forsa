package com.ait.forsah.UI.Activities;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.ValidationUtils;
import com.ait.forsah.base.ParentActivity;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends ParentActivity {

    @BindView(R.id.etNewPass)
    EditText etNewPass;



    @BindView(R.id.etOldPassword)
    EditText etOldPass;


    @BindView(R.id.etConfirmPass)
    EditText etConfirmPass;


    @BindView(R.id.tv_app_bar)
    TextView tv_app_bar;

    @BindView(R.id.avi)
    AVLoadingIndicatorView avi;


    @BindView(R.id.laParent)
    LinearLayout laParent;


    @Override
    protected void initializeComponents() {


        tv_app_bar.setText(R.string.update_password);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_change_password;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }



    private boolean validation(){

        boolean flagOldPass,flagNePass,flagConfirmPass;

        flagOldPass= ValidationUtils.emptyValidation(etOldPass,getString(R.string.fill_empty));

        flagNePass=ValidationUtils.emptyValidation(etNewPass,getString(R.string.fill_empty));

        flagConfirmPass=ValidationUtils.emptyValidation(etConfirmPass,getString(R.string.fill_empty));

        if (flagOldPass){
            flagOldPass=ValidationUtils.checkPassSize(etOldPass,getString(R.string.passwordSizeValidation),6);
        }

        if (flagNePass){
            flagNePass=ValidationUtils.checkPassSize(etNewPass,getString(R.string.passwordSizeValidation),6);
        }


        if ( flagConfirmPass){
            flagConfirmPass=ValidationUtils.checkPassSize(etConfirmPass,getString(R.string.passwordSizeValidation),6);
        }
        if (flagNePass && flagConfirmPass){
            flagConfirmPass=ValidationUtils.validateConfirmPassword(etNewPass,etConfirmPass,getString(R.string.edt_confirm_new_password));
        }


       return flagConfirmPass && flagNePass && flagOldPass;

    }


    @OnClick(R.id.btnResetPass)
    void resetPass(){

        if (validation()){
            resetPass(etOldPass.getText().toString(),etNewPass.getText().toString()
            );
        }


    }


    private void resetPass(String oldPass,String newPass){
        RetroWeb.getClient()
                .create(ServiceApi.class)
                .changeClientPassword(mSharedPrefManager.getUserData().getId()+"",oldPass,newPass,mLanguagePrefManager.getAppLanguage())
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful()){
                            laParent.setVisibility(View.VISIBLE);
                            avi.setVisibility(View.GONE);
                            if (response.body().getKey()==1){

                                CommonUtil.makeToast(mContext,response.body().getMsg());
                                finish();

                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        laParent.setVisibility(View.GONE);
                        avi.setVisibility(View.VISIBLE);

                    }
                });
    }

    @OnClick(R.id.img_back)
    void btnBack(){
        onBackPressed();
    }


}
