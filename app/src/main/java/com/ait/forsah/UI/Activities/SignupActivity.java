package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ait.forsah.R;

public class SignupActivity extends AppCompatActivity {


    private Button buttonSeeker;
    private Button buttonEmployer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        buttonSeeker = findViewById(R.id.btnSeeker);
        buttonSeeker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                buttonSeeker.setClickable(false);

                Intent i = new Intent(SignupActivity.this, SignInActivity.class);
                i.putExtra("signup",1);
                startActivity(i);
//                finish();

            }
        });

        buttonEmployer = findViewById(R.id.btnEmployer);
        buttonEmployer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                buttonEmployer.setClickable(false);

                Intent i = new Intent(SignupActivity.this, SignInActivity.class);
                i.putExtra("signup",2);
                startActivity(i);
               // finish();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        buttonSeeker.setClickable(true);

        buttonEmployer.setClickable(true);

    }
}
