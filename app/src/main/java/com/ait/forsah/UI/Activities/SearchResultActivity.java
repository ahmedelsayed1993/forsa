package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.Providers.ProviderSearchModel;
import com.ait.forsah.Models.Providers.ProviderSearchResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.UI.Adapters.SearchAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultActivity extends AppCompatActivity {

    private static final String MYPREFERENCE = "myPref";
    private static final String LANG = "langKey";
    private String lang;


    SharedPrefManager mSharedPreferences;
    LanguagePrefManager mLanguagePrefManager;



    private ImageButton imagNotitfy;
    private ImageButton imagBack;
    private TextView notficationCont;


    List<ProviderSearchModel> list = new ArrayList<>();
    private TextView tvResultData;
   private RecyclerView recyclerView;
   private SearchAdapter adapter;

   private RelativeLayout relativeLayout;

   String jobId,cityId,regionId,degree,gender;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        mSharedPreferences = new SharedPrefManager(this);
        mLanguagePrefManager = new LanguagePrefManager(this);

        lang = mLanguagePrefManager.getAppLanguage();
        Intent intent = getIntent();




        imagNotitfy = findViewById(R.id.imgNotification);
        notficationCont = findViewById(R.id.notficationCont);
        imagBack = findViewById(R.id.img_back);


        getCount();

        imagNotitfy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(SearchResultActivity.this,NotificationActivity.class);
                intent.putExtra("type","provider");
                startActivity(intent);

            }
        });

        imagBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();

            }
        });


        tvResultData= findViewById(R.id.tv_result_data);
        relativeLayout = findViewById(R.id.liner_result);

        recyclerView = findViewById(R.id.recycler_search_result);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false));


        jobId = intent.getStringExtra("JobValue");
        cityId = intent.getStringExtra("CityValue");
        regionId = intent.getStringExtra("RegionValue");
        degree = intent.getStringExtra("DegreeValue");
        gender = intent.getStringExtra("GenderValue");


//        Toast.makeText(this, jobId, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, cityId, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, regionId, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, degree, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, gender, Toast.LENGTH_SHORT).show();

//        cityId = getIntent().getExtras().getInt("CityValue");
//        regionId = getIntent().getExtras().getInt("RegionValue");
//        degree = getIntent().getExtras().getInt("DegreeValue");
//        gender = getIntent().getExtras().getInt("GenderValue");

        Log.e("<<<jobId", String.valueOf(jobId));

        Log.e("<<<cityId", String.valueOf(cityId));
        Log.e("<<<joregionIdbId", String.valueOf(regionId));
        Log.e("<<<degree", String.valueOf(degree));



//
//        int JobValue = i.intValue();
//        int CityValue = i2.intValue();
//        int Region = i3.intValue();
//        int Degree = i4.intValue();
//        int dender = i5.intValue();

//        Toast.makeText(this, jobId, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, cityId, Toast.LENGTH_SHORT).show();



//        Toast.makeText(this, "know we are fine", Toast.LENGTH_SHORT).show();

//        Log.d("MainActivity","on create started");




//        adapter = new SearchAdapter(list);

        displayResult();
    }


    private void getCount(){

        RetroWeb.getClient().create(ServiceApi.class).
                GetNotifyByprovider(mSharedPreferences.getUserData().getId()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {
                        notficationCont.setText(String.valueOf(response.body().getCountNotify()));
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
            }
        });
    }

    private void displayResult() {
        RetroWeb.getClient().create(ServiceApi.class)
                .search(jobId,cityId,regionId,gender,degree,lang).enqueue(new Callback<ProviderSearchResponse>() {
            @Override
            public void onResponse(Call<ProviderSearchResponse> call, Response<ProviderSearchResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        list = response.body().getResult();
                        if (list.size() != 0) {
                            Log.e("list",new Gson().toJson(response.body().getResult()));
                            adapter = new SearchAdapter(SearchResultActivity.this, response.body().getResult());
                            recyclerView.setAdapter(adapter);
                        }else {
                            relativeLayout.setVisibility(View.VISIBLE);
                            tvResultData.setText(response.body().getMsg());
                        }
//                        if (list.isEmpty()){
//                            relativeLayout.setVisibility(View.VISIBLE);
//                            tvResultData.setText(response.body().getMsg());
////                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_LONG).show();
//                        }
                    }else {
                        Toast.makeText(SearchResultActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(SearchResultActivity.this,response.body().getMsg(), Toast.LENGTH_SHORT).show();

                }
            }
            @Override
            public void onFailure(Call<ProviderSearchResponse> call, Throwable t) {

            }
        });
    }


}
