package com.ait.forsah.UI.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.Listeners.Delet;
import com.ait.forsah.Models.Clients.ClientGetMyAdvetiseMent;
import com.ait.forsah.R;
import com.ait.forsah.UI.Activities.EditAdvertiseMeantActivity;
import com.ait.forsah.UI.Activities.MyAdvertiseMeantDetailsActivity;

import java.util.List;

public class MyAdvrtiseMentAdapter extends RecyclerView.Adapter<MyAdvrtiseMentAdapter.OrderViewHolder> {

    private Context context;
    private List<ClientGetMyAdvetiseMent> list;
    Dialog myDialog;
    Delet delet;


    public MyAdvrtiseMentAdapter(Context context, List<ClientGetMyAdvetiseMent> list) {
        this.context = context;
        this.list = list;
    }
    public MyAdvrtiseMentAdapter(Context context, List<ClientGetMyAdvetiseMent> list, Delet delet) {
        this.context = context;
        this.list = list;
        this.delet=delet;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderViewHolder(LayoutInflater.from(context).inflate(R.layout.ads_order_item,parent,false));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, final int position) {

        holder.tvSeekerName.setText(list.get(position).getName());
        holder.tvJobTitle.setText(list.get(position).getJob_title());
        holder.tvCity.setText(list.get(position).getCity());
        holder.tvRegion.setText(list.get(position).getRegion());
        holder.tvStstus.setText(list.get(position).getStutes());

                holder.img_edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Button btnEdit;
                        Button btnExit;
                        TextView textView;
                        myDialog = new Dialog(context);

                        myDialog.setContentView(R.layout.pop_layout_delet);
                        textView=myDialog.findViewById(R.id.tv_hint);
                        btnEdit =  myDialog.findViewById(R.id.btn_stay);
                        btnExit = myDialog.findViewById(R.id.btn_exit);

                        btnEdit.setText(context.getString(R.string.edit));

                        textView.setText(context.getString(R.string.edit_your_ad));
                        btnExit.setText(context.getString(R.string.cancel));
                        myDialog.show();
                        btnEdit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i;
                                i = new Intent(context, EditAdvertiseMeantActivity.class);
                                i.putExtra("jobId",list.get(position).getId_job());
                                context.startActivity(i);
                                myDialog.dismiss();
//                    myDialog.cancel();
                            }
                        });
                        btnExit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                myDialog.dismiss();
                            }
                        });

                    }
                });

        holder.img_dlet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button btnDlet;
                Button btnExit;
                TextView textView;
                myDialog = new Dialog(context);

                myDialog.setContentView(R.layout.pop_layout_delet);
                textView=myDialog.findViewById(R.id.tv_hint);
                btnDlet =  myDialog.findViewById(R.id.btn_stay);
                btnExit = myDialog.findViewById(R.id.btn_exit);

                btnDlet.setText(context.getString(R.string.dlet));

                textView.setText(context.getString(R.string.dlet_your_ad));
                btnExit.setText(context.getString(R.string.cancel));
                myDialog.show();
                btnDlet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                        delet.deletItem(position);


//                    myDialog.cancel();
                    }
                });
                btnExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myDialog.dismiss();
                    }
                });

            }
        });



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i;
                i = new Intent(context, MyAdvertiseMeantDetailsActivity.class);
                i.putExtra("jobId",list.get(position).getId_job());
                context.startActivity(i);


            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class OrderViewHolder extends RecyclerView.ViewHolder{
        private TextView tvSeekerName;
        private TextView tvJobTitle;
        private TextView tvCity;
        private TextView tvRegion;
        private TextView tvStstus;
        private ImageView img_edit;
        private ImageView img_dlet;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);

            tvSeekerName = itemView.findViewById(R.id.tv_ads_seeker_name);
            tvJobTitle = itemView.findViewById(R.id.tv_ads_job_title);
            tvCity = itemView.findViewById(R.id.tv_ads_city);
            tvRegion = itemView.findViewById(R.id.tv_ads_region);
            tvStstus = itemView.findViewById(R.id.tv_order_status);
            img_edit = itemView.findViewById(R.id.img_edit);
            img_dlet = itemView.findViewById(R.id.img_delete);
        }
    }
}
