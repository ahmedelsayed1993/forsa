package com.ait.forsah.UI.Activities;

import android.app.Dialog;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Listeners.Delet;
import com.ait.forsah.Models.Clients.ClientGetMyAdvetiseMent;
import com.ait.forsah.Models.Clients.ClientMyAdvtiseMentResponse;

import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.UI.Adapters.MyAdvrtiseMentAdapter;
import com.ait.forsah.base.ParentActivity;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAdvertiseMeantActivity extends ParentActivity implements Delet {


    @BindView(R.id.recycler_order_item)
    RecyclerView recyclerView;

    @BindView(R.id.tv_app_bar)
    TextView tv_app_bar;
    private MyAdvrtiseMentAdapter adapter;
    List<ClientGetMyAdvetiseMent> list;

    Dialog myDialog;


    private AVLoadingIndicatorView avi;


    @Override
    protected void initializeComponents() {

        myDialog = new Dialog(this);

        avi= findViewById(R.id.avi);
        tv_app_bar.setText(getString(R.string.My_ads));

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false));




        displayOrder();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_my_advrtise_meant;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        displayOrder();
    }

    private void displayOrder() {
        avi.smoothToShow();
        RetroWeb.getClient().create(ServiceApi.class)
                .getMyAdvert(mSharedPrefManager.getUserData().getId()+"",mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ClientMyAdvtiseMentResponse>() {
            @Override
            public void onResponse(Call<ClientMyAdvtiseMentResponse> call, Response<ClientMyAdvtiseMentResponse> response) {
                if (response.isSuccessful()){
                    avi.smoothToHide();
                    if (response.body().getKey() == 1){
                        list=response.body().getResult();
                        adapter = new MyAdvrtiseMentAdapter(MyAdvertiseMeantActivity.this,list,MyAdvertiseMeantActivity.this);
                        recyclerView.setAdapter(adapter);

                    }else {
                        Toast.makeText(MyAdvertiseMeantActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(MyAdvertiseMeantActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ClientMyAdvtiseMentResponse> call, Throwable t) {
                avi.smoothToHide();
                Toast.makeText(MyAdvertiseMeantActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.img_back)
    void back(){
        onBackPressed();
    }

    @Override
    public void deletItem(int position) {
        avi.smoothToShow();
        RetroWeb.getClient().create(ServiceApi.class)
                .changeStutes(list.get(position).getId_job()+"","7",mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    avi.smoothToHide();
                    if (response.body().getKey() == 1){
                        list.remove(position);
                        adapter.notifyDataSetChanged();


                    }else {
                        Toast.makeText(MyAdvertiseMeantActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(MyAdvertiseMeantActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                avi.smoothToHide();
                Toast.makeText(MyAdvertiseMeantActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deletItem(){

    }
}
