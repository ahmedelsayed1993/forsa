package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PolicyActivity extends AppCompatActivity {


    private SharedPrefManager sharedPrefManager;
    private LanguagePrefManager languagePrefManager;
    private static final String MYPREFERENCE = "myPref";
    private static final String LANG = "langKey";
    private String lang;

    private ImageButton imgBack;
    private TextView textViewPolicy;
    private TextView tvAppBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);

        sharedPrefManager = new SharedPrefManager(this);
        languagePrefManager =new LanguagePrefManager(this);
        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.menu_conditions);
        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        textViewPolicy = findViewById(R.id.tv_policy);


        displayPloyce();

    }

    void displayPloyce() {

        int value1 = getIntent().getIntExtra("home",0);
        Log.e("Valou",value1+"");

        switch (value1) {
            case 2:
                RetroWeb.getClient().create(ServiceApi.class)
                        .getConditionForClient(languagePrefManager.getAppLanguage()).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getKey() == 1) {
                                Log.d("TAG", "This is Client");
                                textViewPolicy.setText(response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        Log.e("<<<error", "error");
                    }
                });
                break;

            case 3:
                RetroWeb.getClient().create(ServiceApi.class)
                        .getConditionForProvider(languagePrefManager.getAppLanguage()).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getKey() == 1) {
                                Log.d("TAG", "This is provider");
                                textViewPolicy.setText(response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        Log.e("<<<error", "error");
                    }
                });
                break;
        }
    }
}