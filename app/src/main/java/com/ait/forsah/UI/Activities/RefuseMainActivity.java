package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ait.forsah.R;

public class RefuseMainActivity extends AppCompatActivity {

    private TextView tvAppBar;
    private ImageButton imgBack;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refuse_main);


        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.title_refuse);
        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
}
