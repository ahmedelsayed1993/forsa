package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ait.forsah.R;
import com.ait.forsah.Shared.SharedPrefManager;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class SeekerProfileActivity extends AppCompatActivity {



    private TextView tvAppBar;

    private CircleImageView imagEdit;
    private CircleImageView imgProfile;
    private TextView tvName;
    private TextView tvUserName;
    private TextView tvPhone;
    private TextView tvMail;
    private TextView tvCity;
    private TextView tvRegion;
   // private TextView tvJob;
    private TextView tvDegree;
    private TextView tvExperience;
    private TextView tvCourse;
    private TextView tvAbout;


    SharedPrefManager mSharedPreferences;
    protected SharedPrefManager mEditor;


    ImageView imageView;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seeker_profile);

        mSharedPreferences = new SharedPrefManager(this);
        mEditor=new SharedPrefManager(this);

        imageView = findViewById(R.id.img_back);
        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.title_profile);

        imagEdit = findViewById(R.id.img_edit_profile);



        imagEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivity(new Intent(SeekerProfileActivity.this, SeekerProfileEditActivity.class));
                finish();
            }

        });

        imageView.setOnClickListener(view ->
                onBackPressed()
                );
        imgProfile = findViewById(R.id.img_profile);
        tvName = findViewById(R.id.tv_user_profile_name);
        tvUserName = findViewById(R.id.tv_user_profile_user_name);
        tvPhone = findViewById(R.id.tv_user_profile_phone);
        tvMail = findViewById(R.id.tv_user_profile_mail);
        tvCity = findViewById(R.id.tv_user_profile_city);
        tvRegion = findViewById(R.id.tv_user_profile_region);
//        tvJob = findViewById(R.id.tv_user_profile_job);
        tvDegree = findViewById(R.id.tv_user_profile_qualification);
        tvExperience = findViewById(R.id.tv_user_profile_experience);
        tvCourse = findViewById(R.id.tv_user_profile_course);
        tvAbout = findViewById(R.id.tv_user_profile_about);



        displayData();

    }

    private void displayData() {


        Picasso.get().load(mSharedPreferences.getUserData().getImg()).into(imgProfile);



        tvName.setText(mSharedPreferences.getUserData().getName());
        tvUserName.setText(mSharedPreferences.getUserData().getUser_name());
        tvPhone.setText(mSharedPreferences.getUserData().getPhone());
        tvMail.setText(mSharedPreferences.getUserData().getEmail());
        tvCity.setText(mSharedPreferences.getUserData().getCity());
        tvRegion.setText(mSharedPreferences.getUserData().getRegion());
      //  tvJob.setText(mSharedPreferences.getUserData().getJob_title());
        tvDegree.setText(mSharedPreferences.getUserData().getQualification());
        tvExperience.setText(mSharedPreferences.getUserData().getPrevious_experience());
        tvCourse.setText(mSharedPreferences.getUserData().getEducational_courses());
        tvAbout.setText(mSharedPreferences.getUserData().getAbout());
        tvAbout.setMovementMethod(new ScrollingMovementMethod());

    }




}
