package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ait.forsah.R;

public class OrderSentActivity extends AppCompatActivity {

    private TextView textView;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final String MYPREFERENCE = "myPref";
    private static final String USER_ID = "idKey";
    private String idUser;
    private String jobId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_sent);


        sharedPreferences = getSharedPreferences(MYPREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        idUser = sharedPreferences.getString(USER_ID,null);


        Intent intent = getIntent();
        Integer i = intent.getIntExtra("status",1);
        int value = i.intValue();


        jobId = intent.getStringExtra("jobId");

        Log.d("here ","services");
//        addServices();



        textView = findViewById(R.id.tv_back_home);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(OrderSentActivity.this,HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

//    private void addServices() {
//
//        RetroWeb.getClient().create(ServiceApi.class)
//                .addOrder(idUser,jobId).enqueue(new Callback<BaseResponse>() {
//            @Override
//            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
//                if (response.isSuccessful()){
//                    if (response.body().getKey() == 1){
//                        Toast.makeText(OrderSentActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(OrderSentActivity.this, "sent", Toast.LENGTH_SHORT).show();
//                    }
//                }else {
//                    Toast.makeText(OrderSentActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
//                    Toast.makeText(OrderSentActivity.this, "key not 1", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<BaseResponse> call, Throwable t) {
//
//            }
//        });
//
//    }
}
