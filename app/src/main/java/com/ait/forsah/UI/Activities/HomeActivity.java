package com.ait.forsah.UI.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.CityResponse;
import com.ait.forsah.Models.JobTitleResponse;
import com.ait.forsah.Models.RegionResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;

import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    SharedPrefManager mSharedPreferences;
    LanguagePrefManager mLanguagePrefManager;

    protected SharedPrefManager mEditor;


    private TextView textView;
    private Button btnEdit;
    private Button btnExit;


    private ImageButton imageButtonMenu;
    private ImageButton imageButtonNoti;
    private TextView notficationCont;
    private Button buttonEmployer;
    private Spinner spinnerJobTitle;
    private Spinner spinnerCity;
    private Spinner spinnerRegion;
    private Spinner spinnerGender;
    private TextView textViewProfile;
    private TextView textViewUaseName;
    private TextView textViewOrder;
    private TextView textViewCall;
    private TextView textViewPolicy;
    private TextView textViewAbout;
    private TextView textViewLogout;

    private Spinner spinnerDegree;
    private CircleImageView imageView;
    private static final String MYPREFERENCE = "myPref";
    private static final String LOG_IN = "loginKey";
    private static final String USER_ID = "idKey";
    private static final String LANG = "langKey";
    private static final String USER_Name = "nameKey";
    private static final String USER_IMAGE = "imageKey";

    private DrawerLayout drawer;
    private String id;
    private String user;
    private String imageUrl;
    private String lang;

    String JOBID;
    String CITYID;
    String REGIONID;
    String GENDER = "0";
    String DEGREE ="0";
    Intent mIntent;


    Dialog myDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mSharedPreferences = new SharedPrefManager(this);
        mLanguagePrefManager = new LanguagePrefManager(this);
        mEditor=new SharedPrefManager(this);


        myDialog = new Dialog(this);

        drawer = findViewById(R.id.drawer_layout);

        spinnerJobTitle = findViewById(R.id.spin_home_job_title);
        spinnerCity = findViewById(R.id.spin_home_city);
        textViewUaseName = findViewById(R.id.tv_home_provider_name);
        textViewOrder = findViewById(R.id.tv_home_order);
        textViewProfile = findViewById(R.id.tv_home_profile);
        textViewCall = findViewById(R.id.tv_home_call);
        textViewPolicy = findViewById(R.id.tv_home_policy);
        textViewAbout = findViewById(R.id.tv_home_about);
        textViewLogout = findViewById(R.id.tv_home_Logout);

        imageView = findViewById(R.id.img_home_Profile);

        spinnerRegion = findViewById(R.id.spin_home_region);
        spinnerGender = findViewById(R.id.spin_home_gender);
        spinnerDegree = findViewById(R.id.spin_home_degree);


        id = mSharedPreferences.getUserData().getId()+"";

        user = mSharedPreferences.getUserData().getUser_name();
        textViewUaseName.setText(user);
        imageUrl = mSharedPreferences.getUserData().getImg();

        Picasso.get().load(imageUrl).into(imageView);





        //Side Menu
        textViewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, OrderActivity.class));
                drawer.closeDrawer(GravityCompat.START);

            }
        });
        textViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ProviderProfileActivity.class));
                drawer.closeDrawer(GravityCompat.START);

            }
        });
        textViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, CallUsActivity.class);
                i.putExtra("home", 2);
                startActivity(i);
                drawer.closeDrawer(GravityCompat.START);

            }
        });
        textViewPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, PolicyActivity.class);
                i.putExtra("home", 3);
                startActivity(i);
                drawer.closeDrawer(GravityCompat.START);

            }
        });
        textViewAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, AboutActivity.class);
                i.putExtra("home", 3);
                startActivity(i);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        textViewLogout.setOnClickListener(view -> {

            myDialog.setContentView(R.layout.pop_layout_delet);
            textView = myDialog.findViewById(R.id.tv_hint);
            btnEdit = myDialog.findViewById(R.id.btn_stay);
            btnExit = myDialog.findViewById(R.id.btn_exit);

//            btnEdit.setText(this.getString(R.string.exit));

            textView.setText(getString(R.string.tostLogOut));

            myDialog.show();
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RetroWeb.getClient().create(ServiceApi.class)
                            .logoutProvider(mSharedPreferences.getUserData().getId() + "").enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()) {
                                if (response.body().getKey() == 1) {
                                    mSharedPreferences.setLoginStatus(false);
                                    mSharedPreferences.Logout();
                                    Toast.makeText(HomeActivity.this, getString(R.string.logOut), Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(HomeActivity.this, ChangeLanguageActivity.class));
                                    finishAffinity();
                                } else {
                                    Toast.makeText(HomeActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });


//                    myDialog.cancel();
                }
            });
            btnExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myDialog.dismiss();
                }
            });

        });


        //Initializing Spinner of Gender
        List<String> gender = new ArrayList<>();
        gender.add(getString(R.string.gender));
        gender.add(getString(R.string.male));
        gender.add(getString(R.string.female));

        final List<String> degree = new ArrayList<>();
        degree.add(getString(R.string.degree));
        degree.add(getString(R.string.secondary));
        degree.add(getString(R.string.academic));
        degree.add(getString(R.string.master));


//        android.R.layout.simple_list_item_activated_1,gender){

//        simple_list_item_checked صح

//        preference_category خط بين كل عنصر

        // simple_list_item_activated_1

        //Gender Spinner
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_activated_1, gender) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }
        };
        spinnerGender.setAdapter(genderAdapter);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1) {
                    GENDER = "m";
//                    selectedItem.equals(m);
//                    Toast.makeText(getApplicationContext(), "Selected "+ m , Toast.LENGTH_LONG).show();

                } else if (position ==2){
                    GENDER = "f";
//                    selectedItem.equals(f);
//                    Toast.makeText(getApplicationContext(), "Selected " + f, Toast.LENGTH_LONG).show();
                }

                Log.e("<<<gender",GENDER);
//                GENDER = selectedItem;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Degree Spinner
        ArrayAdapter<String> degreeAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_activated_1, degree) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }
        };
        spinnerDegree.setAdapter(degreeAdapter);
        spinnerDegree.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1) {
                    DEGREE = "1" ;
//                    Toast.makeText(getApplicationContext(), "Selected "+ position+ selectedItem, Toast.LENGTH_LONG).show();
                } else if (position == 2) {
                    DEGREE = "2" ;
//                    Toast.makeText(getApplicationContext(), "Selected "+ position+ selectedItem, Toast.LENGTH_LONG).show();
                } else if (position == 3){
                    DEGREE = "3" ;
//                    Toast.makeText(getApplicationContext(), "Selected "+ position+ selectedItem, Toast.LENGTH_LONG).show();
                }
                Log.e("<<<DEGREE",DEGREE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        final NavigationView navigationView = findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(HomeActivity.this);
        imageButtonMenu = findViewById(R.id.img_menu);
        imageButtonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });

        imageButtonNoti = findViewById(R.id.imgNotification);
        notficationCont=  findViewById(R.id.notficationCont);



        getCount();
        imageButtonNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, NotificationActivity.class);
                i.putExtra("type","provider");
                startActivity(i);
            }
        });
        buttonEmployer = findViewById(R.id.btn_employer_search);
        buttonEmployer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkStringText(JOBID, "0", getString(R.string.must_chose_job)) ||
                        checkStringText(CITYID, "0", getString(R.string.must_chose_city)) ||
                        checkStringText(REGIONID, "0", getString(R.string.must_chose_region)) ||
                        checkStringText(DEGREE, "0", getString(R.string.must_chose_qualification)) ||
                        checkStringText(GENDER, "0", getString(R.string.must_chose_gender))
                ) {
                    return;
                } else {


                    mIntent = new Intent(HomeActivity.this, SearchResultActivity.class);

                    mIntent.putExtra("JobValue", JOBID);
                    mIntent.putExtra("CityValue", CITYID);
                    mIntent.putExtra("RegionValue", REGIONID);
                    mIntent.putExtra("DegreeValue", DEGREE);
                    mIntent.putExtra("GenderValue", GENDER);
                    startActivity(mIntent);

                }
            }
        });


        displayJobTitle();
        displayCity();

    }



    private void getCount(){

        RetroWeb.getClient().create(ServiceApi.class).
        GetNotifyByprovider(mSharedPreferences.getUserData().getId()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {
                            notficationCont.setText(String.valueOf(response.body().getCountNotify()));
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
            }
        });
    }

    // Job Title Spinner
    private void displayJobTitle() {
        final ArrayList<String> jobName = new ArrayList<>();
        jobName.add(0, getString(R.string.spinner_job_title));
        final ArrayList<Integer> jobId = new ArrayList<>();
        jobId.add(0, 0);
        RetroWeb.getClient().create(ServiceApi.class)
                .getJobTitle(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<JobTitleResponse>() {
            @Override
            public void onResponse(Call<JobTitleResponse> call, Response<JobTitleResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {

                        for (int i = 0; i < response.body().getJob_title().size(); i++) {

                            jobName.add(response.body().getJob_title().get(i).getName());
                            jobId.add(response.body().getJob_title().get(i).getId());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(HomeActivity.this,
                                android.R.layout.simple_list_item_activated_1, jobName) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        spinnerJobTitle.setAdapter(adapter);
                        spinnerJobTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                if (!jobName.get(position).equals(R.string.edt_job_title)) {
//                                    String name = jobName.get(position);
//                                    i.putExtra("JobValue",);
                                    //  = String.valueOf(intent.putExtra("JobValue",jobId));
                                    JOBID = String.valueOf(jobId.get(position));
                                    Log.e("basedID", JOBID);
//                                    JOBID = String.valueOf(3);


//                                    Toast.makeText(HomeActivity.this, String.valueOf(id), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<JobTitleResponse> call, Throwable t) {
            }
        });
    }

    // City Spinner
    private void displayCity() {
        final ArrayList<String> cityName = new ArrayList<>();
        cityName.add(0, getString(R.string.spinner_city));
        final ArrayList<Integer> cityId = new ArrayList<>();
        cityId.add(0, 0);
        RetroWeb.getClient().create(ServiceApi.class)
                .getCity(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {

                        for (int i = 0; i < response.body().getCity().size(); i++) {
                            cityName.add(response.body().getCity().get(i).getName());
                            cityId.add(response.body().getCity().get(i).getId());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(HomeActivity.this,
                                android.R.layout.simple_list_item_activated_1, cityName) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        spinnerCity.setAdapter(adapter);
                        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int postion, long l) {
                                if (!cityName.get(postion).equals(R.string.tv_city)) {
//                                    String city = cityName.get(postion);
                                    int id = cityId.get(postion);
                                    CITYID = String.valueOf(cityId.get(postion));

//                                    CITYID = String.valueOf(5);

                                    displayRegion(id);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });
    }

    // Region Spinner
    private void displayRegion(int id) {
        final ArrayList<String> regionName = new ArrayList<>();
        regionName.add(0, getString(R.string.spinner_region));
        final ArrayList<Integer> regionId = new ArrayList<>();
        regionId.add(0, 0);
        RetroWeb.getClient().create(ServiceApi.class)
                .getRegion(id, mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<RegionResponse>() {
            @Override
            public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {
//                        Toast.makeText(HomeActivity.this, "success", Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < response.body().getRegion().size(); i++) {
                            regionName.add(response.body().getRegion().get(i).getName());
                            regionId.add(response.body().getRegion().get(i).getId());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(HomeActivity.this,
                                android.R.layout.simple_list_item_activated_1, regionName) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        spinnerRegion.setAdapter(adapter);
                        spinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int postion, long l) {
                                if (!regionName.get(postion).equals(R.string.user_town)) {
//                                    String region = regionName.get(postion);
                                    int id = regionId.get(postion);

                                    REGIONID = String.valueOf(regionId.get(postion));

//                                    REGIONID = String.valueOf(5);

//                                    Toast.makeText(getApplicationContext(),String.valueOf(id),Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<RegionResponse> call, Throwable t) {
                Toast.makeText(HomeActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
            }
        });
    }


//
//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
//            drawer.closeDrawer(Gravity.RIGHT);
//        } else {
//            super.onBackPressed();
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
//            drawer.closeDrawer(Gravity.RIGHT);
//        } else {
//            Button btnStay;
//            Button btnExit;
//            myDialog.setContentView(R.layout.pop_layout);
//            btnStay = myDialog.findViewById(R.id.btn_stay);
//            btnExit = myDialog.findViewById(R.id.btn_exit);
//            btnStay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    myDialog.dismiss();
////                    myDialog.cancel();
//                }
//            });
//            btnExit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    HomeActivity.this.finishAffinity();
//                }
//            });
//            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            myDialog.show();
//        }
//    }

    private boolean checkStringText(String text, String match, String message) {
        if (text.equals(match)) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        textViewUaseName.setText(mSharedPreferences.getUserData().getUser_name());
        imageUrl = mSharedPreferences.getUserData().getImg();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCount();
//        list.clear();
//        adapter.notifyDataSetChanged();
//        displayResult();
    }
}
