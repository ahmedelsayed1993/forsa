package com.ait.forsah.UI.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Listeners.SpinerCityAndRejone;
import com.ait.forsah.Models.BranchCountModel;
import com.ait.forsah.Models.BranchCountModelSupmet;
import com.ait.forsah.Models.CityResponse;
import com.ait.forsah.Models.GetBranchesModel;
import com.ait.forsah.Models.RegionResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.UI.Adapters.BranchCountAdapter;
import com.ait.forsah.base.ParentActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddBranchActivity extends ParentActivity implements SpinerCityAndRejone {



    @BindView(R.id.spn_register_branch_count)
     Spinner spinnerBranchConut;

    @BindView(R.id.tv_app_bar)
    TextView tvAppBar;



    @BindView(R.id.img_back)
    ImageView img_back;





    @BindView(R.id.recycler_branch_count)
            RecyclerView recyclerView;




    ArrayList<String> cityName;
    ArrayList<Integer> cityId ;

    BranchCountAdapter adapterBranches;

    List<BranchCountModel> branches;
    LinearLayoutManager branhesManger;


    ArrayList<GetBranchesModel> getBranchesModel;

    @Override
    protected void initializeComponents() {
        getBranchesModel = new ArrayList<>();

        Intent intent = getIntent();
        getBranchesModel = (ArrayList<GetBranchesModel>)intent.getSerializableExtra("branches");



        branches = new ArrayList<>();

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));

        tvAppBar.setText(getString(R.string.AddBranch));



        ArrayList<String> number = new ArrayList<>();
        number.add("عدد الفروع");
        number.add("1");
        number.add("2");
        number.add("3");



        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, number) {

            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }
//
//
        };

        displayCity();




        spinnerBranchConut.setAdapter(arrayAdapter);
        spinnerBranchConut.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position!=0){
                    branches.clear();
                    for (int i=0;i<Integer.valueOf(number.get(position));i++){
                        BranchCountModel branchCountModel = new BranchCountModel();
                        branchCountModel.setCitySpinner(cityName);
                        branchCountModel.setCityId(cityId);

                        branches.add(branchCountModel);
                            branhesManger = new LinearLayoutManager(AddBranchActivity.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(branhesManger);
                        adapterBranches = new BranchCountAdapter(AddBranchActivity.this,branches, AddBranchActivity.this);
                        recyclerView.setAdapter(adapterBranches);
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });








    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_branch;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void Rejone(int position, int portionItem, String cityName) {
        displayRegion(position,portionItem,cityName);
    }




    @OnClick(R.id.btnAdd)
    void btnAdd() {

        ArrayList<BranchCountModelSupmet> branchCountModelSupmetArrayList = new ArrayList<BranchCountModelSupmet>();
        if (branches.size() != 0) {



            if (getBranchesModel.size()!=0){

                for (int i = 0; i < getBranchesModel.size(); i++) {
                    BranchCountModelSupmet branchCountModelSupmet = new BranchCountModelSupmet();
                    branchCountModelSupmet.setCity_id(getBranchesModel.get(i).getCity_id());
                    branchCountModelSupmet.setRegion(getBranchesModel.get(i).getRegion());
                    branchCountModelSupmet.setStreet(getBranchesModel.get(i).getStreet());
                    branchCountModelSupmet.setWork_hours(getBranchesModel.get(i).getWork_hours()+"");
                    branchCountModelSupmetArrayList.add(branchCountModelSupmet);
                }

            }

            for (int i = 0; i < branches.size(); i++) {
                BranchCountModelSupmet branchCountModelSupmet = new BranchCountModelSupmet();
                branchCountModelSupmet.setCity_id(branches.get(i).getCitid());
                branchCountModelSupmet.setRegion(branches.get(i).getRejonid());
                branchCountModelSupmet.setStreet(branches.get(i).getStreet());
                branchCountModelSupmet.setWork_hours(branches.get(i).getWorkHours());
                branchCountModelSupmetArrayList.add(branchCountModelSupmet);
            }
            update( new Gson().toJson(branchCountModelSupmetArrayList));


        }else {
            CommonUtil.makeToast(mContext,getString(R.string.noUpdate));
        }

    }

    private void displayCity(){
        cityName = new ArrayList<>();
        cityId = new ArrayList<>();
        RetroWeb.getClient().create(ServiceApi.class)
                .getCity(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        for (int i =0;i<response.body().getCity().size();i++){
                            cityName.add(response.body().getCity().get(i).getName());
                            cityId.add(response.body().getCity().get(i).getId());
                            cityId.get(i);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });
    }

    private void displayRegion(int id ,int postionItem , String CiteyName) {
        final ArrayList<String> regionName = new ArrayList<>();
        regionName.add("الحي");
        final ArrayList<Integer> regionId = new ArrayList<>();


        RetroWeb.getClient().create(ServiceApi.class)
                .getRegion(id,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<RegionResponse>() {
            @Override
            public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        Toast.makeText(AddBranchActivity.this, "success", Toast.LENGTH_SHORT).show();
                        for (int i=0;i<response.body().getRegion().size();i++){
                            regionName.add(response.body().getRegion().get(i).getName());
                            regionId.add(response.body().getRegion().get(i).getId());
                        }
                        BranchCountModel branchCountModel=branches.get(postionItem);
                        branchCountModel.setRegionSpinner(regionName);
                        branchCountModel.setRjonId(regionId);
                        branchCountModel.setCityName(CiteyName);
                        branches.set(postionItem,branchCountModel);
                        adapterBranches.notifyItemChanged(postionItem);
                        Log.e("branches", String.valueOf(branchCountModel.getCitid()));
                    }
                }
            }
            @Override
            public void onFailure(Call<RegionResponse> call, Throwable t) {
                Toast.makeText(AddBranchActivity.this, "fail", Toast.LENGTH_SHORT).show();
            }
        });



    }


    private void update(String branches){
        RetroWeb.getClient().create(ServiceApi.class)
                .updateBranches(mSharedPrefManager.getUserData().getId()+"",branches)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        CommonUtil.makeToast(AddBranchActivity.this,response.body().getMsg());
                        finish();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {

                    }
                });
    }


    @OnClick(R.id.img_back)
    void  img_back(){
        onBackPressed();
    }
}
