package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.ait.forsah.R;

public class JobDetailsActivity extends AppCompatActivity {

    private TextView tvAppBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);

        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.title_job_hire_details);
    }
}



