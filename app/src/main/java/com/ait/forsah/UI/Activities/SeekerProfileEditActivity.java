package com.ait.forsah.UI.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Models.CityResponse;
import com.ait.forsah.Models.Clients.ClientRegisterModel;
import com.ait.forsah.Models.Clients.ClientRegisterResponse;
import com.ait.forsah.Models.JobTitleResponse;
import com.ait.forsah.Models.RegionResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.ProgressRequestBody;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.ValidationUtils;
import com.fxn.pix.Pix;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeekerProfileEditActivity extends AppCompatActivity implements ProgressRequestBody.UploadCallbacks  {

    private  String QULIFCATIONID;
    private TextView tvAppBar;


    private AVLoadingIndicatorView avi;

    private CircleImageView imgProfile;
    private EditText edtName;
    private EditText edtUserName;
    private EditText edtPhone;
    private EditText edtMail;
    private Spinner spinnerCity;
    private Spinner spinnerRegion;
    private Spinner spinnerJob;

    private Spinner spinDegree;
    private EditText edtExperience;
    private EditText edtCourse;
    private TextView tvPassword;
    private EditText edtAbout;
    private Button btn_Confirm;
    private ImageView btnBack;

    int idCity;
    int idRegion;


    ClientRegisterModel clientRegisterModel;

    SharedPrefManager mSharedPreferences;
    LanguagePrefManager languagePrefManager;
    protected SharedPrefManager mEditor;
    private int idJobTitle;
    final List<String> qoulifcation = new ArrayList<>();
    private String realImagePath;
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seeker_profile_edit);
        languagePrefManager =new LanguagePrefManager(this);
        mSharedPreferences = new SharedPrefManager(this);
        mEditor = new SharedPrefManager(this);

        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.title_profile);
        btnBack=findViewById(R.id.img_back);

        relativeLayout=findViewById(R.id.relative_layout);

        final String indicator=getIntent().getStringExtra("indicator");
        avi= findViewById(R.id.avi);
        avi.setIndicator(indicator);



        btn_Confirm = findViewById(R.id.btn_Confirm);
        imgProfile = findViewById(R.id.img_profile);
        edtName = findViewById(R.id.edt_user_profile_name);
        edtUserName = findViewById(R.id.edt_user_profile_user_name);
        edtPhone = findViewById(R.id.edt_user_profile_phone);
        edtMail = findViewById(R.id.edt_user_profile_mail);
        spinDegree = findViewById(R.id.spin_user_profile_qualification);
        edtExperience = findViewById(R.id.edt_user_profile_experience);
        edtCourse = findViewById(R.id.edt_user_profile_course);
        tvPassword = findViewById(R.id.edt_user_profile_password);
        edtAbout = findViewById(R.id.edt_user_profile_about);

        imgProfile.setOnClickListener(view ->
                Pix.start(this,1,1)
);

        spinnerCity = findViewById(R.id.spin_user_profile_city);
        spinnerRegion = findViewById(R.id.spin_user_profile_region);
     //   spinnerJob = findViewById(R.id.spin_user_profile_job);


        tvPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SeekerProfileEditActivity.this, ChangePasswordActivity.class));
            }
        });


        btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validation()){
                    if (realImagePath!=null){
                    confirmUpdateImg();
                    } else {

                        confirmUpdate();
                    }
                }
            }

        });


        btnBack.setOnClickListener(view ->
                onBackPressed());

        getUserData();
    }






    private void confirmUpdate() {
        relativeLayout.setVisibility(View.INVISIBLE);
        avi.smoothToShow();


            RetroWeb.getClient().create(ServiceApi.class).updateClientData(
                    String.valueOf(mSharedPreferences.getUserData().getId()),
                    edtName.getText().toString(),
                    mSharedPreferences.getUserData().getId_number(),
                    mSharedPreferences.getUserData().getBirth_date(),
                    edtPhone.getText().toString(),
                    edtMail.getText().toString(),
                    edtUserName.getText().toString(),
                    idCity,
                    idRegion,
                    idJobTitle,
                    QULIFCATIONID,
                    edtExperience.getText().toString(),
                    edtCourse.getText().toString(),
                    edtAbout.getText().toString(),
                    languagePrefManager.getAppLanguage()).enqueue(new Callback<ClientRegisterResponse>() {
                @Override
                public void onResponse(Call<ClientRegisterResponse> call, Response<ClientRegisterResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getKey() == 1) {
                            Toast.makeText(SeekerProfileEditActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            relativeLayout.setVisibility(View.VISIBLE);
                            avi.hide();
                            mSharedPreferences.setUserData(response.body().getClient());
                            finish();

                        } else {
                            Toast.makeText(SeekerProfileEditActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            relativeLayout.setVisibility(View.VISIBLE);
                            avi.hide();
                        }

                    }
                }

                @Override
                public void onFailure(Call<ClientRegisterResponse> call, Throwable t) {
                    relativeLayout.setVisibility(View.VISIBLE);
                    avi.hide();
                }
            });

        }


//    private void displayJobTitle() {
//        final ArrayList<String> jobName = new ArrayList<>();
//        jobName.add(0, getString(R.string.spinner_job_title));
//        final ArrayList<Integer> jobId = new ArrayList<>();
//        RetroWeb.getClient().create(ServiceApi.class)
//                .getJobTitle("ar").enqueue(new Callback<JobTitleResponse>() {
//            @Override
//            public void onResponse(Call<JobTitleResponse> call, Response<JobTitleResponse> response) {
//                if (response.isSuccessful()) {
//                    if (response.body().getKey() == 1) {
//                        for (int i = 0; i < response.body().getJob_title().size(); i++) {
//                            jobName.add(response.body().getJob_title().get(i).getName());
//                            jobId.add(response.body().getJob_title().get(i).getId());
//                        }
//
//                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SeekerProfileEditActivity.this,
//                                android.R.layout.simple_list_item_activated_1, jobName) {
//                            @Override
//                            public boolean isEnabled(int position) {
//                                if (position == 0) {
//                                    // Disable the first item from Spinner
//                                    // First item will be use for hint
//                                    return false;
//                                } else {
//                                    return true;
//                                }
//                            }
//                        };
//                        spinnerJob.setAdapter(adapter);
//                        spinnerJob.setSelection(jobName.indexOf(clientRegisterModel.getJob_title()));
//                        spinnerJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                            @Override
//                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                                    String name = jobName.get(position);
//                                    idJobTitle = jobId.get(position -1);
////                                    Toast.makeText(RegisterAsSeekerActivity.this, String.valueOf(id), Toast.LENGTH_SHORT).show();
//
//
//
//                            }
//
//                            @Override
//                            public void onNothingSelected(AdapterView<?> adapterView) {
//
//                            }
//                        });
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JobTitleResponse> call, Throwable t) {
//
//            }
//        });
//
//    }


    private void confirmUpdateImg() {

        Log.e(">>>>>>>", "Das");
        File ImageFile = new File(realImagePath);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, SeekerProfileEditActivity.this);
        MultipartBody.Part fileParts;
        fileParts= MultipartBody.Part.createFormData("Img", ImageFile.getName(), fileBody);

        relativeLayout.setVisibility(View.INVISIBLE);
        avi.smoothToShow();
        RetroWeb.getClient().create(ServiceApi.class).updateClientData(
                String.valueOf(mSharedPreferences.getUserData().getId()),
                edtName.getText().toString(),
                mSharedPreferences.getUserData().getId_number(),
                mSharedPreferences.getUserData().getBirth_date(),
                edtPhone.getText().toString(),
                edtMail.getText().toString(),
                edtUserName.getText().toString(),
                idCity,
                idRegion,
                idJobTitle,
                QULIFCATIONID,
                edtExperience.getText().toString(),
                edtCourse.getText().toString(),
                edtAbout.getText().toString(),
                fileParts,
                languagePrefManager.getAppLanguage()).enqueue(new Callback<ClientRegisterResponse>() {
            @Override
            public void onResponse(Call<ClientRegisterResponse> call, Response<ClientRegisterResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {
                        Toast.makeText(SeekerProfileEditActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        relativeLayout.setVisibility(View.VISIBLE);
                        avi.hide();
                        mSharedPreferences.setUserData(response.body().getClient());
                        finish();
                    } else {
                        Toast.makeText(SeekerProfileEditActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        relativeLayout.setVisibility(View.VISIBLE);
                        avi.hide();

                    }
                }
            }

            @Override
            public void onFailure(Call<ClientRegisterResponse> call, Throwable t) {
                relativeLayout.setVisibility(View.VISIBLE);
                avi.hide();
            }
        });

    }




        private void getUserData() {

            RetroWeb.getClient().create(ServiceApi.class)
                    .getUserData(mSharedPreferences.getUserData().getId(),languagePrefManager.getAppLanguage())
                    .enqueue(new Callback<ClientRegisterResponse>() {
                        @Override
                        public void onResponse(Call<ClientRegisterResponse> call, Response<ClientRegisterResponse> response) {


                            if (response.isSuccessful()) {

                                if (response.body().getKey()==1){
                                    clientRegisterModel =response.body().getClient();

                                    displayData();
                                    displayCity();

                                    displayRegion(clientRegisterModel.getFk_city());
                                    //displayJobTitle();

                                }

                            }


                        }

                        @Override
                        public void onFailure(Call<ClientRegisterResponse> call, Throwable t) {

                        }
                    });
    }







    private void displayData() {

        Picasso.get().load(clientRegisterModel.getImg()).into(imgProfile);

        idCity=clientRegisterModel.getFk_city();
        idRegion=clientRegisterModel.getFk_region();

        idJobTitle=clientRegisterModel.getFk_job_title();
        edtName.setText(clientRegisterModel.getName());
        edtUserName.setText(clientRegisterModel.getUser_name());
        edtPhone.setText(clientRegisterModel.getPhone());
        edtMail.setText(clientRegisterModel.getEmail());
        edtExperience.setText(clientRegisterModel.getPrevious_experience());
        edtCourse.setText(clientRegisterModel.getEducational_courses());
        edtAbout.setText(clientRegisterModel.getAbout());









        qoulifcation.add(clientRegisterModel.getQualification());
        qoulifcation.add(getString(R.string.secondary));
        qoulifcation.add(getString(R.string.academic));
        qoulifcation.add(getString(R.string.master));
        QULIFCATIONID=clientRegisterModel.getQualification_id();

        ArrayAdapter<String> arrayAdapterQoulifcation = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_activated_1, qoulifcation) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

        };


        spinDegree.setAdapter(arrayAdapterQoulifcation);

        spinDegree.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String selectedItem = (String) parent.getItemAtPosition(position);

                if (position == 1) {
                    QULIFCATIONID = "1";

                } else if (position ==2){
                    QULIFCATIONID = "2";
                }else if (position==3){
                    QULIFCATIONID = "3";
                }

                Log.e("<<<gender",QULIFCATIONID);

            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


    }


    //CheckEditTextError
    public static boolean checkEditTextError(EditText editText, String message, Window window) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError(message);
            if (editText.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return true;
        }
        return false;
    }





    private void displayCity() {
        final ArrayList<String> cityName = new ArrayList<>();
        final ArrayList<Integer> cityId = new ArrayList<>();
        RetroWeb.getClient().create(ServiceApi.class)
                .getCity("ar").enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {

                        for (int i = 0; i < response.body().getCity().size(); i++) {
                            cityName.add(response.body().getCity().get(i).getName());
                            cityId.add(response.body().getCity().get(i).getId());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SeekerProfileEditActivity.this,
                                android.R.layout.simple_list_item_activated_1, cityName) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        spinnerCity.setAdapter(adapter);
                        spinnerCity.setSelection(cityName.indexOf(clientRegisterModel.getCity()));
                        Log.e("city",clientRegisterModel.getCity());
                        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int postion, long l) {

                                    idCity = cityId.get(postion);
                                    displayRegion(idCity);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });
    }

    private void displayRegion(int id) {

        final ArrayList<String> regionName = new ArrayList<>();
        final ArrayList<Integer> regionId = new ArrayList<>();

        RetroWeb.getClient().create(ServiceApi.class)
                .getRegion(id, languagePrefManager.getAppLanguage()).enqueue(new Callback<RegionResponse>() {
            @Override
            public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {
                        for (int i = 0; i < response.body().getRegion().size(); i++) {
                            regionName.add(response.body().getRegion().get(i).getName());
                            regionId.add(response.body().getRegion().get(i).getId());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SeekerProfileEditActivity.this,
                                android.R.layout.simple_list_item_activated_1, regionName) {
                            @Override
                            public boolean isEnabled(int position) {
                                return true;
                            }
                        };
                        spinnerRegion.setAdapter(adapter);
                        if(clientRegisterModel.getFk_city()==id)
                        spinnerRegion.setSelection(regionName.indexOf(clientRegisterModel.getRegion()));
                        spinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int postion, long l) {


                                    idRegion = regionId.get(postion);

                                    Log.e("idRegion", idRegion + "");

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<RegionResponse> call, Throwable t) {
                Toast.makeText(SeekerProfileEditActivity.this, "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private boolean validation(){
        boolean flagEtPhone,flagEtNmae,flagEtpassword,checkCondtions,flagGender
                ,flagBirthDate,flagNationalId,
                flagCity,flagRejone,flagIdJob
                ,flagQoulifcationId,flagEmail,
                flagETnameUser,
                flagCourse;


        flagCourse= ValidationUtils.emptyValidation(edtCourse,getString(R.string.fill_empty));

        flagEtNmae=ValidationUtils.emptyValidation(edtName,getString(R.string.fill_empty));

        flagEmail=ValidationUtils.emptyValidation(edtMail,getString(R.string.fill_empty));


        flagETnameUser=ValidationUtils.emptyValidation(edtUserName,getString(R.string.fill_empty));

        if (flagEmail){
            flagEmail = ValidationUtils.validateEmail(edtMail ,getString(R.string.EmailValidation));
        }




        if (idCity==0){
            flagCity=false;
            CommonUtil.makeToast(SeekerProfileEditActivity.this,getString(R.string.must_chose_city));
        }else {
            flagCity=true;
        }


        if (idRegion==0){
            flagRejone=false;
            Log.e("idRegion",idRegion+"");
            CommonUtil.makeToast(SeekerProfileEditActivity.this,getString(R.string.must_chose_region));
        }else {
            flagRejone=true;
        }

        if (idJobTitle==0){
            flagIdJob=false;
            CommonUtil.makeToast(SeekerProfileEditActivity.this,getString(R.string.must_chose_job));
        }else {
            flagIdJob=true;
        }

        flagEtPhone= ValidationUtils.emptyValidation(edtPhone,getString(R.string.fill_empty));
        if (flagEtPhone){
            flagEtPhone= ValidationUtils.checkSize(edtPhone,getString(R.string.phone_size),10);
        }



        if (QULIFCATIONID.equals("0")){
            flagQoulifcationId=false;

            CommonUtil.makeToast(SeekerProfileEditActivity.this,getString(R.string.qoulifcation));

        }else{
            flagQoulifcationId=true;

        }


        return      flagEmail
                && flagEtNmae
                && flagEtPhone
                && flagCity
                && flagRejone
                && flagETnameUser
                && flagIdJob
                && flagQoulifcationId
                && flagCourse;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 1) {

            realImagePath = data.getStringArrayListExtra(Pix.IMAGE_RESULTS).get(0);

            if (realImagePath!=null){
                Picasso.get().load(realImagePath).into(imgProfile);
            }

        }

    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
