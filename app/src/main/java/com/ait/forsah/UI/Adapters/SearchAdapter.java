package com.ait.forsah.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.Models.Providers.ProviderSearchModel;
import com.ait.forsah.R;
import com.ait.forsah.UI.Activities.ProviderDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {

    private Context context;
    private List<ProviderSearchModel> list;


    public SearchAdapter(Context context, List<ProviderSearchModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SearchViewHolder(LayoutInflater.from(context).inflate(R.layout.search_result_item,parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, final int position) {
        holder.tvUserName.setText(list.get(position).getUser_name());
        holder.tvPrice.setText(list.get(position).getExpected_salary());
        holder.tvAbout.setText(list.get(position).getAbout());
//        holder.imageProfile.setImageResource(list.get(position).getImg());

        Picasso.get().load(list.get(position).getImg()).into(holder.imageProfile);


        holder.tvHint.setText(context.getString(R.string.expectedSalary));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i;
                i = new Intent(context, ProviderDetailsActivity.class);
                i.putExtra("jobId",String.valueOf(list.get(position).getId()));
                Log.e("id",String.valueOf(list.get(position).getId()));
                context.startActivity(i);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageProfile;
        private TextView tvUserName;
        private TextView tvPrice;
        private TextView tvAbout;
        private TextView tvHint;


        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);

            imageProfile = itemView.findViewById(R.id.img_profile);
            tvUserName = itemView.findViewById(R.id.tv_user_name);
            tvPrice = itemView.findViewById(R.id.tv_price);
            tvAbout = itemView.findViewById(R.id.tv_description);
            tvHint =itemView.findViewById(R.id.tv_hint);


        }
    }

}
