package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;

public class SplashActivity extends AppCompatActivity {


    private final int SPLASH_SCREEN_TIMER = 3000;

    private static final String MYPREFERENCE = "myPref";
    private static final String LOG_IN = "loginKey";



    SharedPrefManager mSharedPreferences;
    protected SharedPrefManager mEditor;
    LanguagePrefManager mLanguagePrefManager;



    private boolean LongIn;


    Animation animationBackground;
    Animation animationLeft;
    Animation animationRight;
    ImageView imageViewRight;
    ImageView imageViewLeft;
    ImageView imageViewBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



        mSharedPreferences = new SharedPrefManager(this);
        mLanguagePrefManager = new LanguagePrefManager(this);
        mEditor=new SharedPrefManager(this);

       imageViewBackground = findViewById(R.id.img_background);
       imageViewLeft = findViewById(R.id.img_left);
       imageViewRight = findViewById(R.id.img_right);

        animationBackground = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.alpha);
        animationRight = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.from_right);
       animationLeft = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.from_left);

       imageViewBackground.startAnimation(animationBackground);
       imageViewRight.startAnimation(animationRight);
       imageViewLeft.startAnimation(animationLeft);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                Intent i = new Intent(SplashActivity.this,SignInActivity.class);
//                startActivity(i);
//                finish();


                    startActivity(new Intent(SplashActivity.this,ChangeLanguageActivity.class));
                    finish();



            }
        },SPLASH_SCREEN_TIMER);

    }
}
