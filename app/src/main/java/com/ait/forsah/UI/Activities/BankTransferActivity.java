package com.ait.forsah.UI.Activities;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.LinearLayout;
import android.widget.TextView;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.FileUtils;
import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.SettingResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.ProgressRequestBody;
import com.ait.forsah.R;
import com.ait.forsah.ValidationUtils;
import com.ait.forsah.base.ParentActivity;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.ACTION_DIAL;

public class BankTransferActivity extends ParentActivity  implements ProgressRequestBody.UploadCallbacks {



    private int IMAGE_CODE=1;

    Dialog myDialog;

    private Bitmap bitmap;
    String realImagePath;

    @BindView(R.id.et_banck_name)
    EditText etBanckName;



    @BindView(R.id.et_account_owner)
    EditText etAccountOwner;



    @BindView(R.id.et_account_num)
    EditText etAccountNum;


    @BindView(R.id.et_amount)
    EditText etAmount;



    @BindView(R.id.tv_img_transfer)
    TextView tvImgTransfer;


    @BindView(R.id.account_num)
    TextView account_num;

    @BindView(R.id.tv_cost)
    TextView tvCost;


    @BindView(R.id.btn_send)
    Button btnSend;


    @BindView(R.id.tv_app_bar)
    TextView tv_app_bar;

    @BindView(R.id.la_parent)
    LinearLayout laParent;

    @BindView(R.id.avi)
    AVLoadingIndicatorView avi;



    String phone;
    String fk_job;

    private String idOrder;


    @Override
    protected void initializeComponents() {

        idOrder=getIntent().getStringExtra("orderId");
        phone=getIntent().getStringExtra("phone");
        fk_job=getIntent().getStringExtra("fk_job");

        tv_app_bar.setText(getString(R.string.title_bank));

        contact();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_bank_transfer;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }







    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_CODE && resultCode == RESULT_OK &&data != null) {


            Uri selectedImage = data.getData();

            InputStream imageStream = null;
            try {
                imageStream = getContentResolver().openInputStream(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            bitmap = BitmapFactory.decodeStream(imageStream);

            realImagePath =FileUtils.getPath(BankTransferActivity.this, CommonUtil.getImageUri(mContext,bitmap));
           // realImagePath = FileUtils.getPath(BankTransferActivity.this, selectedImage);

        }
    }



    private void contact() {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getSetting(fk_job).enqueue(new Callback<SettingResponse>() {
            @Override
            public void onResponse(Call<SettingResponse> call, Response<SettingResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1 ){
                        tvCost.setText(response.body().getSetting().getCost()+"");
                        etAmount.setText(response.body().getSetting().getCost()+"");
                        account_num.setText(response.body().getSetting().getAccount_number());
                    }
                }
            }

            @Override
            public void onFailure(Call<SettingResponse> call, Throwable t) {
                 hideProgressDialog();
            }
        });
    }





    private void Pay(String idOrder,String clientId,String banckName,String ownerName,String num,String price) {
        laParent.setVisibility(View.GONE);
        avi.setVisibility(View.VISIBLE);

        Log.e("client_id",idOrder+"");
        File ImageFile = new File(realImagePath);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile,  BankTransferActivity.this);
//            ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, UserProfileFragment.this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).pay(idOrder,clientId,banckName,ownerName,num,price,mLanguagePrefManager.getAppLanguage(),filePart).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    laParent.setVisibility(View.VISIBLE);
                    avi.setVisibility(View.GONE);
                    if (response.body().getKey() == 1 ){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent i = new Intent(BankTransferActivity.this, HomeSeekerActivity.class);
                        startActivity(i);
                        finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());

                    }

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

                laParent.setVisibility(View.GONE);
                avi.setVisibility(View.VISIBLE);

                CommonUtil.makeToast(mContext,getString(R.string.error));


            }
        });
    }


    @OnClick(R.id.btn_send)
    void payAction(){
        if (
                ValidationUtils.emptyValidation(etAccountNum,getString(R.string.fill_empty))&&
                        ValidationUtils.emptyValidation(etAccountOwner,getString(R.string.fill_empty))&&
                        ValidationUtils.emptyValidation(etBanckName,getString(R.string.fill_empty)) ){
            if (realImagePath!=null) {
                Pay(idOrder, mSharedPrefManager.getUserData().getId() + "", etBanckName.getText().toString(), etAccountOwner.getText().toString(), etAccountNum.getText().toString(), etAmount.getText().toString());
            }else {
                CommonUtil.makeToast(BankTransferActivity.this, R.string.enterImg);
            }
        }
    }


    @OnClick(R.id.tv_img_transfer)
    void  Img(){
        //  Util.makeToast(this,">>>>>>>>>>");
        try {
            if (ActivityCompat.checkSelfPermission(BankTransferActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(BankTransferActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 30);
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(intent.ACTION_GET_CONTENT);
                startActivityForResult(intent,IMAGE_CODE);

            }
        } catch (Exception e) {e.printStackTrace();}
    }

    @OnClick(R.id.img_back)
    void btnBack(){
        onBackPressed();
    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }


    private void dialogPhone(){
        myDialog = new Dialog(this);

        myDialog.setContentView(R.layout.dialog_phone);
        myDialog.setCancelable(false);
        TextView tvPhone = myDialog.findViewById(R.id.tv_phone);
        Button btnBackToHome = myDialog.findViewById(R.id.btn_exit);


       tvPhone.setText(phone);

       tvPhone.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(ACTION_DIAL , Uri.parse("tel:" + tvPhone.getText().toString()));
               startActivity(intent);
           }
       });
       btnBackToHome.setOnClickListener(view -> {
           Intent i = new Intent(BankTransferActivity.this, HomeSeekerActivity.class);
           startActivity(i);
           finish();
       });
       myDialog.show();


    }
}
