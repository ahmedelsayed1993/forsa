package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.Models.Providers.ProviderAdvertsmentDetailsResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProviderDetailsAcceptActivity extends AppCompatActivity {


    private TextView tvAppBar;

    private TextView tvUserNSme;
    private TextView tvHoursePrice;
    private TextView tvAbout;
    private TextView tvPhone;
    private TextView tvAge;
    private TextView tvCity;
    private TextView tvRegion;
    private TextView tvDegree;
    private TextView tvExerience;
    private TextView tvSkills;
    private TextView tvhours;
    private TextView tvWorkDays;
    private ImageView imgProfile;
    private LinearLayout linearLayout;
    private ImageButton imgBack;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final String MYPREFERENCE = "myPref";
    private static final String USER_ID = "idKey";
    private static final String JOB_ID = "jobKey";

    private static final String LANG = "langKey";
    private String lang;
    private String idUser;
    private String jobId;



    SharedPrefManager mSharedPreferences;
    LanguagePrefManager mLanguagePrefManager;
    private int idNotfy;
    private int id_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_details_accept);

        mSharedPreferences = new SharedPrefManager(this);
        mLanguagePrefManager = new LanguagePrefManager(this);

        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.employee_title);



        Intent intent = getIntent();
        Integer i = intent.getIntExtra("status",1);
        int value = i.intValue();
        jobId = intent.getStringExtra("jobId");


        lang = mLanguagePrefManager.getAppLanguage();
        idUser = mSharedPreferences.getUserData().getId()+"";

        idNotfy= intent.getIntExtra("id_notfy",0);
        id_order= intent.getIntExtra("id_order",0);





        tvUserNSme = findViewById(R.id.tv_accept_name);
        tvHoursePrice = findViewById(R.id.tv_accept_hour_price);
        tvAbout = findViewById(R.id.tv_accept_about);
//        tvPhone = findViewById(R.id.tv_accept_phone);
        tvAge = findViewById(R.id.tv_accept_age);
        tvCity = findViewById(R.id.tv_accept_city);
        tvRegion = findViewById(R.id.tv_accept_region);
        tvDegree = findViewById(R.id.tv_accept_degree);
        tvExerience = findViewById(R.id.tv_accept_experience);
        tvSkills = findViewById(R.id.tv_accept_skill);
        tvhours = findViewById(R.id.tv_accept_hours);
//        tvWorkDays = findViewById(R.id.tv_accept_work_days);
        imgProfile = findViewById(R.id.img_accept);
//        linearLayout = findViewById(R.id.liner_phone);
//
        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        if (value == 1){
//            linearLayout.setVisibility(View.VISIBLE);
//        }else {
//            linearLayout.setVisibility(View.GONE);
//
//        }
//
//        Toast.makeText(this, idUser, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, jobId, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, lang, Toast.LENGTH_SHORT).show();



        displayData();

    }

    private void displayData() {

        RetroWeb.getClient().create(ServiceApi.class)
                .getProviderAdvDetail(jobId,idUser,lang,idNotfy,id_order).enqueue(new Callback<ProviderAdvertsmentDetailsResponse>() {
            @Override
            public void onResponse(Call<ProviderAdvertsmentDetailsResponse> call, Response<ProviderAdvertsmentDetailsResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() ==1){
                        tvUserNSme.setText(response.body().getResult().getUser_name());
                        tvHoursePrice.setText(response.body().getResult().getExpected_salary());
                        tvAbout.setText(response.body().getResult().getAbout());
                    //    tvPhone.setText(response.body().getResult().getPhone());
                        tvAge.setText(String.valueOf(response.body().getResult().getAge()));
                        tvCity.setText(response.body().getResult().getCity());
                        tvRegion.setText(response.body().getResult().getRegion());
                        tvDegree.setText(response.body().getResult().getQualification());
                        tvExerience.setText(response.body().getResult().getPrevious_experience());
                        tvSkills.setText(response.body().getResult().getEducational_courses());
                        tvhours.setText(response.body().getResult().getHours_no());
//                        StringBuilder stringBuilder = new StringBuilder();
//                        for (String day : response.body().getResult().getAvilable_days()){
//                            stringBuilder.append(day + ",");
//                            stringBuilder.lastIndexOf(day);
//                        }
//                        tvWorkDays.setText(stringBuilder);
                        Picasso.get().load(response.body().getResult().getImg()).into(imgProfile);

                    }else {
                        Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderAdvertsmentDetailsResponse> call, Throwable t) {

            }
        });
    }
}
