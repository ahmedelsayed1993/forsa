package com.ait.forsah.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.Models.Clients.ClientGetOrderModel;
import com.ait.forsah.R;
import com.ait.forsah.UI.Activities.OfferDetailsClientActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SeekerSearchAdapter extends RecyclerView.Adapter<SeekerSearchAdapter.SeekerViewHolder>  {
    private Context context;
    private List<ClientGetOrderModel>list;

    public SeekerSearchAdapter(Context context, List<ClientGetOrderModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public SeekerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SeekerViewHolder(LayoutInflater.from(context).inflate(R.layout.search_result_seeker_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull SeekerViewHolder holder, int position) {
        holder.tvCompanyName.setText(list.get(position).getName());
        holder.tvWorkHour.setText(list.get(position).getHours_no());
        holder.tvAboutCompany.setText(list.get(position).getAbout());
        Picasso.get().load(list.get(position).getImg()).into(holder.imageCompanyProfile);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, OfferDetailsClientActivity.class);
                intent.putExtra("orderId",list.get(position).getOrder_id());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SeekerViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageCompanyProfile;
        private TextView tvCompanyName;
        private TextView tvWorkHour;
        private TextView tvAboutCompany;
        public SeekerViewHolder(@NonNull View itemView) {
            super(itemView);

            imageCompanyProfile = itemView.findViewById(R.id.img_company_profile);
            tvCompanyName = itemView.findViewById(R.id.tv_company_name);
            tvWorkHour = itemView.findViewById(R.id.tv_work_hours);
            tvAboutCompany = itemView.findViewById(R.id.tv_company_description);        }
    }
}
