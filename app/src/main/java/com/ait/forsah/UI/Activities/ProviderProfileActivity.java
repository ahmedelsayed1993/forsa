package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.Models.Providers.ProviderGetBranchModel;
import com.ait.forsah.Models.Providers.ProviderGetDataOfProviderResponse;
import com.ait.forsah.Models.Providers.ProviderUpdateUserDataResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.UI.Adapters.BranchCountAdapter;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class   ProviderProfileActivity extends AppCompatActivity {


    private TextView tvAppBar;

    private ImageButton imgBack;
    BranchCountAdapter adapter;
    RecyclerView recyclerView;

    private List<ProviderGetBranchModel> branchModels;

    ImageButton buttonAdd;
    ImageButton buttonDelete;
    int count ;

    private EditText companyName;
    private EditText commercial;
    private EditText companyActivity;
    private EditText workHour;
    private EditText phone;
    private EditText email;
    private TextView branch;
    private TextView editBranch;
    private EditText username;
    private EditText password;
    private EditText about;

    private Button edit;

    SharedPrefManager mSharedPreferences;
    LanguagePrefManager mLanguagePrefManager;


    private static final String MYPREFERENCE = "myPref";
    private static final String USER_IMAGE = "imageKey";
    private static final String USER_ID = "idKey";
    private static final String LANG = "langKey";
    private String lang;

    private String user_id;

    private int branchCount;
    private CircleImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_profile);


        mSharedPreferences = new SharedPrefManager(this);
        mLanguagePrefManager = new LanguagePrefManager(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.title_profile);

        imgBack = findViewById(R.id.img_back);


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        lang = mLanguagePrefManager.getAppLanguage();
        user_id = mSharedPreferences.getUserData().getId()+"";
//        Log.e("<<<userIDDDD",user_id);
//        Toast.makeText(this, user_id, Toast.LENGTH_LONG).show();




//        recyclerView = findViewById(R.id.recycler_profile_branch_count);
//        buttonAdd = findViewById(R.id.img_btn_add);
//        buttonDelete = findViewById(R.id.img_btn_dec);



        imageView = findViewById(R.id.img_home_Profile);
        companyName = findViewById(R.id.edt_provider_profile_company_name);
        commercial = findViewById(R.id.edt_provider_profile_commercial);
        companyActivity = findViewById(R.id.edt_provider_profile_activity);
        workHour = findViewById(R.id.edt_provider_profile_work_hours);
        phone = findViewById(R.id.edt_provider_profile_phone);
        email = findViewById(R.id.edt_provider_profile_mail);
        branch = findViewById(R.id.tv_branch_count);
        editBranch = findViewById(R.id.et_edit_branch_count);
        username = findViewById(R.id.edt_provider_profile_user_name);
        password = findViewById(R.id.tv_update_provider_password);
        about = findViewById(R.id.edt_provider_profile_about);




//        password.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(ProviderProfileActivity.this,));
//            }
//        });

        edit = findViewById(R.id.btn_provider_profile_edit);


        displayUserData(user_id);

//        Picasso.get().load(imageUrl).into(imageView);



//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//                photoPickerIntent.setType("image/*");
//                startActivityForResult(photoPickerIntent, 1);
//            }
//        });


        //  textView = findViewById(R.id.tv_branch_count);
        //textView.setText(count);

//        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false));

//        buttonAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                count++;
//                adapter = new BranchCountAdapter(getApplicationContext(),count);
//                recyclerView.setAdapter(adapter);
//            }
//        });
//        buttonDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                count--;
//                adapter = new BranchCountAdapter(getApplicationContext(),count);
//                recyclerView.setAdapter(adapter);
//            }
//        });

        editBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(ProviderProfileActivity.this,EditBranchActivity.class);
//                        i.putExtra("branch",branchCount);
                startActivity(i);
            }
        });


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editProviderProfile();
            }
        });

    }
    private void editProviderProfile() {
        if (checkEditTextError(companyName,getString(R.string.requerd),getWindow()) ||
                checkEditTextError(commercial,getString(R.string.requerd),getWindow()) ||
                checkEditTextError(companyActivity,getString(R.string.requerd),getWindow()) ||
                checkEditTextError(workHour,getString(R.string.requerd),getWindow()) ||
                checkEditTextError(phone,getString(R.string.requerd),getWindow()) ||
                checkEditTextError(email,getString(R.string.requerd),getWindow()) ||
                checkEditTextError(username,getString(R.string.requerd),getWindow())){

            return;
        }else if(password.getText().toString().length() < 6) {
            password.setError("password minimum contain 6 character");
            password.requestFocus();
        }else
        {
            RetroWeb.getClient().create(ServiceApi.class)
                    .updateProviderData(
                            user_id,
                            companyName.getText().toString(),
                            commercial.getText().toString(),
                            companyActivity.getText().toString(),
                            workHour.getText().toString(),
                            phone.getText().toString(),
                            email.getText().toString(),
                            username.getText().toString(),
                            password.getText().toString(),
                            about.getText().toString())
                    .enqueue(new Callback<ProviderUpdateUserDataResponse>() {
                        @Override
                        public void onResponse(Call<ProviderUpdateUserDataResponse> call, Response<ProviderUpdateUserDataResponse> response) {
                            if (response.isSuccessful()){
                                if (response.body().getKey() ==1 ){
                                    branchCount = response.body().getProvider().getBranch_count();
                                    mSharedPreferences.setUserData(response.body().getProvider());
                                    Toast.makeText(ProviderProfileActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(ProviderProfileActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(ProviderProfileActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<ProviderUpdateUserDataResponse> call, Throwable t) {
                            Toast.makeText(ProviderProfileActivity.this, "Fail upload data ", Toast.LENGTH_SHORT).show();

                        }
                    });
        }
    }


    public static boolean checkEditTextError(EditText editText, String message, Window window) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError(message);
            if (editText.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return true;
        }
        return false;
    }


//    // chose image from gallery
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            try {
//                final Uri imageUri = data.getData();
//                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
//                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                imageView.setImageBitmap(selectedImage);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//                Toast.makeText(ProviderProfileActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
//            }
//
//        }else {
//            Toast.makeText(ProviderProfileActivity.this, "You haven't picked Image",Toast.LENGTH_LONG).show();
//        }
//
//    }


    private void displayUserData(String userId) {

        RetroWeb.getClient().create(ServiceApi.class)
                .getDataOfProvider(userId,lang).enqueue(new Callback<ProviderGetDataOfProviderResponse>() {
            @Override
            public void onResponse(Call<ProviderGetDataOfProviderResponse> call, Response<ProviderGetDataOfProviderResponse> response) {

                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        Toast.makeText(ProviderProfileActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        companyName.setText(response.body().getProvider().getName());
                        commercial.setText(response.body().getProvider().getCommercial_registration_no());
                        companyActivity.setText(response.body().getProvider().getActivity());
                        workHour.setText(response.body().getProvider().getWork_hours());
                        phone.setText(response.body().getProvider().getPhone());
                        email.setText(response.body().getProvider().getEmail());
                        branch.setText(response.body().getProvider().getBranch_count());
                        username.setText(response.body().getProvider().getUser_name());
                        about.setText(response.body().getProvider().getAbout());
                        int i = response.body().getProvider().getBranches().size();

//                                adapter = new BranchCountAdapter(getApplicationContext(),i);
//                                recyclerView.setAdapter(adapter);
//                                adapter = new BranchCountAdapter(getApplicationContext(),i,branchModels);
//                                adapter.onBindViewHolder(recyclerView.getAdapter().getItemViewType(i));

                    }else {
                        Toast.makeText(ProviderProfileActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {

                    Toast.makeText(ProviderProfileActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ProviderGetDataOfProviderResponse> call, Throwable t) {
                Toast.makeText(ProviderProfileActivity.this, "Fail to get user data ", Toast.LENGTH_SHORT).show();

            }
        });


    }

}
