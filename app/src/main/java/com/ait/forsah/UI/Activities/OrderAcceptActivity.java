package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ait.forsah.R;

public class OrderAcceptActivity extends AppCompatActivity {

    private TextView tvAppBar;
    private ImageButton imgBack;
    private Button btnPay;
    private String orderId;
    private TextView tvBackToHome;

    String phone;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_accept);


        tvAppBar = findViewById(R.id.tv_app_bar);
        tvAppBar.setText(R.string.accept_order);
        btnPay = findViewById(R.id.btnPay);

        imgBack = findViewById(R.id.img_back);

        orderId = getIntent().getStringExtra("orderId");
        phone = getIntent().getStringExtra("phone");


        Log.e("orderId",orderId+"");

        btnPay.setOnClickListener(view -> {
            Intent intent = new Intent(OrderAcceptActivity.this, BankTransferActivity.class);
            intent.putExtra("orderId", orderId);
            intent.putExtra("phone", phone);

            startActivity(intent);
            finish();
        });



//        tvBackToHome = findViewById(R.id.tv_back_home);
//        tvBackToHome.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(OrderAcceptActivity.this, HomeSeekerActivity.class);
//                startActivity(i);
//                finish();
//            }
//        });


        imgBack.setOnClickListener(view -> onBackPressed());

    }
}
