package com.ait.forsah.UI.Activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.CityResponse;
import com.ait.forsah.Models.JobTitleResponse;
import com.ait.forsah.Models.RegionResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.ValidationUtils;
import com.ait.forsah.base.ParentActivity;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddJobActivity extends ParentActivity {



    @BindView(R.id.sp_city)
    Spinner spCity;

    @BindView(R.id.sp_job)
    Spinner spJob;



    @BindView(R.id.sp_rejoin)
    Spinner spRejoin;

    @BindView(R.id.et_expected_salary)
    EditText etExpectedSalary;


    @BindView(R.id.et_ours)
    EditText etOurs;



    @BindView(R.id.edt_end_date)
    EditText etEndDate;

    @BindView(R.id.edt_from_date)
    EditText etFromDate;

    @BindView(R.id.edt_attendees)
    EditText edtAttendees;

    @BindView(R.id.edt_leave)
    EditText edtLeave;

    String day,month,year,minuts,hours;
    String startDate,endDate,timeLeave,timeAttendess;



    int idCity=0;
    int idRegion=0;
    int idJob=0;


    @Override
    protected void initializeComponents() {
        displayCity();
        displayJobTitle();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_job;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }





    private void displayJobTitle() {
        final ArrayList<String> jobName = new ArrayList<>();
        jobName.add(0,getString(R.string.spinner_job_title));
        final ArrayList<Integer> jobId = new ArrayList<>();
        RetroWeb.getClient().create(ServiceApi.class)
                .getJobTitle("ar").enqueue(new Callback<JobTitleResponse>() {
            @Override
            public void onResponse(Call<JobTitleResponse> call, Response<JobTitleResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        for (int i=0; i<response.body().getJob_title().size();i++){
                            jobName.add(response.body().getJob_title().get(i).getName());
                            jobId.add(response.body().getJob_title().get(i).getId());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddJobActivity.this,
                                android.R.layout.simple_list_item_activated_1,jobName){
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        spJob.setAdapter(adapter);
                        spJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                if (position>0){
                                    String name = jobName.get(position);
                                    idJob = jobId.get(position-1);
//                                    Toast.makeText(RegisterAsSeekerActivity.this, String.valueOf(id), Toast.LENGTH_SHORT).show();

                                    Log.e("jobId", idJob+"");

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<JobTitleResponse> call, Throwable t) {

            }
        });

    }

    private void displayCity() {
        final ArrayList<String> cityName = new ArrayList<>();
        cityName.add(0,getString(R.string.spinner_city));
        final ArrayList<Integer> cityId = new ArrayList<>();
        cityId.add(0,0);
        RetroWeb.getClient().create(ServiceApi.class)
                .getCity("ar").enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){

                        for (int i =0;i<response.body().getCity().size();i++){
                            cityName.add(response.body().getCity().get(i).getName());
                            cityId.add(response.body().getCity().get(i).getId());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddJobActivity.this,
                                android.R.layout.simple_list_item_activated_1,cityName){
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        spCity.setAdapter(adapter);
                        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                if (position>0){
                                    String city = cityName.get(position);
                                    idCity = cityId.get(position);
                                    displayRegion(idCity);


                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });
    }

    private void displayRegion(int id) {

        final ArrayList<String> regionName = new ArrayList<>();
        regionName.add(0,getString(R.string.spinner_region) );
        final ArrayList<Integer> regionId = new ArrayList<>();
        regionId.add(0,0);


        RetroWeb.getClient().create(ServiceApi.class)
                .getRegion(id,"ar").enqueue(new Callback<RegionResponse>() {
            @Override
            public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        for (int i=0;i<response.body().getRegion().size();i++){
                            regionName.add(response.body().getRegion().get(i).getName());
                            regionId.add(response.body().getRegion().get(i).getId());
                        }
                        ArrayAdapter<String > adapter = new ArrayAdapter<String >(AddJobActivity.this,
                                android.R.layout.simple_list_item_activated_1,regionName){
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        spRejoin.setAdapter(adapter);
                        spRejoin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                if (position>0){
                                    String region = regionName.get(position);
                                    idRegion = regionId.get(position);
//                                    Toast.makeText(getApplicationContext(),String.valueOf(id),Toast.LENGTH_LONG).show();

                                    Log.e("idRegion", idRegion+"");
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<RegionResponse> call, Throwable t) {
                Toast.makeText(AddJobActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @OnClick(R.id.edt_from_date)
    void  fromDate(){


        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dlg;
        dlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(0);
                cal.set(year, month, dayOfMonth, 0, 0, 0);
                Date chosenDate = cal.getTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyy", Locale.US);
                startDate=dateFormat.format(chosenDate);

                etFromDate.setText(dateFormat.format(chosenDate));
                /// DateFormat df_medium_us = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
                //   tvDetectDate.setText(df_medium_us.format(chosenDate));
            }
        }, year, month,day);
        //  dlg.setTitle(getString(R.string.dtect_date));
        dlg.show();
//        new SingleDateAndTimePickerDialog.Builder(this)
//                .bottomSheet()
//                .curved()
//                .displayMinutes(false)
//                .displayHours(false)
//                .displayDays(false)
//                .displayMonth(true)
//                .displayYears(true)
//                .displayDaysOfMonth(true)
//                .listener(new SingleDateAndTimePickerDialog.Listener() {
//                    @Override
//                    public void onDateSelected(Date date) {
//
//                        day=date.getDay()+"";
//                        month=date.getMonth()+"";
//                        year=date.getYear()+"";
//                        startDate=day+"-"+month+"-"+year;
//
//
//
//                      //  etFromDate.setText(day+"/"+month+"/"+year);
//
//                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyy", Locale.US);
//                        startDate=dateFormat.format(date);
//
//                        SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd/yyy",Locale.US);
//                        String finalDate = timeFormat.format(date);
//
//                        etFromDate.setText(finalDate);
////                        Log.e("hhhh",finalDate);
//                    }
//                })
//                .display();
    }


    @OnClick(R.id.edt_end_date)
    void  toDate(){


        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dlg;
        dlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(0);
                cal.set(year, month, dayOfMonth, 0, 0, 0);
                Date chosenDate = cal.getTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyy", Locale.US);
                endDate=dateFormat.format(chosenDate);

                etEndDate.setText(dateFormat.format(chosenDate));
                /// DateFormat df_medium_us = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
                //   tvDetectDate.setText(df_medium_us.format(chosenDate));
            }
        }, year, month,day);
      //  dlg.setTitle(getString(R.string.dtect_date));
        dlg.show();

//        new SingleDateAndTimePickerDialog.Builder(this)
//                .bottomSheet()
//                .curved()
//                .displayMinutes(false)
//                .displayHours(false)
//                .displayDays(false)
//                .displayMonth(true)
//                .displayYears(true)
//                .displayDaysOfMonth(true)
//                .listener(new SingleDateAndTimePickerDialog.Listener() {
//                    @Override
//                    public void onDateSelected(Date date) {
//
//                        day=date.getDay()+"";
//                        month=(date.getMonth()+1)+"";
//                        year=date.getYear()+"";
//                   //     endDate=day+"-"+month+"-"+year;
//
//                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyy", Locale.US);
//                        endDate=dateFormat.format(date);
//                        Log.e("hhhh",endDate);
//
//
////                        Date myDate = null;
////                        try { myDate = dateFormat.parse(endDate); } catch (ParseException e) { e.printStackTrace(); }
//                        SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd/yyy",Locale.US);
//                        String finalDate = timeFormat.format(date);
//                        etEndDate.setText(finalDate);
//                        Log.e("hhhh",finalDate);
//
//                    }
//                })
//                .display();
    }


    @OnClick(R.id.edt_leave)
    void  setTimeLeave(){


        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog dlg;
        TimePickerDialog.OnTimeSetListener myTimeListener  = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {

                if (timePicker.isShown()) {
                    mcurrentTime.set(Calendar.HOUR_OF_DAY, hour);
                    mcurrentTime.set(Calendar.MINUTE, minute);

                    edtLeave.setText( getTime(i,i1));
                }
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, false);
        timePickerDialog.setTitle(getString(R.string.choose_time));
        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        timePickerDialog.show();

//        new SingleDateAndTimePickerDialog.Builder(this)
//                .bottomSheet()
//                .curved()
//                .displayMinutes(true)
//                .displayHours(true)
//                .displayDays(false)
//                .displayMonth(false)
//                .displayYears(false)
//                .displayDaysOfMonth(false)
//                .listener(new SingleDateAndTimePickerDialog.Listener() {
//                    @Override
//                    public void onDateSelected(Date date) {
//
//                        minuts=date.getMinutes()+"";
//                        hours=date.getHours()+"";
//                      //  timeLeave=minuts+"-"+hours;
//                       // edtLeave.setText(date.getTime()+"");
//
//                        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm a", Locale.US);
//                        timeLeave=dateFormat.format(date);
//                        Log.e("hhhh",timeLeave);
//
////                        Date myDate = null;
////                        try { myDate = dateFormat.parse(endDate); } catch (ParseException e) { e.printStackTrace(); }
//                        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm a",Locale.US);
//                        String finalDate = timeFormat.format(date);
//                        edtLeave.setText(finalDate);
//                        Log.e("hhhh",finalDate);
//
//                    }
//                })
//                .display();
    }


    @OnClick(R.id.edt_attendees)
    void  setTimeAttendess(){


            Calendar mcurrentTime = Calendar.getInstance();

            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog dlg;
            TimePickerDialog.OnTimeSetListener myTimeListener  = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i1) {

                        if (timePicker.isShown()) {
                            mcurrentTime.set(Calendar.HOUR_OF_DAY, hour);
                            mcurrentTime.set(Calendar.MINUTE, minute);

                            edtAttendees.setText( getTime(i,i1));
                        }
                }
            };
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, false);
        timePickerDialog.setTitle(getString(R.string.choose_time));
        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        timePickerDialog.show();
//        {
//                @SuppressLint("DefaultLocale")
//                @Override
//                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
////                    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm a",Locale.forLanguageTag("en"));
////                    String finalDate = timeFormat.format(timePicker);
//                  //  edtAttendees.setText(finalDate);
////                  time=selectedHour+":"+selectedMinute;
//
//                    timeAttendess=getTime(selectedHour,selectedMinute);
//                    edtAttendees.setText( getTime(selectedHour,selectedMinute));
//                }
//            }, hour, minute, true);

           // dlg.setTitle(getString(R.string.dtect_date));


//        new SingleDateAndTimePickerDialog.Builder(mContext)
//                .bottomSheet()
//                .curved()
//                .customLocale(Locale.getDefault())
//                .displayMinutes(true)
//                .displayAmPm(true)
//                .displayHours(true)
//                .displayDays(false)
//
//                .displayMonth(false)
//                .displayYears(false)
//                .displayDaysOfMonth(false)
//                .listener(new SingleDateAndTimePickerDialog.Listener() {
//                    @Override
//                    public void onDateSelected(Date date) {
//
//                        minuts=date.getMinutes()+"";
//                        hours=date.getHours()+"";
////                        timeAttendess=minuts+"-"+hours;
////                        edtAttendees.setText(timeAttendess+"");
//
//                        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm a", Locale.getDefault());
//                        timeAttendess=dateFormat.format(date);
//                        Log.e("hhhh",timeAttendess);
//
////                        Date myDate = null;
////                        try { myDate = dateFormat.parse(endDate); } catch (ParseException e) { e.printStackTrace(); }
//                        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm a",Locale.forLanguageTag("en"));
//                        String finalDate = timeFormat.format(date);
//                        edtAttendees.setText(finalDate);
//                        Log.e("hhhh",finalDate);
//                    }
//                })
//                .display();
    }


    @OnClick(R.id.img_back)
    void  back(){
        onBackPressed();
    }

    private boolean validation(){

        boolean  flagTimeLeve,flagTimeAttendess,flagStartDate,FlagEndDate,flagExpectedSalary,flagCity,flagJob,flagRejoin,flagOurs;



        if (idJob==0){
            CommonUtil.makeToast(mContext,"من فضلك اختر الوظيفه");
            flagJob=false;
            return flagJob;
        }else {
            flagJob= true;

        }


        if (idCity==0){
            CommonUtil.makeToast(mContext,"من فضلك اختر المدينة");
            flagCity=false;
            return flagJob;
        }else {
            flagCity= true;

        }

        if (idRegion==0){
            CommonUtil.makeToast(mContext,"من فضلك اختر الحي");
            flagRejoin=false;
            return flagJob;
        }else {
            flagRejoin= true;

        }

        flagStartDate= ValidationUtils.emptyValidation(etFromDate,"من فضلك اختر تاريخ بداية الفترة");

        FlagEndDate=ValidationUtils.emptyValidation(etEndDate,"من فضلك اختر تاريخ نهاية الفترة");

        flagTimeAttendess=ValidationUtils.emptyValidation(edtAttendees,"من فضلك اختر وقت الحضور");

        flagTimeLeve = ValidationUtils.emptyValidation(edtLeave,"من فضلك اختر وقت انصرافك");

        flagExpectedSalary = ValidationUtils.emptyValidation(etExpectedSalary,"من فضلك قم بتحديد الراتب المتوقع");

        flagOurs  = ValidationUtils.emptyValidation(etOurs,"من فضلك قم بتحديد عدد السعات ");

        return  flagTimeLeve && flagTimeAttendess && flagStartDate && FlagEndDate && flagExpectedSalary  && flagOurs;


    }


    @OnClick(R.id.btnAdd)
    void btnAdd(){
        if (validation()){

            addAdvtiseMent(idCity+""
                    , idRegion+""
                    ,startDate
                    ,endDate
                    ,etOurs.getText().toString()
                    ,timeAttendess
                    ,timeLeave
                    ,etExpectedSalary.getText().toString()
                    ,mSharedPrefManager.getUserData().getId()+""
                    ,idJob+"");

        }
    }


    private void addAdvtiseMent(String  cityId ,String regionId , String startDate , String endDate , String hours
            , String timeAttendess ,String timeLeave ,String expectedSalary,String clintId , String jobTitle ){

        showProgressDialog(getString(R.string.pleaseWait));

        RetroWeb.getClient()
                .create(ServiceApi.class)
                .AddAdvertsment(cityId,regionId,startDate,endDate,hours,timeAttendess,timeLeave,
                        expectedSalary,clintId,mLanguagePrefManager.getAppLanguage(),jobTitle)
                .enqueue(new Callback<BaseResponse>() {

                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getKey() == 1) {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                                finish();
                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        hideProgressDialog();
                    }
                });


    }


    private String getTime(int hr,int min) {
        Time tme = new Time(hr,min,0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a",Locale.ENGLISH);
        return formatter.format(tme);
    }

}



