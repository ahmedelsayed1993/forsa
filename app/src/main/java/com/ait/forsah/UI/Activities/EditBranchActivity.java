package com.ait.forsah.UI.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Listeners.SpinerCityAndRejone;
import com.ait.forsah.Models.CityModel;
import com.ait.forsah.Models.CityResponse;
import com.ait.forsah.Models.GetBranchesModel;
import com.ait.forsah.Models.GetBranchesResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.UI.Adapters.GetBranchesAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditBranchActivity extends AppCompatActivity  implements SpinerCityAndRejone {


    SharedPrefManager sharedPrefManager;
    LanguagePrefManager languagePrefManager;

    ArrayList<CityModel> cityName;
    ArrayList<Integer> cityId ;
    RecyclerView recyclerView;

    TextView tv_add;

    ArrayList<GetBranchesModel> getBranchesModel;

    Button btn_edit;

    ImageView img_back;

    GetBranchesAdapter getBranchesAdapter;
    TextView tv_app_bar ;

    private static final String MYPREFERENCE = "myPref";
    private static final String BRANCH_COUNT = "branchKey";
    private SharedPreferences.Editor editor;
    private String branchCount;

    LinearLayoutManager branhesManger;
    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_branch);
        tv_add=findViewById(R.id.tv_add);
        img_back=findViewById(R.id.img_back);
        btn_edit = findViewById(R.id.btn_edit);
        tv_app_bar=findViewById(R.id.tv_app_bar);
        tv_app_bar.setText(getString(R.string.editeBranche));
        sharedPrefManager=new SharedPrefManager(this);
        languagePrefManager = new LanguagePrefManager(this);
        recyclerView=findViewById(R.id.recycler_profile_branch_count);
        getBranchesModel = new ArrayList<>();



        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_edit.setOnClickListener(view -> {

            update( new Gson().toJson(getBranchesModel));

        });
        
        displayCity();

        branhesManger = new LinearLayoutManager(EditBranchActivity.this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(branhesManger);



        tv_add.setOnClickListener(view -> {
            Intent intent = new Intent(EditBranchActivity.this, AddBranchActivity.class);
           intent.putExtra("branches",getBranchesModel);
            startActivity(intent);
        });


//        Log.e("<<Branch",branchCount);

    }



    private void displayCity() {

        cityName = new ArrayList<>();
        cityId = new ArrayList<>();
        RetroWeb.getClient().create(ServiceApi.class)
                .getCity(languagePrefManager.getAppLanguage()).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        cityName=response.body().getCity();
                        for (int i =0;i<response.body().getCity().size();i++){

                            cityId.add(response.body().getCity().get(i).getId());
                        }

                        getBranches();


                        Log.e("She7ata","city");
                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });
    }
    
    
    
    private void getBranches(){
        
        RetroWeb.getClient().create(ServiceApi.class)
                .getBranch(sharedPrefManager.getUserData().getId(),languagePrefManager.getAppLanguage())
                .enqueue(new Callback<GetBranchesResponse>() {
                    @Override
                    public void onResponse(Call<GetBranchesResponse> call, Response<GetBranchesResponse> response) {


                                getBranchesModel = response.body().getBranches();
                                getBranchesAdapter = new GetBranchesAdapter(EditBranchActivity.this,getBranchesModel,cityName,cityId, EditBranchActivity.this);
                                recyclerView.setAdapter(getBranchesAdapter);




                          }



                    @Override
                    public void onFailure(Call<GetBranchesResponse> call, Throwable t) {
                        Log.e("She7ata","lhl");
                        Log.e("She7ata",t+"");
                    }
                });
        
    }

    @Override
    public void Rejone(int position, int portionItem, String cityName) {

        getBranchesModel.remove(position);
        getBranchesAdapter.notifyDataSetChanged();

    }


    private void update(String branches){
        RetroWeb.getClient().create(ServiceApi.class)
                .updateBranches(sharedPrefManager.getUserData().getId()+"",branches)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        CommonUtil.makeToast(EditBranchActivity.this,response.body().getMsg());
                        finish();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {

                    }
                });
    }


    @Override
    protected void onResume() {
        super.onResume();

        getBranchesModel.clear();
        cityName.clear();
        cityId.clear();
        displayCity();

    }
}
