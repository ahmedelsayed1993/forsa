package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassword2Activity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private static final String MYPREFERENCE = "myPref";
    private static final String LANG = "langKey";
    private String lang;

    private EditText edtCode;
    private EditText edtEnterPassword;
    private EditText edtReEnterPassword;
    Button button;

    int value;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password2);


        sharedPreferences = getSharedPreferences(MYPREFERENCE, Context.MODE_PRIVATE);
        lang = sharedPreferences.getString(LANG,null);

        Intent intent = getIntent();
        value = intent.getIntExtra("type",1);
        edtCode = findViewById(R.id.edt_code);
        edtEnterPassword = findViewById(R.id.edt_new_password);
        edtReEnterPassword = findViewById(R.id.edt_reenter_password);
        button = findViewById(R.id.btn_confirm_password);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(ResetPassword2Activity.this,SignInActivity.class);
//                startActivity(i);
                if (value==1){
                    confirmPasswordsecker();
                }  else {
                    confirmPassword();
                }


            }
        });


    }

    private void confirmPasswordsecker() {
        if (checkEditTextError(edtCode,getString(R.string.requerd),getWindow())||
        checkEditTextError(edtEnterPassword,getString(R.string.requerd),getWindow())||
        checkEditTextError(edtReEnterPassword,getString(R.string.requerd),getWindow())){
            return;
        }
        else if (!edtEnterPassword.getText().toString().equals(edtReEnterPassword.getText().toString())){
            Toast.makeText(this, getString(R.string.pass_not), Toast.LENGTH_SHORT).show();
        }else if (edtEnterPassword.getText().toString().length() < 6) {
            edtEnterPassword.setError("password minimum contain 6 character");
                    edtEnterPassword.requestFocus();
        }

        else {
            RetroWeb.getClient().create(ServiceApi.class)
                    .changeClientPasswordByCode(edtCode.getText().toString(),
                            edtEnterPassword.getText().toString(),lang).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body().getKey() == 1){
                            Toast.makeText(ResetPassword2Activity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ResetPassword2Activity.this,SignInActivity.class));
                            finish();
                        }else {
                            Toast.makeText(ResetPassword2Activity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {

                }
            });
        }
    }


    private void confirmPassword() {
        if (checkEditTextError(edtCode,getString(R.string.requerd),getWindow())||
                checkEditTextError(edtEnterPassword,getString(R.string.requerd),getWindow())||
                checkEditTextError(edtReEnterPassword,getString(R.string.requerd),getWindow())){
            return;
        }
        else if (!edtEnterPassword.getText().toString().equals(edtReEnterPassword.getText().toString())){
            Toast.makeText(this, getString(R.string.pass_not), Toast.LENGTH_SHORT).show();
        }else if (edtEnterPassword.getText().toString().length() < 6) {
            edtEnterPassword.setError("password minimum contain 6 character");
            edtEnterPassword.requestFocus();
        }

        else {
            RetroWeb.getClient().create(ServiceApi.class)
                    .changePasswordByCode(edtCode.getText().toString(),
                            edtEnterPassword.getText().toString(),lang).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body().getKey() == 1){
                            Toast.makeText(ResetPassword2Activity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ResetPassword2Activity.this,SignInActivity.class));
                        }else {
                            Toast.makeText(ResetPassword2Activity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {

                }
            });
        }
    }


    public static boolean checkEditTextError(EditText editText, String message, Window window) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError(message);
            if (editText.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return true;
        }
        return false;
    }

}
