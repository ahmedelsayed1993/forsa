package com.ait.forsah.UI.Activities;

import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.Models.Clients.AdvertDescriptionModel;
import com.ait.forsah.Models.Clients.AdvertDescriptionResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.base.ParentActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyAdvertiseMeantDetailsActivity extends ParentActivity {



    @BindView(R.id.tv_name)
    TextView  tv_name ;


    @BindView(R.id.tv__hour_price)
    TextView  tv__hour_price ;

    @BindView(R.id.tv_about)
    TextView  tv_about ;


    @BindView(R.id.tv_age)
    TextView  tv_age ;


    @BindView(R.id.tv_city)
    TextView  tv_city;



    @BindView(R.id.tv_region)
    TextView  tv_region ;


    @BindView(R.id.tv_degree)
    TextView tv_degree ;


    @BindView(R.id.tv_experience)
    TextView  tv_experience ;


    @BindView(R.id.tv_skill)
    TextView  tv_skill ;



    @BindView(R.id.tv_hours)
    TextView  tv_hours ;


    @BindView(R.id.tv_app_bar)
    TextView  tv_app_bar ;




    @BindView(R.id.img_accept)
    CircleImageView img_accept;

    AdvertDescriptionModel mResult;


    
    private int idAdvertiseMent; 



    @Override
    protected void initializeComponents() {

        idAdvertiseMent =getIntent().getIntExtra("jobId",0);
        tv_app_bar.setText(getString(R.string.Employee_details));

        getAdvetisement();



    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_my_add_detaile;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    private void getAdvetisement() {
        RetroWeb.getClient().create(ServiceApi.class)
                .AdvertDescription(idAdvertiseMent,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<AdvertDescriptionResponse>() {
            @Override
            public void onResponse(Call<AdvertDescriptionResponse> call, Response<AdvertDescriptionResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){

                        mResult =response.body().getResult();
                        setDetailes();

                    }else {
                        Toast.makeText(MyAdvertiseMeantDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(MyAdvertiseMeantDetailsActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AdvertDescriptionResponse> call, Throwable t) {
                Toast.makeText(MyAdvertiseMeantDetailsActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.img_back)
    void back(){
        onBackPressed();
    }


    private void setDetailes(){
        tv_name.setText(mResult.getName());
        tv__hour_price.setText(mResult.getHours_no());
        tv_about.setText(mResult.getAbout());
        tv_age.setText(mResult.getAge()+"");
        tv_city.setText(mResult.getCity());
        tv_region.setText(mResult.getRegion());
        tv_degree.setText(mResult.getQualification());
        tv_experience.setText(mResult.getPrevious_experience());
        tv_skill.setText(mResult.getEducational_courses());
        tv_hours.setText(mResult.getHours_no());

        Picasso.get()
                .load(mResult.getImg())
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .into(img_accept);
    }

}
