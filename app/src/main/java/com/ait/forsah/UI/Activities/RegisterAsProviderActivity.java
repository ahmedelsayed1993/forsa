package com.ait.forsah.UI.Activities;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;


import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Listeners.SpinerCityAndRejone;
import com.ait.forsah.Models.BranchCountModel;
import com.ait.forsah.Models.BranchCountModelSupmet;
import com.ait.forsah.Models.CityResponse;
import com.ait.forsah.Models.Providers.ProviderGetBranchModel;
import com.ait.forsah.Models.Providers.ProvidersRegisterResponse;
import com.ait.forsah.Models.RegionResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.UI.Adapters.BranchCountAdapter;
import com.ait.forsah.ValidationUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterAsProviderActivity extends AppCompatActivity implements SpinerCityAndRejone {

    private TextView tvAppBar;
    private ImageButton imgButtonBack;
    ArrayList<String> cityName;
    ArrayList<Integer> cityId ;

    ArrayList<String> regionName;
    ArrayList<Integer> regionId ;
    private List<ProviderGetBranchModel> list ;

    private ScrollView relativeLayout;
    private AVLoadingIndicatorView avi;

    List<BranchCountModel> branches;

    LinearLayoutManager branhesManger;


    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private static final String MYPREFERENCE = "myPref";
    private static final String USER_NAME = "nameKey";
    private static final String USER_ID = "idKey";
    private static final String PASSWORD = "passWordKey";
    private static final String IMG_PROFILE = "imgKey";
    private static final String LANG = "langKey";

    BranchCountAdapter adapterBranches , countAdapter ;
    RecyclerView recyclerView;

    private String lang;

    int count;

    private EditText companyName;
    private EditText commercial;
    private EditText companyActivity;
    private EditText workHour;
    private EditText phone;
    private EditText email;
    private Spinner spinnerBranchConut;
    private Spinner spinnerCity;
    private Spinner spinnerRegion;
    private EditText username;
    private EditText password;
    private Button register;
    private TextView tvPolicy;
    private CheckBox chAccept;

    LanguagePrefManager languagePrefManager;

    SharedPrefManager mSharedPreferences;
    private String mDeviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_as_provider);


        CommonUtil.setupUI(findViewById(R.id.ProParent),this);

        mDeviceId= FirebaseInstanceId.getInstance().getToken();

        mSharedPreferences = new SharedPrefManager(this);

        languagePrefManager= new LanguagePrefManager(this);
        branches = new ArrayList<>();
        tvAppBar = findViewById(R.id.tv_app_bar_register);
        tvAppBar.setText(R.string.register_as_employer);
        imgButtonBack = findViewById(R.id.img_button_back);
        imgButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        relativeLayout = findViewById(R.id.relative_layout);

        final String indicator=getIntent().getStringExtra("indicator");
        avi= findViewById(R.id.avi);
        avi.setIndicator(indicator);
        companyName = findViewById(R.id.edt_company_name);
        commercial = findViewById(R.id.edt_commercial);
        companyActivity = findViewById(R.id.edt_activity);
        workHour = findViewById(R.id.edt_work_hours);
        phone = findViewById(R.id.edt_phone_number);
        email = findViewById(R.id.edt_email);
        spinnerBranchConut = findViewById(R.id.spn_register_branch_count);
        spinnerCity = findViewById(R.id.spin_register_city);
        spinnerRegion = findViewById(R.id.spin_register_region);
        username = findViewById(R.id.edt_username);
        password = findViewById(R.id.edt_password);
        register = findViewById(R.id.btn_register_provider);
        chAccept =findViewById(R.id.chAccept);


        tvPolicy = findViewById(R.id.tv_provider_policy);

        recyclerView = findViewById(R.id.recycler_branch_count);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));

        sharedPreferences = getSharedPreferences(MYPREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        lang = languagePrefManager.getAppLanguage();
        list = new ArrayList<>();


        tvPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RegisterAsProviderActivity.this,PolicyActivity.class);
                i.putExtra("home",3);
                startActivity(i);
            }
        });




        displayCity();

        ProviderGetBranchModel model = new ProviderGetBranchModel();
        model.setStreet("");
        model.setWork_hours("");
        list.add(model);


        ArrayList<String> number = new ArrayList<>();
        number.add(getString(R.string.numBranche));
        number.add("1");
        number.add("2");
        number.add("3");

       
//        recyclerView.setAdapter(adapter);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, number) {

            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }
//
//
        };

        spinnerBranchConut.setAdapter(arrayAdapter);
        spinnerBranchConut.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position!=0){
                    branches.clear();
                    for (int i=0;i<Integer.valueOf(number.get(position));i++){
                        BranchCountModel branchCountModel = new BranchCountModel();
                        branchCountModel.setCitySpinner(cityName);
                        branchCountModel.setCityId(cityId);

                        branches.add(branchCountModel);
                        branhesManger = new LinearLayoutManager(RegisterAsProviderActivity.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(branhesManger);
                        adapterBranches = new BranchCountAdapter(RegisterAsProviderActivity.this,branches,RegisterAsProviderActivity.this);
                        recyclerView.setAdapter(adapterBranches);
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerNewAccount();
            }
        });

    }
    void registerNewAccount() {
        ArrayList<BranchCountModelSupmet> branchCountModelSupmetArrayList = new ArrayList<BranchCountModelSupmet>();
        relativeLayout.setVisibility(View.INVISIBLE);
        avi.smoothToShow();
        if (validation()) {
            for (int i = 0; i < branches.size(); i++) {
                BranchCountModelSupmet branchCountModelSupmet = new BranchCountModelSupmet();
                branchCountModelSupmet.setCity_id(branches.get(i).getCitid());
                branchCountModelSupmet.setRegion(branches.get(i).getRejonid());
                branchCountModelSupmet.setStreet(branches.get(i).getStreet());
                branchCountModelSupmet.setWork_hours(branches.get(i).getWorkHours());
                branchCountModelSupmetArrayList.add(branchCountModelSupmet);

            }


            RetroWeb.getClient().create(ServiceApi.class)
                    .registerAsPProvider(companyName.getText().toString(), commercial.getText().toString(), companyActivity.getText().toString(),
                            workHour.getText().toString(), phone.getText().toString(), email.getText().toString(), spinnerBranchConut.getSelectedItem().toString(),

                            new Gson().toJson(branchCountModelSupmetArrayList), username.getText().toString(), password.getText().toString(),mDeviceId,true)
                    .enqueue(new Callback<ProvidersRegisterResponse>() {
                        @Override
                        public void onResponse(Call<ProvidersRegisterResponse> call, Response<ProvidersRegisterResponse> response) {
                            if (response.isSuccessful()) {
                                if (response.body().getKey() == 1) {

                                    mSharedPreferences.setLoginStatus(true);
                                    mSharedPreferences.setUserData(response.body().getProvider());
                                    startActivity(new Intent(RegisterAsProviderActivity.this, HomeActivity.class));

                                    Toast.makeText(RegisterAsProviderActivity.this, getString(R.string.SucessRegester), Toast.LENGTH_SHORT).show();
                                            finish();
                                } else {
                                    relativeLayout.setVisibility(View.VISIBLE);
                                    avi.smoothToHide();
                                    Toast.makeText(RegisterAsProviderActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<ProvidersRegisterResponse> call, Throwable t) {
                        }
                    });
        }else {
            relativeLayout.setVisibility(View.VISIBLE);
            avi.smoothToHide();
        }
    }


    public static boolean checkEditTextError(EditText editText, String message, Window window) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError(message);
            if (editText.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return true;
        }
        return false;
    }


    private void displayCity() {
//        ArrayList<String> cityName = new ArrayList<>();
//        cityName.add("المدينة");
//        ArrayList<Integer> cityId = new ArrayList<>();
//        cityId.add(0);
//
        cityName = new ArrayList<>();
        cityId = new ArrayList<>();
        RetroWeb.getClient().create(ServiceApi.class)
                .getCity(languagePrefManager.getAppLanguage()).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        for (int i =0;i<response.body().getCity().size();i++){
                            cityName.add(response.body().getCity().get(i).getName());
                            cityId.add(response.body().getCity().get(i).getId());
                            cityId.get(i);
                        }





                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });
    }

    private void displayRegion(int id,int postionItem,String CiteyName) {
        final ArrayList<String> regionName = new ArrayList<>();
        regionName.add(getString(R.string.rejon));
        final ArrayList<Integer> regionId = new ArrayList<>();


        RetroWeb.getClient().create(ServiceApi.class)
                .getRegion(id,languagePrefManager.getAppLanguage()).enqueue(new Callback<RegionResponse>() {
            @Override
            public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
//                        Toast.makeText(RegisterAsProviderActivity.this, "success", Toast.LENGTH_SHORT).show();
                        for (int i=0;i<response.body().getRegion().size();i++){
                            regionName.add(response.body().getRegion().get(i).getName());
                            regionId.add(response.body().getRegion().get(i).getId());
                        }
                        BranchCountModel branchCountModel=branches.get(postionItem);
                        branchCountModel.setRegionSpinner(regionName);
                        branchCountModel.setRjonId(regionId);
                        branchCountModel.setCityName(CiteyName);
                        branches.set(postionItem,branchCountModel);
                        adapterBranches.notifyItemChanged(postionItem);
                        Log.e("branches", String.valueOf(branchCountModel.getCitid()));
                    }
                }
            }
            @Override
            public void onFailure(Call<RegionResponse> call, Throwable t) {
                Toast.makeText(RegisterAsProviderActivity.this, R.string.errNetwork, Toast.LENGTH_SHORT).show();
            }
        });



    }




    @Override
    public void Rejone(int position, int portionItem, String cityName) {

        displayRegion(position,portionItem,cityName);

    }


    private boolean validation(){
        boolean flagEtPhone,flagEtNmae,flagEtpassword,checkCondtions,flagGender
                ,flagBirthDate,flagNationalId,
                flagCity,flagRejone,flagBranch
                ,flagQoulifcationId,flagEmail,
                flagETnameUser,
                flagWorkOurs,
                flagCourse;

        flagWorkOurs =ValidationUtils.emptyValidation(workHour,getString(R.string.fill_empty));
        flagCourse= ValidationUtils.emptyValidation(companyActivity,getString(R.string.fill_empty));

        flagEtNmae=ValidationUtils.emptyValidation(companyName,getString(R.string.fill_empty));

        flagEmail=ValidationUtils.emptyValidation(email,getString(R.string.fill_empty));


        flagETnameUser=ValidationUtils.emptyValidation(username,getString(R.string.fill_empty));

        if (flagEmail){
            flagEmail = ValidationUtils.validateEmail(email ,getString(R.string.EmailValidation));
        }




        if (branches.size()==0){
            flagBranch=false;
            CommonUtil.makeToast(RegisterAsProviderActivity.this,getString(R.string.branchesRequestError));
        }else {
            flagBranch=true;
        }


        flagEtPhone= ValidationUtils.emptyValidation(phone,getString(R.string.fill_empty));
        if (flagEtPhone){
            flagEtPhone= ValidationUtils.checkSize(phone,getString(R.string.phone_size),10);
        }



        checkCondtions=chAccept.isChecked();
        if (!checkCondtions){
            AlertDialog.Builder dialog=new AlertDialog.Builder(Objects.requireNonNull(this));

            dialog.setCancelable(true)
                    .setMessage(getString(R.string.checkCondtions));
            dialog.show();
        }

        flagEtpassword=ValidationUtils.emptyValidation(password,getString(R.string.fill_empty));

        if (flagEtpassword){
            flagEtpassword= ValidationUtils.checkSize(password,getString(R.string.passwordSizeValidation),6);
        }


        flagNationalId=ValidationUtils.emptyValidation(commercial,getString(R.string.fill_empty));

        if (flagNationalId){
            flagNationalId= ValidationUtils.checkSize(commercial,getString(R.string.nationalId),11);
        }

        return      flagEmail
                && flagEtNmae
                && flagEtPhone
                && flagEtpassword
                && checkCondtions
                && flagNationalId
                && flagBranch
                && flagETnameUser
                && flagCourse
                && flagWorkOurs;
    }

}
