package com.ait.forsah.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.Listeners.OnItemClickListener;
import com.ait.forsah.Models.Providers.ProviderNotificationModel;
import com.ait.forsah.R;
import com.ait.forsah.UI.Activities.OfferDetailsClientActivity;
import com.ait.forsah.UI.Activities.ProviderDetailsRefuseActivity;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private Context mContext;
    private List<ProviderNotificationModel> list;
    private String type;
    private OnItemClickListener onItemClickListener;

    public NotificationAdapter(Context mContext, List<ProviderNotificationModel> list , String type, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.list = list;
        this.type =type;
        this.onItemClickListener=onItemClickListener;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotificationViewHolder(LayoutInflater.from(mContext).inflate(R.layout.notification_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        holder.tvNotification.setText(list.get(position).getName() + " " + list.get(position).getUser_name());
        String reason  = list.get(position).getReason();


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("typetype",list.get(position).getType()+"");

                if (type.equals("provider")) {


                        Intent i = new Intent(mContext, ProviderDetailsRefuseActivity.class);


                        i.putExtra("UserId", String.valueOf(list.get(position).getId()));
                        i.putExtra("jobId", String.valueOf(list.get(position).getFk_job()));
                        i.putExtra("type", list.get(position).getType());
                    Log.e("typetype",list.get(position).getType()+"");

                        i.putExtra("id_notfy", list.get(position).getId());

                        i.putExtra("id_order", 0);


//                    if (list.get(position).getReason().equals("")) {
//                        i.putExtra("Reason", 8);
//                    }
//                i.putExtra("status",0);
                        mContext.startActivity(i);

                }else  if (type.equals("client")){
                    Intent intent = new Intent(mContext, OfferDetailsClientActivity.class);
                    intent.putExtra("orderId",list.get(position).getFk_order());
                    intent.putExtra("type", String.valueOf(list.get(position).getType()));
                    Log.e("typetype",list.get(position).getType()+"");

                    mContext.startActivity(intent);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNotification;
        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNotification = itemView.findViewById(R.id.tv_notification);

        }
    }
}
