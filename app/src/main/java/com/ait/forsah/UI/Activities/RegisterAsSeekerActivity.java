package com.ait.forsah.UI.Activities;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.CommonUtil;
import com.ait.forsah.Models.CityResponse;
import com.ait.forsah.Models.Clients.ClientRegisterResponse;
import com.ait.forsah.Models.JobTitleResponse;
import com.ait.forsah.Models.RegionResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.ValidationUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.wang.avi.AVLoadingIndicatorView;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterAsSeekerActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {


    private TextView tvAppBar;
    String GENDER = "0";
    String QULIFCATIONID ="0";

    private LanguagePrefManager languagePrefManager;
//    private Spinner spinnerJobTitle;
    private Spinner spinnerCity;
    private Spinner spinnerRegion;
    private Spinner spinnerGender;
    private Spinner spinnerQulifcations;
    private Spinner spinnerDegree;
    private ImageButton imgButton;
    private TextView dateOfBirth;
    private EditText etName;
    private EditText etPassword;
    private EditText etUserName;
    private EditText etPhone;
    private EditText etPhoneNum;
    private EditText etCoarses;
    private EditText etEmail;
    private EditText etNationalId;
    private EditText etExpriance;
    private Button buttonRegister;
    private TextView tvSeekerPolicy;
    public int idJob;
    private ProgressDialog mProgressDialog;
    private AVLoadingIndicatorView avi;
    private LinearLayout linearLayout;
    private CheckBox chAccept;


    protected SharedPrefManager mSharedPrefManager;

    protected SharedPrefManager mEditor;
    int idCity;
    int idRegion;
    private String mDeviceId;

    public RegisterAsSeekerActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_as_seeker);
//        genderType = new String();

        CommonUtil.setupUI(findViewById(R.id.parent),this);
        mDeviceId= FirebaseInstanceId.getInstance().getToken();

        languagePrefManager=new LanguagePrefManager(this);

        mSharedPrefManager = new SharedPrefManager(this);
        mEditor = new SharedPrefManager(this);
        tvAppBar = findViewById(R.id.tv_app_bar_register);
        tvAppBar.setText(R.string.register_as_seeker);
        etEmail = findViewById(R.id.et_mail);
        etName = findViewById(R.id.et_name);
        etUserName = findViewById(R.id.et_user_name);
        etPhone = findViewById(R.id.et_phone);
        etPassword = findViewById(R.id.et_password);
        etCoarses = findViewById(R.id.et_courses);
        etNationalId = findViewById(R.id.et_id_num);
        etExpriance = findViewById(R.id.et_expriance);
        chAccept =findViewById(R.id.chAccept);


        linearLayout = findViewById(R.id.parent_linear);
        tvSeekerPolicy = findViewById(R.id.tv_seeker_policy);
        tvSeekerPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RegisterAsSeekerActivity.this,PolicyActivity.class);
                i.putExtra("home",2);
                startActivity(i);
            }
        });


        final String indicator=getIntent().getStringExtra("indicator");
        avi= findViewById(R.id.avi);
        avi.setIndicator(indicator);

        // Get reference of widgets from XML layout
        spinnerGender = findViewById(R.id.spin_register_gender);
        spinnerQulifcations = findViewById(R.id.spin_qoualifications);
        spinnerCity = findViewById(R.id.spin_register_city);
        spinnerRegion = findViewById(R.id.spin_register_seeker_region);
       // spinnerJobTitle = findViewById(R.id.spin_register_job_title);


        dateOfBirth = findViewById(R.id.date_of_birth);
        imgButton = findViewById(R.id.img_button_back);
        buttonRegister = findViewById(R.id.btn_register_seeker);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validation()) {

                    register(etUserName.getText().toString()
                            , etName.getText().toString()
                            , GENDER
                            , dateOfBirth.getText().toString()
                            , etPhone.getText().toString()
                            , etEmail.getText().toString()
                            , etPassword.getText().toString()
                            , idCity + ""
                            , idRegion + ""
                            , idJob + ""
                            , QULIFCATIONID
                            , etExpriance.getText().toString()
                            , etCoarses.getText().toString()
                            , mDeviceId,
                            languagePrefManager.getAppLanguage());

                }
            }
        });
        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RegisterAsSeekerActivity.this, SignupActivity.class);
                startActivity(i);
            }
        });

        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                new DatePickerDialog(RegisterAsSeekerActivity.this, date,
//                        calendar.get(Calendar.MONTH),
//                        calendar.get(Calendar.DAY_OF_MONTH),
//                        calendar.get(Calendar.YEAR)).show();

                chooseDate(RegisterAsSeekerActivity.this,dateOfBirth);

            }
        });


        // Initializing a String Array
        final List<String> gender = new ArrayList<>();
        gender.add(getString(R.string.gender));
        gender.add(getString(R.string.male));
        gender.add(getString(R.string.female));

        final List<String> qoulifcation = new ArrayList<>();
        qoulifcation.add(getString(R.string.degree));
        qoulifcation.add(getString(R.string.secondary));
        qoulifcation.add(getString(R.string.academic));
        qoulifcation.add(getString(R.string.master));

        // Initializing an ArrayAdapter
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_activated_1, gender) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

        };

        ArrayAdapter<String> arrayAdapterQoulifcation = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_activated_1, qoulifcation) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

        };
        spinnerGender.setAdapter(arrayAdapter);
        spinnerQulifcations.setAdapter(arrayAdapterQoulifcation);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String selectedItem = (String) parent.getItemAtPosition(position);

                if (position == 1) {
                    GENDER = "M";

                } else if (position ==2){
                    GENDER = "F";
                }

                Log.e("<<<gender",GENDER);

            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });



        spinnerQulifcations.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String selectedItem = (String) parent.getItemAtPosition(position);

                if (position == 1) {
                    QULIFCATIONID = "1";

                } else if (position ==2){
                    QULIFCATIONID = "2";
                }else if (position==3){
                    QULIFCATIONID = "3";
                }

                Log.e("<<<gender",QULIFCATIONID);

            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        //displayJobTitle();
        displayCity();
    }

//    private void displayJobTitle() {
//        final ArrayList<String> jobName = new ArrayList<>();
//        jobName.add(0, getString(R.string.spinner_job_title));
//        final ArrayList<Integer> jobId = new ArrayList<>();
//        RetroWeb.getClient().create(ServiceApi.class)
//                .getJobTitle(languagePrefManager.getAppLanguage()).enqueue(new Callback<JobTitleResponse>() {
//            @Override
//            public void onResponse(Call<JobTitleResponse> call, Response<JobTitleResponse> response) {
//                if (response.isSuccessful()) {
//                    if (response.body().getKey() == 1) {
//                        for (int i = 0; i < response.body().getJob_title().size(); i++) {
//                            jobName.add(response.body().getJob_title().get(i).getName());
//                            jobId.add(response.body().getJob_title().get(i).getId());
//                        }
//
//                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegisterAsSeekerActivity.this,
//                                android.R.layout.simple_list_item_activated_1, jobName) {
//                            @Override
//                            public boolean isEnabled(int position) {
//                                if (position == 0) {
//                                    // Disable the first item from Spinner
//                                    // First item will be use for hint
//                                    return false;
//                                } else {
//                                    return true;
//                                }
//                            }
//                        };
//                        spinnerJobTitle.setAdapter(adapter);
//                        spinnerJobTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                            @Override
//                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                                if (!jobName.get(position).equals(getString(R.string.spinner_job_title))) {
//                                    String name = jobName.get(position);
//                                    idJob = jobId.get(position-1);
////                                    Toast.makeText(RegisterAsSeekerActivity.this, String.valueOf(id), Toast.LENGTH_SHORT).show();
//
//                                    Log.e("jobId", idJob + "");
//
//                                }
//                            }
//
//                            @Override
//                            public void onNothingSelected(AdapterView<?> adapterView) {
//
//                            }
//                        });
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JobTitleResponse> call, Throwable t) {
//
//            }
//        });
//
//    }

    private void displayCity() {
        final ArrayList<String> cityName = new ArrayList<>();
        cityName.add(0, getString(R.string.spinner_city));
        final ArrayList<Integer> cityId = new ArrayList<>();
        cityId.add(0, -1);
        RetroWeb.getClient().create(ServiceApi.class)
                .getCity(languagePrefManager.getAppLanguage()).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {

                        for (int i = 0; i < response.body().getCity().size(); i++) {
                            cityName.add(response.body().getCity().get(i).getName());
                            cityId.add(response.body().getCity().get(i).getId());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegisterAsSeekerActivity.this,
                                android.R.layout.simple_list_item_activated_1, cityName) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        spinnerCity.setAdapter(adapter);
                        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int postion, long l) {
                                if (!cityName.get(postion).equals(R.string.tv_city)) {
                                    String city = cityName.get(postion);
                                    idCity = cityId.get(postion);

                                    displayRegion(idCity);


                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });
    }

    private void displayRegion(int id) {

        final ArrayList<String> regionName = new ArrayList<>();
        regionName.add(0, getString(R.string.spinner_region));
        final ArrayList<Integer> regionId = new ArrayList<>();
        regionId.add(0, 0);

        RetroWeb.getClient().create(ServiceApi.class)
                .getRegion(id, languagePrefManager.getAppLanguage()).enqueue(new Callback<RegionResponse>() {
            @Override
            public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getKey() == 1) {
//                        Toast.makeText(RegisterAsSeekerActivity.this, "success", Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < response.body().getRegion().size(); i++) {
                            regionName.add(response.body().getRegion().get(i).getName());
                            regionId.add(response.body().getRegion().get(i).getId());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegisterAsSeekerActivity.this,
                                android.R.layout.simple_list_item_activated_1, regionName) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        };
                        spinnerRegion.setAdapter(adapter);
                        spinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int postion, long l) {
                                if (!regionName.get(postion).equals(getString(R.string.spinner_region))) {
                                    String region = regionName.get(postion);
                                    idRegion = regionId.get(postion);
//                                    Toast.makeText(getApplicationContext(),String.valueOf(id),Toast.LENGTH_LONG).show();

                                    Log.e("idRegion", idRegion + "");
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<RegionResponse> call, Throwable t) {
                Toast.makeText(RegisterAsSeekerActivity.this, "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }


    // update Calender
    private void updateLabel() {
        String format = "MM/dd/yy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        dateOfBirth.setText(simpleDateFormat.format(calendar.getTime()));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String text = adapterView.getItemAtPosition(i).toString();
        Toast.makeText(adapterView.getContext(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }

    final Calendar calendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            calendar.set(Calendar.YEAR, year);

            updateLabel();
        }
    };


    private void register(
            String userNmae,
            String name,
            String gendr,
            String birthDate,
            String phone,
            String mail,
            String password,
            String City,
            final String region,
            String titleJob,
            String qoulifcation,
            String previousExpriance,
            String educationalCours,
            String token,
            String lang)
    {
        linearLayout.setVisibility(View.INVISIBLE);
            avi.smoothToShow();

            RetroWeb.getClient()
                    .create(ServiceApi.class)
                    .register(userNmae,
                            name,
                            etNationalId.getText().toString(),
                            gendr,
                            birthDate,
                            phone,
                            mail,
                            password,
                            City,
                            region,
                            titleJob,
                            qoulifcation,
                            previousExpriance,
                            educationalCours,
                            token,
                            lang,
                            true)
                    .enqueue(new Callback<ClientRegisterResponse>() {
                        @Override
                        public void onResponse(Call<ClientRegisterResponse> call, Response<ClientRegisterResponse> response) {
//                            mProgressDialog.dismiss();
                            avi.smoothToHide();
                            if (response.isSuccessful()) {
                                if (response.body().getKey() == 1) {
//                                    avi.smoothToHide();
                                    mSharedPrefManager.setUserData(response.body().getClient());
                                    mSharedPrefManager.setLoginStatus(true);
                                    CommonUtil.makeToast(RegisterAsSeekerActivity.this, R.string.SucessRegester);


                                    Intent i = new Intent(RegisterAsSeekerActivity.this, HomeSeekerActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();


                                } else {
                                    avi.smoothToHide();
                                    linearLayout.setVisibility(View.VISIBLE);
                                    CommonUtil.makeToast(RegisterAsSeekerActivity.this, response.body().getMsg());
                                    Log.e("<<Error",response.body().getMsg());


                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ClientRegisterResponse> call, Throwable t) {

                            CommonUtil.makeToast(RegisterAsSeekerActivity.this,R.string.errNetwork);
                            mProgressDialog.dismiss();

                        }
                    });

        }

    public static void  chooseDate(Context context, final TextView textView ){
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dlg;
        dlg = new DatePickerDialog(context, (view, year1, month1, dayOfMonth) -> {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year1, month1, dayOfMonth, 0, 0, 0);
            Date chosenDate = cal.getTime();
            String date= month1+1 +"-"+dayOfMonth+"-"+year1;
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyy",Locale.getDefault());
            Log.e("day",sdf.format(chosenDate));

            textView.setText(date);
        }, year, month,day);
//        dlg.setTitle(getString(R.string.dtect_date));
        dlg.show();

    }




    private boolean validation(){
        boolean flagEtPhone,flagEtNmae,flagEtpassword,checkCondtions,flagGender,flagExpriance
                ,flagBirthDate,flagNationalId,
                flagCity,flagRejone/*flagIdJob*/
                ,flagQoulifcationId,flagEmail,
                flagETnameUser,
                flagCourse;

        flagExpriance=ValidationUtils.emptyValidation(etExpriance,getString(R.string.fill_empty));

        flagCourse=ValidationUtils.emptyValidation(etCoarses,getString(R.string.fill_empty));

        flagEtNmae=ValidationUtils.emptyValidation(etName,getString(R.string.fill_empty));

        flagEmail=ValidationUtils.emptyValidation(etEmail,getString(R.string.fill_empty));


        flagETnameUser=ValidationUtils.emptyValidation(etUserName,getString(R.string.fill_empty));

        if (flagEmail){
            flagEmail = ValidationUtils.validateEmail(etEmail ,getString(R.string.EmailValidation));
        }




        if (idCity==0){
            flagCity=false;
            CommonUtil.makeToast(RegisterAsSeekerActivity.this,getString(R.string.must_chose_city));
        }else {
            flagCity=true;
        }


        if (idRegion==0){
            flagRejone=false;
            CommonUtil.makeToast(RegisterAsSeekerActivity.this,getString(R.string.must_chose_region));
        }else {
            flagRejone=true;
        }
//
//        if (idJob==0){
//            //flagIdJob=false;
//            CommonUtil.makeToast(RegisterAsSeekerActivity.this,getString(R.string.must_chose_job));
//        }else {
//            //flagIdJob=true;
//        }

        flagEtPhone= ValidationUtils.emptyValidation(etPhone,getString(R.string.fill_empty));
        if (flagEtPhone){
            flagEtPhone= ValidationUtils.checkSize(etPhone,getString(R.string.phone_size),10);
        }

        if (GENDER.equals("0")){
            flagGender=false;

            CommonUtil.makeToast(RegisterAsSeekerActivity.this, getString(R.string.chooseGender));

        }else{
            flagGender=true;

        }

        if (QULIFCATIONID.equals("0")){
            flagQoulifcationId=false;

            CommonUtil.makeToast(RegisterAsSeekerActivity.this,getString(R.string.qoulifcation));

        }else{
            flagQoulifcationId=true;

        }
        if (dateOfBirth.getText().toString().equals("")){
            flagBirthDate=false;
            CommonUtil.makeToast(RegisterAsSeekerActivity.this, getString(R.string.chooseBirthDate));

        }else {
            flagBirthDate=true;

        }

        checkCondtions=chAccept.isChecked();
        if (!checkCondtions){
            AlertDialog.Builder dialog=new AlertDialog.Builder(Objects.requireNonNull(this));

            dialog.setCancelable(true)
                    .setMessage(getString(R.string.checkCondtions));
            dialog.show();
        }

        flagEtpassword=ValidationUtils.emptyValidation(etPassword,getString(R.string.fill_empty));

        if (flagEtpassword){
            flagEtpassword= ValidationUtils.checkSize(etPassword,getString(R.string.passwordSizeValidation),6);
        }


        flagNationalId=ValidationUtils.emptyValidation(etNationalId,getString(R.string.fill_empty));

        if (flagNationalId){
            flagNationalId= ValidationUtils.checkSize(etNationalId,getString(R.string.nationalId),11);
        }


        Log.e("checkCondtions",checkCondtions+"");

        return      flagEmail
                && flagEtNmae
                && flagEtPhone
                && flagEtpassword
                && checkCondtions
                && flagGender
                && flagBirthDate
                && flagNationalId
                && flagCity
                && flagRejone
                && flagETnameUser/* flagIdJob*/
                && flagQoulifcationId
                && flagCourse
                && flagExpriance;
    }





}
