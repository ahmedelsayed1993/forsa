package com.ait.forsah.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ait.forsah.Models.Providers.ProviderGetOrderModel;
import com.ait.forsah.Models.Providers.ProviderGetOrderResponse;
import com.ait.forsah.Network.RetroWeb;
import com.ait.forsah.Network.ServiceApi;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.UI.Adapters.OrderAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends AppCompatActivity {



    private ProviderGetOrderModel orderModel;





    private RecyclerView recyclerView;
    private OrderAdapter adapter;
    private ArrayList<ProviderGetOrderModel> list;

    private static final String MYPREFERENCE = "myPref";
    private static final String USER_ID = "idKey";
//    private static final String LANG_AR = "arKey";
    private static final String LANG = "langKey";

//    private static final String LANG_EN = "enKey";

    private String idUser;
    private String lang;

    private AVLoadingIndicatorView avi;
    private LanguagePrefManager mLanguagePrefManager;
    private SharedPrefManager  mSharedPreferences;


    private ImageView img_back;

    TextView tv_app_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        img_back=findViewById(R.id.img_back);
        tv_app_bar=findViewById(R.id.tv_app_bar);
        tv_app_bar.setText(R.string.menu_my_order);

        mSharedPreferences = new SharedPrefManager(this);
        mLanguagePrefManager = new LanguagePrefManager(this);

        recyclerView = findViewById(R.id.recycler_order_item);


        idUser = mSharedPreferences.getUserData().getId()+"";
        lang =mLanguagePrefManager.getAppLanguage();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false));

        final String indicator=getIntent().getStringExtra("indicator");
        avi= findViewById(R.id.avi);
        avi.setIndicator(indicator);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        displayOrder();
    }

    private void displayOrder() {
        avi.smoothToShow();
        RetroWeb.getClient().create(ServiceApi.class)
                .getOrder(idUser,lang).enqueue(new Callback<ProviderGetOrderResponse>() {
            @Override
            public void onResponse(Call<ProviderGetOrderResponse> call, Response<ProviderGetOrderResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getKey() == 1){
                        avi.smoothToHide();
                        adapter = new OrderAdapter(OrderActivity.this,response.body().getResult());
                        recyclerView.setAdapter(adapter);
                    }else {
                        Toast.makeText(OrderActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(OrderActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProviderGetOrderResponse> call, Throwable t) {
                Toast.makeText(OrderActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
