package com.ait.forsah.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ait.forsah.Models.Providers.ProviderGetOrderModel;
import com.ait.forsah.R;
import com.ait.forsah.UI.Activities.ProviderDetailsAcceptActivity;
import com.ait.forsah.UI.Activities.ProviderDetailsRefuseActivity;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    private Context context;
    private List<ProviderGetOrderModel> list;

    public OrderAdapter(Context context, List<ProviderGetOrderModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderViewHolder(LayoutInflater.from(context).inflate(R.layout.order_item,parent,false));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, final int position) {

        holder.tvSeekerName.setText(list.get(position).getName());
        holder.tvJobTitle.setText(list.get(position).getJob_title());
        holder.tvCity.setText(list.get(position).getCity());
        holder.tvRegion.setText(list.get(position).getRegion());
        holder.tvStstus.setText(list.get(position).getStutes());
        final int status = list.get(position).getStutes_no();

        if (status == 0 ){
            holder.imgStatus.setImageResource(R.mipmap.clock);
        }else if (status == 1 || status ==5){
            holder.imgStatus.setImageResource(R.mipmap.check_box);
        }else if (status == 2){
            holder.imgStatus.setImageResource(R.mipmap.multiply);
        }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i;
                        if (status==0){
                            i = new Intent(context,ProviderDetailsAcceptActivity.class);
                            i.putExtra("jobId",String.valueOf(list.get(position).getJob_id()));
                            i.putExtra("status",0);
                            i.putExtra("id_notfy", 0);
                            i.putExtra("id_order", list.get(position).getOrder_id());
                            Log.e("jobId",String.valueOf(list.get(position).getJob_id()));
                            Log.e("status",status+"");
                            context.startActivity(i);

                        }
                        else if (status==1){
                            i = new Intent(context,ProviderDetailsAcceptActivity.class);
                            i.putExtra("jobId",String.valueOf(list.get(position).getJob_id()));
                            i.putExtra("status",1);
                            Log.e("jobId",String.valueOf(list.get(position).getJob_id()));
                            Log.e("status",status+"");
                            i.putExtra("id_notfy", 0);
                            i.putExtra("id_order", list.get(position).getOrder_id());

                            context.startActivity(i);
                        }
                        else{
                            i = new Intent(context,ProviderDetailsRefuseActivity.class);
                            //i.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            i.putExtra("jobId",String.valueOf(list.get(position).getJob_id()));
                            i.putExtra("status",2);
                            Log.e("jobId",String.valueOf(list.get(position).getJob_id()));
                            i.putExtra("id_notfy", 0);
                            i.putExtra("id_order", list.get(position).getOrder_id());
                            Log.e("status",status+"");

                            context.startActivity(i);
                        }
                    }
                });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class OrderViewHolder extends RecyclerView.ViewHolder{
        private TextView tvSeekerName;
        private TextView tvJobTitle;
        private TextView tvCity;
        private TextView tvRegion;
        private TextView tvStstus;
        private ImageView imgStatus;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);

            tvSeekerName = itemView.findViewById(R.id.tv_order_seeker_name);
            tvJobTitle = itemView.findViewById(R.id.tv_order_job_title);
            tvCity = itemView.findViewById(R.id.tv_order_city);
            tvRegion = itemView.findViewById(R.id.tv_order_region);
            tvStstus = itemView.findViewById(R.id.tv_order_status);
            imgStatus = itemView.findViewById(R.id.img_order_status);
        }
    }
}
