package com.ait.forsah;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;



/*
created by mahmoud 10/4/2017
 */

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;

    private static Context mContext;

    public static synchronized AppController getInstance() {
        return mInstance;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mContext = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}