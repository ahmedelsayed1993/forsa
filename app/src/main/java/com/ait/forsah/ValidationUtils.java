package com.ait.forsah;

import android.content.Context;
import android.net.ConnectivityManager;

import android.text.TextUtils;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by mahmoud on 8/9/17.
 */

public class ValidationUtils {

    private static final boolean isValid(String input) {
        boolean valid = true;
        if (input.trim().isEmpty()) {
            valid = false;
        }
        return valid;
    }

    public static final boolean emptyValidation(EditText editText, String error) {
        editText.setError(null);
        if (editText.getText().toString().trim().isEmpty()) {
            editText.setError(error);
            return false;
        }
        return true;
    }

    public static boolean checkError(EditText editText, TextInputLayout textInputLayout, String error_message) {
        if (!isValid(editText.getText().toString())) {
            textInputLayout.setError(error_message);
            return false;
        } else {
            textInputLayout.setError(null);
            return true;
        }
    }

    public static final boolean checkPassSize(EditText editText, String message, int size) {
        if (editText.getText().toString().length() < size) {
            editText.setError(message);
            return false;
        } else {
            editText.setError(null);
            return true;
        }

    }

    public static final boolean emptyValidationReturnBoolean(EditText editText) {
        if (editText.getText().toString().trim().isEmpty()) {
            return false;
        }
        return true;
    }


    public static final boolean passwordSizeValidation(EditText editText , String error) {
        if (editText.getText().toString().length() <= 5) {
            editText.setError(error);
            return false;
        }
        return true;
    }

    public static final boolean checkNamePref(EditText editText, String error) {
        if (editText.getText().toString().matches("^[a-zA-Z].*")) {
            return true;
        } else {
            editText.setError(error);
            return false;
        }
    }


    public static boolean validateConfirmPassword(EditText et_user_password, EditText et_confirm_password , String error) {
        if (!et_confirm_password.getText().toString().equals(et_user_password.getText().toString().trim())) {
            et_confirm_password.setError(error);
            return false;
        }
        return true;
    }

    public static boolean validateEmail(EditText editText, String error) {
        if (editText.getText().toString().trim().isEmpty()) {
            editText.setError("Enter your Email");
            return false;
        } else if (!isValidEmail(editText.getText().toString().trim())) {
            editText.setError(error);
            return false;
        }
        return true;
    }




    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean checkSize(EditText editText, String message, int size) {
        if (editText.getText().toString().trim().length() < size) {
            editText.setError(message);
            return false;
        } else {
            editText.setError(null);
            return true;
        }

    }







    public static boolean isEmailValid(EditText email, TextInputLayout textInputLayout, String message) {
        String emailString = email.getText().toString();
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(emailString);
        if (matcher.matches() == true) {
            textInputLayout.setError(null);
            return true;
        } else {
            textInputLayout.setError(message);
            return false;
        }
    }

    public static boolean checkMatch(EditText pass, EditText confirm_pass,
                                     String message) {
        boolean isMatch = false;

        if (pass.getText().toString().matches(confirm_pass.getText().toString())) {
            isMatch = true;
        } else {
            confirm_pass.setError(message);
        }
        return isMatch;
    }

    public static final boolean emptyValidation(Context context, TextInputEditText editText, String errorMessage) {
        if (editText.getText().toString().trim().isEmpty()) {
            CommonUtil.makeToast(context, errorMessage);
            return false;
        }
        return true;
    }

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
