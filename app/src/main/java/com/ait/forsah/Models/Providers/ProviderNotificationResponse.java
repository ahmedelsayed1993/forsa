package com.ait.forsah.Models.Providers;

import com.ait.forsah.Listeners.BaseResponse;

import java.util.ArrayList;
import java.util.List;

public class ProviderNotificationResponse extends BaseResponse {

    private List<ProviderNotificationModel>notify;

    private ArrayList<ProviderNotificationModel> client;

    public ArrayList<ProviderNotificationModel> getClient() {
        return client;
    }

    public void setClient(ArrayList<ProviderNotificationModel> client) {
        this.client = client;
    }

    public List<ProviderNotificationModel> getNotify() {
        return notify;
    }

    public void setNotify(List<ProviderNotificationModel> notify) {
        this.notify = notify;
    }
}
