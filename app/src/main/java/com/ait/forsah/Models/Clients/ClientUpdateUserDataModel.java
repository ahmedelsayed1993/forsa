package com.ait.forsah.Models.Clients;

import java.io.Serializable;

public class ClientUpdateUserDataModel implements Serializable {


    private String user_name;
    private String name;
    private String email;
    private String phone;
    private String id_number;
    private int Id;
    private String birth_date;
    private String educational_courses;
    private String previous_experience;
    private String qualification;
    private String city;
    private String Region;
    private String Job_title;
    private String img;
    private String token;


    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getEducational_courses() {
        return educational_courses;
    }

    public void setEducational_courses(String educational_courses) {
        this.educational_courses = educational_courses;
    }

    public String getPrevious_experience() {
        return previous_experience;
    }

    public void setPrevious_experience(String previous_experience) {
        this.previous_experience = previous_experience;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getJob_title() {
        return Job_title;
    }

    public void setJob_title(String job_title) {
        Job_title = job_title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
