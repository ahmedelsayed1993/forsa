package com.ait.forsah.Models;

import com.ait.forsah.Listeners.BaseResponse;

import java.util.ArrayList;

public class JobTitleResponse extends BaseResponse {


    private ArrayList<JobTitleModel> Job_title;

    public ArrayList<JobTitleModel> getJob_title() {
        return Job_title;
    }

    public void setJob_title(ArrayList<JobTitleModel> job_title) {
        Job_title = job_title;
    }
}
