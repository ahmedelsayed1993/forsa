package com.ait.forsah.Models.Clients;

import java.io.Serializable;

public class ClientOrderDetailsModel implements Serializable {

    private String name;
    private String user_name;
    private String img;
    private String about;
    private String activity;
    private int branch_count;
    private String phone;
    private int stutes;
    private String Region;
    private String city;
    private String street_branch;
    private String work_hours;
    private String timeDeleteOrdar;
    private String timePaymentOrdar;

    private String fk_job;


    public String getFk_job() {
        return fk_job;
    }

    public void setFk_job(String fk_job) {
        this.fk_job = fk_job;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getBranch_count() {
        return branch_count;
    }

    public void setBranch_count(int branch_count) {
        this.branch_count = branch_count;
    }

    public int getStutes() {
        return stutes;
    }

    public void setStutes(int stutes) {
        this.stutes = stutes;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet_branch() {
        return street_branch;
    }

    public void setStreet_branch(String street_branch) {
        this.street_branch = street_branch;
    }

    public String getWork_hours() {
        return work_hours;
    }

    public void setWork_hours(String work_hours) {
        this.work_hours = work_hours;
    }

    public String getTimeDeleteOrdar() {
        return timeDeleteOrdar;
    }

    public void setTimeDeleteOrdar(String timeDeleteOrdar) {
        this.timeDeleteOrdar = timeDeleteOrdar;
    }

    public String getTimePaymentOrdar() {
        return timePaymentOrdar;
    }

    public void setTimePaymentOrdar(String timePaymentOrdar) {
        this.timePaymentOrdar = timePaymentOrdar;
    }
}
