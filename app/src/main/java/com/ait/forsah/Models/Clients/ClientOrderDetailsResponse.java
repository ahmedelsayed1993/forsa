package com.ait.forsah.Models.Clients;

import com.ait.forsah.Listeners.BaseResponse;

public class ClientOrderDetailsResponse extends BaseResponse {

    private ClientOrderDetailsModel result;

    public ClientOrderDetailsModel getResult() {
        return result;
    }

    public void setResult(ClientOrderDetailsModel result) {
        this.result = result;
    }
}
