package com.ait.forsah.Models.Clients;

import com.ait.forsah.Listeners.BaseResponse;

public class AdvertDescriptionResponse extends BaseResponse {


    AdvertDescriptionModel result;

    public AdvertDescriptionModel getResult() {
        return result;
    }

    public void setResult(AdvertDescriptionModel result) {
        this.result = result;
    }


}
