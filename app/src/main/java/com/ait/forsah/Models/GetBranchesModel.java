package com.ait.forsah.Models;

import java.io.Serializable;

public class GetBranchesModel  implements Serializable {


   private int  city_id;

   private String street;

   private int region;

   private int Id;

   private int work_hours;

    private int fk_Provider;


    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String
                                  street) {
        this.street = street;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getWork_hours() {
        return work_hours;
    }

    public void setWork_hours(int work_hours) {
        this.work_hours = work_hours;
    }

    public int getFk_Provider() {
        return fk_Provider;
    }

    public void setFk_Provider(int fk_Provider) {
        this.fk_Provider = fk_Provider;
    }
}
