package com.ait.forsah.Models.Clients;

import java.io.Serializable;


/**
 * Typed by Shehatah -->Last Dragon
 */
public class ClientRegisterModel implements Serializable {

    private String user_name;
    private String name;
    private String email;
    private String phone;
    private String id_number;
    private String type_user;
    private int Id;
    private String birth_date;
    private String educational_courses;
    private String previous_experience;
    private String qualification_id;
    private String qualification;
    private String city;
    private String Region;
    private String Job_title;
    private String img;
    private String token;
    private String commercial_registration_no;
    private String about;
    private int branch_count;
    private String work_hours;
    private int fk_city;
    private int fk_job_title;
    private int fk_region;


    public String getQualification_id() {
        return qualification_id;
    }

    public void setQualification_id(String qualification_id) {
        this.qualification_id = qualification_id;
    }

    public int getFk_city() {
        return fk_city;
    }

    public void setFk_city(int fk_city) {
        this.fk_city = fk_city;
    }

    public int getFk_job_title() {
        return fk_job_title;
    }

    public void setFk_job_title(int fk_job_title) {
        this.fk_job_title = fk_job_title;
    }

    public int getFk_region() {
        return fk_region;
    }

    public void setFk_region(int fk_region) {
        this.fk_region = fk_region;
    }

    public String getCommercial_registration_no() {
        return commercial_registration_no;
    }

    public void setCommercial_registration_no(String commercial_registration_no) {
        this.commercial_registration_no = commercial_registration_no;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getBranch_count() {
        return branch_count;
    }

    public void setBranch_count(int branch_count) {
        this.branch_count = branch_count;
    }

    public String getWork_hours() {
        return work_hours;
    }

    public void setWork_hours(String work_hours) {
        this.work_hours = work_hours;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getType_user() {
        return type_user;
    }

    public void setType_user(String type_user) {
        this.type_user = type_user;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getEducational_courses() {
        return educational_courses;
    }

    public void setEducational_courses(String educational_courses) {
        this.educational_courses = educational_courses;
    }

    public String getPrevious_experience() {
        return previous_experience;
    }

    public void setPrevious_experience(String previous_experience) {
        this.previous_experience = previous_experience;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getJob_title() {
        return Job_title;
    }

    public void setJob_title(String job_title) {
        Job_title = job_title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isClient(){
      return type_user.equals("client");
    }

    public boolean isProvidor(){
        return type_user.equals("provider");
    }
}
