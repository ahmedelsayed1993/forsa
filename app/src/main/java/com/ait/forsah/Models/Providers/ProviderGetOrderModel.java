package com.ait.forsah.Models.Providers;

import java.io.Serializable;

public class ProviderGetOrderModel implements Serializable {

    private int order_id;
    private int job_id;
    private String name;
    private String user_name;
    private String stutes;
    private int stutes_no;
    private String city;
    private String Region;
    private String Job_title;


    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getJob_id() {
        return job_id;
    }

    public void setJob_id(int job_id) {
        this.job_id = job_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getStutes() {
        return stutes;
    }

    public void setStutes(String stutes) {
        this.stutes = stutes;
    }

    public int getStutes_no() {
        return stutes_no;
    }

    public void setStutes_no(int stutes_no) {
        this.stutes_no = stutes_no;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getJob_title() {
        return Job_title;
    }

    public void setJob_title(String job_title) {
        Job_title = job_title;
    }
}
