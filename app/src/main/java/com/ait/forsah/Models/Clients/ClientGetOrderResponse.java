package com.ait.forsah.Models.Clients;

import com.ait.forsah.Listeners.BaseResponse;


import java.util.List;

public class ClientGetOrderResponse extends BaseResponse {


    private List<ClientGetOrderModel> result;
    private Boolean makeAdvert;
    private Boolean checkrefuseAllOrder;

    public List<ClientGetOrderModel> getResult() {
        return result;
    }

    public void setResult(List<ClientGetOrderModel> result) {
        this.result = result;
    }

    public Boolean getMakeAdvert() {
        return makeAdvert;
    }

    public void setMakeAdvert(Boolean makeAdvert) {
        this.makeAdvert = makeAdvert;
    }

    public Boolean getCheckrefuseAllOrder() {
        return checkrefuseAllOrder;
    }

    public void setCheckrefuseAllOrder(Boolean checkrefuseAllOrder) {
        this.checkrefuseAllOrder = checkrefuseAllOrder;
    }
}
