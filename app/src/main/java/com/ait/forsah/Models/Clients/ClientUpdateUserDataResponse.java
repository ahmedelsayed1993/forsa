package com.ait.forsah.Models.Clients;

import com.ait.forsah.Listeners.BaseResponse;

public class ClientUpdateUserDataResponse extends BaseResponse {

    private ClientUpdateUserDataModel client;

    public ClientUpdateUserDataModel getClient() {
        return client;
    }

    public void setClient(ClientUpdateUserDataModel client) {
        this.client = client;
    }
}
