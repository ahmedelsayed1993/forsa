package com.ait.forsah.Models;

import com.ait.forsah.Listeners.BaseResponse;

import java.util.ArrayList;

public class GetBranchesResponse  extends BaseResponse {

    private ArrayList<GetBranchesModel> branches;


    public ArrayList<GetBranchesModel> getBranches() {
        return branches;
    }

    public void setBranches(ArrayList<GetBranchesModel> branches) {
        this.branches = branches;
    }
}
