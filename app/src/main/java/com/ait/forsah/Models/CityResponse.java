package com.ait.forsah.Models;

import com.ait.forsah.Listeners.BaseResponse;

import java.util.ArrayList;

public class CityResponse extends BaseResponse {

    private ArrayList<CityModel> City;

    public ArrayList<CityModel> getCity() {
        return City;
    }

    public void setCity(ArrayList<CityModel> city) {
        City = city;
    }
}
