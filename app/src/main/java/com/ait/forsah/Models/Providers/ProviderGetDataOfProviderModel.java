package com.ait.forsah.Models.Providers;

import java.io.Serializable;
import java.util.List;

public class ProviderGetDataOfProviderModel implements Serializable {

    private String user_name;
    private String name;
    private String email;
    private String phone;
    private int Id;
    private String commercial_registration_no;
    private String about;
    private String branch_count;
    private String work_hours;
    private String activity;
    private String img;
    private String token;
    private List<ProviderGetBranchModel> branches;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCommercial_registration_no() {
        return commercial_registration_no;
    }

    public void setCommercial_registration_no(String commercial_registration_no) {
        this.commercial_registration_no = commercial_registration_no;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getBranch_count() {
        return branch_count;
    }

    public void setBranch_count(String branch_count) {
        this.branch_count = branch_count;
    }

    public String getWork_hours() {
        return work_hours;
    }

    public void setWork_hours(String work_hours) {
        this.work_hours = work_hours;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<ProviderGetBranchModel> getBranches() {
        return branches;
    }

    public void setBranches(List<ProviderGetBranchModel> branches) {
        this.branches = branches;
    }
}
