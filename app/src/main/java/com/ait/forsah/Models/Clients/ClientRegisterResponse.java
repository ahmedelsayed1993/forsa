package com.ait.forsah.Models.Clients;

import com.ait.forsah.Listeners.BaseResponse;

public class ClientRegisterResponse extends BaseResponse {


    private ClientRegisterModel client;

    public ClientRegisterResponse(ClientRegisterModel client) {
        this.client = client;
    }


    public ClientRegisterModel getClient() {
        return client;
    }

    public void setClient(ClientRegisterModel client) {
        this.client = client;
    }
}
