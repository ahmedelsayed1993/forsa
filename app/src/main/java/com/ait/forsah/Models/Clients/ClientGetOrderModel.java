package com.ait.forsah.Models.Clients;

import java.io.Serializable;

public class ClientGetOrderModel implements Serializable {
    private int order_id;
    private int job_id;
    private String name;
    private String hours_no;
    private String user_name;
    private String img;
    private String expected_salary;
    private String about;
    private int fk_provider;
    private String stutes;
    private int stutes_no;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getJob_id() {
        return job_id;
    }

    public void setJob_id(int job_id) {
        this.job_id = job_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHours_no() {
        return hours_no;
    }

    public void setHours_no(String hours_no) {
        this.hours_no = hours_no;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getExpected_salary() {
        return expected_salary;
    }

    public void setExpected_salary(String expected_salary) {
        this.expected_salary = expected_salary;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getFk_provider() {
        return fk_provider;
    }

    public void setFk_provider(int fk_provider) {
        this.fk_provider = fk_provider;
    }

    public String getStutes() {
        return stutes;
    }

    public void setStutes(String stutes) {
        this.stutes = stutes;
    }

    public int getStutes_no() {
        return stutes_no;
    }

    public void setStutes_no(int stutes_no) {
        this.stutes_no = stutes_no;
    }
}
