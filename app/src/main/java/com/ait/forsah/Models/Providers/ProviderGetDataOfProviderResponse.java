package com.ait.forsah.Models.Providers;

import com.ait.forsah.Listeners.BaseResponse;

public class ProviderGetDataOfProviderResponse extends BaseResponse {

    private ProviderGetDataOfProviderModel provider;

    public ProviderGetDataOfProviderModel getProvider() {
        return provider;
    }

    public void setProvider(ProviderGetDataOfProviderModel provider) {
        this.provider = provider;
    }
}
