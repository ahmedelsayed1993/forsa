package com.ait.forsah.Models.Providers;

import com.ait.forsah.Listeners.BaseResponse;

public class ProviderAdvertsmentDetailsResponse extends BaseResponse {

    private ProviderAdvertsmentDetailsModel result;

    public ProviderAdvertsmentDetailsModel getResult() {
        return result;
    }

    public void setResult(ProviderAdvertsmentDetailsModel result) {
        this.result = result;
    }
}
