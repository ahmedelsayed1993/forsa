package com.ait.forsah.Models.Clients;

import java.io.Serializable;

public class ClientNotificationModel implements Serializable {

    private int Id;
    private int type;
    private String user_name;
    private int fk_order;
    private String name;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getFk_order() {
        return fk_order;
    }

    public void setFk_order(int fk_order) {
        this.fk_order = fk_order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
