package com.ait.forsah.Models.Providers;

import com.ait.forsah.Listeners.BaseResponse;

import java.util.ArrayList;

public class ProviderGetBranchResponse extends BaseResponse {

    private ArrayList<ProviderGetBranchModel> branches;

    public ArrayList<ProviderGetBranchModel> getBranches() {
        return branches;
    }

    public void setBranches(ArrayList<ProviderGetBranchModel> branches) {
        this.branches = branches;
    }
}
