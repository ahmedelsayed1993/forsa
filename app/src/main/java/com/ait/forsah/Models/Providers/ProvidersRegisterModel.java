package com.ait.forsah.Models.Providers;

import java.io.Serializable;

public class ProvidersRegisterModel implements Serializable {

    private String user_name;
    private String name;
    private String email;
    private String phone;
    private int Id;
    private String commercial_registration_no;
    private String about;
    private int branch_count;
    private String work_hours;
    private String type_user;
    private String img;
    private String token;


    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCommercial_registration_no() {
        return commercial_registration_no;
    }

    public void setCommercial_registration_no(String commercial_registration_no) {
        this.commercial_registration_no = commercial_registration_no;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getBranch_count() {
        return branch_count;
    }

    public void setBranch_count(int branch_count) {
        this.branch_count = branch_count;
    }

    public String getWork_hours() {
        return work_hours;
    }

    public void setWork_hours(String work_hours) {
        this.work_hours = work_hours;
    }

    public String getType_user() {
        return type_user;
    }

    public void setType_user(String type_user) {
        this.type_user = type_user;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
