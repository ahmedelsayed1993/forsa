package com.ait.forsah.Models.Providers;

import com.ait.forsah.Listeners.BaseResponse;

import java.util.List;

public class ProviderSearchResponse extends BaseResponse {

    private List<ProviderSearchModel> result;

    public List<ProviderSearchModel> getResult() {
        return result;
    }

    public void setResult(List<ProviderSearchModel> result) {
        this.result = result;
    }
}
