package com.ait.forsah.Models.Clients;

import com.ait.forsah.Listeners.BaseResponse;

import java.util.ArrayList;

public class ClientNotificationResponse extends BaseResponse {


    private ArrayList<ClientNotificationModel> client;

    public ArrayList<ClientNotificationModel> getClient() {
        return client;
    }

    public void setClient(ArrayList<ClientNotificationModel> client) {
        this.client = client;
    }
}
