package com.ait.forsah.Models.Clients;

import java.io.Serializable;

public class AdvertDescriptionModel implements Serializable {


        private int id_job;

        private String phone;

        private String email;

        private String name;

        private String user_name;

        private String img;

        private String expected_salary;

        private String about;

        private int age;

        private int city_id;

        private int Region_id;

        private String city;

        private String Region;

        private String Job_title;

        private int fk_job_title;

        private String educational_courses;

        private String previous_experience;

        private String qualification;

        private String hours_no;

        private String Start;

        private String end;

        private String attendees_time;

        private String take_off;

        private String price_per_houre;

        private String reason;


    public int getId_job() {
        return id_job;
    }

    public void setId_job(int id_job) {
        this.id_job = id_job;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getExpected_salary() {
        return expected_salary;
    }

    public void setExpected_salary(String expected_salary) {
        this.expected_salary = expected_salary;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getRegion_id() {
        return Region_id;
    }

    public void setRegion_id(int region_id) {
        Region_id = region_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getJob_title() {
        return Job_title;
    }

    public void setJob_title(String job_title) {
        Job_title = job_title;
    }

    public int getFk_job_title() {
        return fk_job_title;
    }

    public void setFk_job_title(int fk_job_title) {
        this.fk_job_title = fk_job_title;
    }

    public String getEducational_courses() {
        return educational_courses;
    }

    public void setEducational_courses(String educational_courses) {
        this.educational_courses = educational_courses;
    }

    public String getPrevious_experience() {
        return previous_experience;
    }

    public void setPrevious_experience(String previous_experience) {
        this.previous_experience = previous_experience;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getHours_no() {
        return hours_no;
    }

    public void setHours_no(String hours_no) {
        this.hours_no = hours_no;
    }

    public String getStart() {
        return Start;
    }

    public void setStart(String start) {
        Start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getAttendees_time() {
        return attendees_time;
    }

    public void setAttendees_time(String attendees_time) {
        this.attendees_time = attendees_time;
    }

    public String getTake_off() {
        return take_off;
    }

    public void setTake_off(String take_off) {
        this.take_off = take_off;
    }

    public String getPrice_per_houre() {
        return price_per_houre;
    }

    public void setPrice_per_houre(String price_per_houre) {
        this.price_per_houre = price_per_houre;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
