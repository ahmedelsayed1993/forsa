package com.ait.forsah.Models;

import java.io.Serializable;

public class SettingModel implements Serializable {
    private String account_number;
    private double cost;
    private String facebook;
    private String linkedin;
    private String twiter;
    private String DateDeleteOrdar;
    private String DatePaymentOrdar;
    private String Number;
    private String Email;

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getTwiter() {
        return twiter;
    }

    public void setTwiter(String twiter) {
        this.twiter = twiter;
    }

    public String getDateDeleteOrdar() {
        return DateDeleteOrdar;
    }

    public void setDateDeleteOrdar(String dateDeleteOrdar) {
        DateDeleteOrdar = dateDeleteOrdar;
    }

    public String getDatePaymentOrdar() {
        return DatePaymentOrdar;
    }

    public void setDatePaymentOrdar(String datePaymentOrdar) {
        DatePaymentOrdar = datePaymentOrdar;
    }
}
