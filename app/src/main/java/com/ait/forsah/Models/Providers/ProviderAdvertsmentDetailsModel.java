package com.ait.forsah.Models.Providers;

import java.io.Serializable;
import java.util.List;

public class ProviderAdvertsmentDetailsModel implements Serializable {

    private int id_job;
    private String phone;
    private String email;
    private String name;
    private String user_name;
    private String img;
    private String expected_salary;
    private String about;
    private int age;
    private String city;
    private String Region;
    private String Job_title;
    private String educational_courses;
    private String previous_experience;
    private String qualification;
    private String hours_no;
    private List<String> Avilable_days;
    private String reason;
    private int number_days;

    public int getNumber_days() {
        return number_days;
    }

    public void setNumber_days(int number_days) {
        this.number_days = number_days;
    }

    public int getId_job() {
        return id_job;
    }

    public void setId_job(int id_job) {
        this.id_job = id_job;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getExpected_salary() {
        return expected_salary;
    }

    public void setExpected_salary(String expected_salary) {
        this.expected_salary = expected_salary;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getJob_title() {
        return Job_title;
    }

    public void setJob_title(String job_title) {
        Job_title = job_title;
    }

    public String getEducational_courses() {
        return educational_courses;
    }

    public void setEducational_courses(String educational_courses) {
        this.educational_courses = educational_courses;
    }

    public String getPrevious_experience() {
        return previous_experience;
    }

    public void setPrevious_experience(String previous_experience) {
        this.previous_experience = previous_experience;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getHours_no() {
        return hours_no;
    }

    public void setHours_no(String hours_no) {
        this.hours_no = hours_no;
    }

    public List<String> getAvilable_days() {
        return Avilable_days;
    }

    public void setAvilable_days(List<String> avilable_days) {
        Avilable_days = avilable_days;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
