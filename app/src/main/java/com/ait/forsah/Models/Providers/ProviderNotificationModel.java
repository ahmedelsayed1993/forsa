package com.ait.forsah.Models.Providers;

import java.io.Serializable;

public class ProviderNotificationModel implements Serializable {
    private int Id;
    private int type;
    private String user_name;
    private int fk_job;
    private String reason;
    private String name;
    private int fk_order;

   private int clientid;


    public int getClientid() {
        return clientid;
    }

    public void setClientid(int clientid) {
        this.clientid = clientid;
    }

    public int getFk_order() {
        return fk_order;
    }

    public void setFk_order(int fk_order) {
        this.fk_order = fk_order;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getFk_job() {
        return fk_job;
    }

    public void setFk_job(int fk_job) {
        this.fk_job = fk_job;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
