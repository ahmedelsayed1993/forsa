package com.ait.forsah.Models;

import com.ait.forsah.Listeners.BaseResponse;

import java.util.ArrayList;

public class RegionResponse extends BaseResponse {

    private ArrayList<RegionModel> Region;

    public ArrayList<RegionModel> getRegion() {
        return Region;
    }

    public void setRegion(ArrayList<RegionModel> region) {
        Region = region;
    }
}
