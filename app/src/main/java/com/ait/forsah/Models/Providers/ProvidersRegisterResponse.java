package com.ait.forsah.Models.Providers;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.Clients.ClientRegisterModel;

public class ProvidersRegisterResponse extends BaseResponse {


    private ClientRegisterModel provider;

    public ClientRegisterModel getProvider() {
        return provider;
    }

    public void setProvider(ClientRegisterModel provider) {
        this.provider = provider;
    }
}
