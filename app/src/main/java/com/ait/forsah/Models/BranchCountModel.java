package com.ait.forsah.Models;

import java.util.ArrayList;

public class BranchCountModel {

    private ArrayList<String>  citySpinner;
    private ArrayList<String>  regionSpinner;
    private ArrayList<Integer> cityId ;
    private ArrayList<Integer> rjonId ;
    private String cityName;
    private Integer citid;
    private Integer rejonid;
    private String rejonName;
    private String street;
    private String workHours;


    public Integer getCitid() {
        return citid;
    }

    public void setCitid(Integer citid) {
        this.citid = citid;
    }

    public Integer getRejonid() {
        return rejonid;
    }

    public void setRejonid(Integer rejonid) {
        this.rejonid = rejonid;
    }

    public void setRjonId(ArrayList<Integer> rjonId) {
        this.rjonId = rjonId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getRejonName() {
        return rejonName;
    }

    public void setRejonName(String rejonName) {
        this.rejonName = rejonName;
    }

    public ArrayList<Integer> getRjonId() {
        return rjonId;
    }

    public ArrayList<Integer> getCityId() {
        return cityId;
    }

    public void setCityId(ArrayList<Integer> cityId) {
        this.cityId = cityId;
    }

    public ArrayList<String> getCitySpinner() {
        return citySpinner;
    }

    public void setCitySpinner(ArrayList<String> citySpinner) {
        this.citySpinner = citySpinner;
    }


    public ArrayList<String> getRegionSpinner() {
        return regionSpinner;
    }

    public void setRegionSpinner(ArrayList<String> regionSpinner) {
        this.regionSpinner = regionSpinner;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getWorkHours() {
        return workHours;
    }

    public void setWorkHours(String workHours) {
        this.workHours = workHours;
    }
}
