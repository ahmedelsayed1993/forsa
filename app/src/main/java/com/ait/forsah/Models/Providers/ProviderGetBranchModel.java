package com.ait.forsah.Models.Providers;

import java.io.Serializable;

public class ProviderGetBranchModel implements Serializable {

    private String city;
    private String Region;
    private int city_id;
    private String street;
    private int region;
    private int Id;
    private String work_hours;
    private int fk_Provider;



    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getRegion1() {
        return region;
    }

    public void setRegion1(int region) {
        this.region = region;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getWork_hours() {
        return work_hours;
    }

    public void setWork_hours(String work_hours) {
        this.work_hours = work_hours;
    }

    public int getFk_Provider() {
        return fk_Provider;
    }

    public void setFk_Provider(int fk_Provider) {
        this.fk_Provider = fk_Provider;
    }
}
