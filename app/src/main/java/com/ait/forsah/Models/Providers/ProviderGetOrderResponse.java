package com.ait.forsah.Models.Providers;

import com.ait.forsah.Listeners.BaseResponse;

import java.util.List;

public class  ProviderGetOrderResponse extends BaseResponse {

    private List<ProviderGetOrderModel> result;

    public List<ProviderGetOrderModel> getResult() {
        return result;
    }

    public void setResult(List<ProviderGetOrderModel> result) {
        this.result = result;
    }
}
