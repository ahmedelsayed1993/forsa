package com.ait.forsah.Models;

import com.ait.forsah.Listeners.BaseResponse;

public class SettingResponse extends BaseResponse {
    private SettingModel Setting;

    public SettingModel getSetting() {
        return Setting;
    }

    public void setSetting(SettingModel setting) {
        Setting = setting;
    }
}
