package com.ait.forsah.Models;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.Clients.ClientRegisterModel;

public class LoginResponse extends BaseResponse {

    private ClientRegisterModel provider;

    private ClientRegisterModel client;

    public ClientRegisterModel getProvider() {
        return provider;
    }

    public void setProvider(ClientRegisterModel provider) {
        this.provider = provider;
    }

    public ClientRegisterModel getClient() {
        return client;
    }

    public void setClient(ClientRegisterModel client) {
        this.client = client;
    }
}
