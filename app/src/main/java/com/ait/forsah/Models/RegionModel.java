package com.ait.forsah.Models;

import java.io.Serializable;

public class RegionModel implements Serializable {

    private int Id;
    private String name;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
