package com.ait.forsah.Listeners;

import java.io.Serializable;

public class BaseResponse implements Serializable {

    private int key;
    private String msg;

    private int CountNotify;

    public int getCountNotify() {
        return CountNotify;
    }

    public void setCountNotify(int countNotify) {
        CountNotify = countNotify;
    }

    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
