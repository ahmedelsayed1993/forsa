package com.ait.forsah.Network;

import com.ait.forsah.Listeners.BaseResponse;
import com.ait.forsah.Models.CityResponse;
import com.ait.forsah.Models.Clients.AdvertDescriptionResponse;
import com.ait.forsah.Models.Clients.ClientGetOrderResponse;
import com.ait.forsah.Models.Clients.ClientMyAdvtiseMentResponse;
import com.ait.forsah.Models.GetBranchesResponse;
import com.ait.forsah.Models.JobTitleResponse;
import com.ait.forsah.Models.LoginResponse;
import com.ait.forsah.Models.Clients.ClientOrderDetailsResponse;
import com.ait.forsah.Models.Pay;
import com.ait.forsah.Models.Providers.ProviderAdvertsmentDetailsResponse;
import com.ait.forsah.Models.Providers.ProviderGetDataOfProviderResponse;
import com.ait.forsah.Models.Providers.ProviderGetOrderResponse;
import com.ait.forsah.Models.Providers.ProviderNotificationResponse;
import com.ait.forsah.Models.Providers.ProviderSearchResponse;
import com.ait.forsah.Models.Providers.ProviderUpdateUserDataResponse;
import com.ait.forsah.Models.Providers.ProvidersRegisterResponse;
import com.ait.forsah.Models.RegionResponse;
import com.ait.forsah.Models.Clients.ClientRegisterResponse;
import com.ait.forsah.Models.SettingResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ServiceApi {

    //Client Side ##################################################################################
    //User

    @POST("ClientIos/Register")
    Call<ClientRegisterResponse> register(@Query("user_name") String user_name,
                                          @Query("name") String name,
                                          @Query("id_number") String id_number,
                                          @Query("Gender") String Gender,
                                          @Query("birth_date") String birth_date,
                                          @Query("phone") String phone,
                                          @Query("email") String email,
                                          @Query("password") String password,
                                          @Query("fk_city") String fk_city,
                                          @Query("fk_region") String fk_region,
                                          @Query("fk_job_title") String fk_job_title,
                                          @Query("qualification") String qualification,
                                          @Query("previous_experience") String previous_experience,
                                          @Query("educational_courses") String educational_courses,
                                          @Query("token") String token,
                                          @Query("lang") String lang,
                                          @Query("user_type")boolean user_type
                                          );


    @POST("ProviderIos/EditBranch")
    Call<BaseResponse> updateBranches(
            @Query("provider_id") String provider_id,
            @Query("branch") String branch
    );


    @POST("ClientIos/GetDataOfClient")
    Call<ClientRegisterResponse> getUserData(
            @Query("client_id") int client_id,
            @Query("lang") String lang
    );



    @POST("ProviderIos/GetDataOfProvider")
    Call<ClientRegisterResponse> getUserProvidoreData(
            @Query("provider_id") int provider_id,
            @Query("lang") String lang
    );

    @Multipart
    @POST("ClientIos/UpdateUserData")
    Call<ClientRegisterResponse> updateClientData
            (@Query("id") String id,
              @Query("name") String name,
              @Query("id_number") String id_number,
              @Query("birth_date") String birth_date,
              @Query("phone") String phone,
              @Query("email") String email,
              @Query("user_name") String user_name,
              @Query("fk_city") int fk_city,
              @Query("fk_region") int fk_region,
              @Query("fk_job_title") int fk_job_title,
              @Query("qualification") String qualification,
              @Query("previous_experience") String previous_experience,
              @Query("educational_courses") String educational_courses,
              @Query("about") String about,
              @Part MultipartBody.Part image,
              @Query("lang") String lang
    );

    @POST("ClientIos/UpdateUserData")
    Call<ClientRegisterResponse> updateClientData
            (@Query("id") String id,
             @Query("name") String name,
             @Query("id_number") String id_number,
             @Query("birth_date") String birth_date,
             @Query("phone") String phone,
             @Query("email") String email,
             @Query("user_name") String user_name,
             @Query("fk_city") int fk_city,
             @Query("fk_region") int fk_region,
             @Query("fk_job_title") int fk_job_title,
             @Query("qualification") String qualification,
             @Query("previous_experience") String previous_experience,
             @Query("educational_courses") String educational_courses,
             @Query("about") String about,
             @Query("lang") String lang
            );



    @POST("ClientIos/ChangePassword")
    Call<BaseResponse> changeClientPassword(@Query("ID") String Id,
                                      @Query("last_pass") String last_pass,
                                      @Query("current_pass") String current_pass,
                                      @Query("lang") String lang);

    @POST("ClientIos/ConfirmCodeRegister")
    Call<BaseResponse> confirmClientCodeRegister(@Query("phone") String phone,
                                           @Query("code") String code,
                                           @Query("lang") String lang);
//
//    @POST("ClientIos/ReSendCode")
//    Call<BaseResponse> reSendCode(@Query("lang") String lang);

    @POST("ClientIos/ChangePasswordByCode")
    Call<BaseResponse> changeClientPasswordByCode(@Query("code") String code,
                                            @Query("current_pass") String current_pass,
                                            @Query("lang") String lang);




    @POST("ClientIos/GetNotifaction")
    Call<ProviderNotificationResponse> getNotifaction(@Query("client_id") String client_id,
                                                    @Query("lang") String lang);





    //@Post
    //Call<***************> GetDataOfClient(*******)


    //Advertsment ********************************************************************************

    @POST("AddAdvertsment")
    Call<BaseResponse> addAdvertsment(@Query("city_id")String city_id,
                                      @Query("region_id")String region_id,
                                      @Query("start")String start,
                                      @Query("end")String end,
                                      @Query("hour_no")String hour_no,
                                      @Query("attendees_time")String attendees_time,
                                      @Query("take_off")String take_off,
                                      @Query("expected_salary")String expected_salary,
                                      @Query("client_id")String client_id,
                                      @Query("lang")String lang);


    @POST("/ClientIos/EditAdvertsment")
    Call<BaseResponse> editAdvertsment(
            @Query("city_id") String city_id,
            @Query("region_id") String region_id,
            @Query("start") String start,
            @Query("end") String end,
            @Query("hour_no") String hour_no,
            @Query("attendees_time") String attendees_time,
            @Query("take_off") String take_off,
            @Query("expected_salary") String expected_salary,
            @Query("client_id") String client_id,
            @Query("lang") String lang,
            @Query("fk_job_title") String fk_job_title
    );

    // getCity & getRegion we have create it before and we can call it .


    //Main Page ********************************************************************************

    //Call<****************> GetOrder (*****************)
    @POST("ClientIos/GetOrder")
    Call<ClientGetOrderResponse>getClientOrder(@Query("client_id")String client_id,
                                               @Query("lang")String lang);


    @POST("ClientIos/Getetting")
    Call<ClientGetOrderResponse> getSettingBanck();

    @POST("ClientIos/GetMyAdvert")
    Call<ClientMyAdvtiseMentResponse>getMyAdvert(@Query("client_id")String client_id,
                                                 @Query("lang")String lang);

    @POST("ClientIos/OrderDetails")
    Call<ClientOrderDetailsResponse> getOrderDetails(@Query("order_id") String order_id,
                                                     @Query("lang") String lang);


    @POST("ClientIos/ChangeStutes")
    Call<BaseResponse> changeStutes(@Query("id_order")String id_order,
                                    @Query("stutes")String stutes,
                                    @Query("reason")String reason,
                                    @Query("lang")String lang);

    // here we use multipart lib to upload img
    @Multipart
    @POST("ClientIos/Pay")
    Call<BaseResponse> pay(@Query("id_order")String id_order,
                           @Query("client_id")String client_id,
                           @Query("bank_name")String bank_name,
                           @Query("owner_name")String owner_name,
                           @Query("number")String number,
                           @Query("price")String price,
                           @Query("lang")String lang,
                           @Part MultipartBody.Part image);


    //Call<************> GetSetting(******* )

    //Side Menu ***********************************************************************

    @POST("ClientIos/AboutUs_For_client")
    Call<BaseResponse> getAboutUsForClient(@Query("lang")String lang);

    @POST("ClientIos/Condtion_For_client")
    Call<BaseResponse> getConditionForClient(@Query("lang")String lang);

    @POST("clientIos/contactUs_for_client")
    Call<BaseResponse>contactClient(@Query("lang")String lang,
                                    @Query("text") String text,
                                    @Query("fk_user") String fk_user);
    //@Query("text")String text,
    // @Query("fk_user")String fk_user,


    @POST("ClientIos/Logout")
    Call<BaseResponse> logoutClient(@Query("client_id")String client_id);

    @POST("ProviderIos/Logout")
    Call<BaseResponse> logoutProvider(@Query("provider_id")String provider_id);

    //###############################################################################################
    //Common Services

    //GetJob_title
    @POST("ProviderIos/GetJob_title")
    Call<JobTitleResponse> getJobTitle(@Query("lang")String lang);

    //GetCities
    @POST("ProviderIos/GetCities")
    Call<CityResponse> getCity(@Query("lang")String lang);

    //getRegion
    @POST("ProviderIos/GetRegion")
    Call<RegionResponse> getRegion(@Query("city_id") int city_id,
                                   @Query("lang")String lang);




    @POST("ClientIos/AdvertDescription")
    Call<AdvertDescriptionResponse> AdvertDescription(@Query("id_job") int id_job,
                                                      @Query("lang")String lang);


    @POST("ProviderIos/GetBranchs")
    Call<GetBranchesResponse> getBranch(@Query("provider_id") int provider_id,
                                        @Query("lang")String lang);

    //getSettings
    @POST("ClientIos/GetSetting")
    Call<SettingResponse>getSetting(@Query("fk_job") String fk_job);


    @Multipart
    @POST("ClientIos/Pay")
    Call<BaseResponse>Pay(@Query("id_order") String id_order,
                          @Query("client_id") String client_id,
                          @Query("bank_name") String bank_name,
                          @Query("owner_name") String owner_name,
                          @Query("number") String number,
                          @Query("price") String price,
                          @Part MultipartBody.Part image,
                          @Query("lang") String lang);

    //Provider Side ##################################################################################
    //User
    @POST("ProviderIos/Register")
    Call<ProvidersRegisterResponse> registerAsPProvider(@Query("name")String name,
                                                        @Query("commercial_registration_no")String commercial_registration_no,
                                                        @Query("activity")String activity,
                                                        @Query("work_hours")String work_hours,
                                                        @Query("phone")String phone,
                                                        @Query("email")String email,
                                                        @Query("branch_count")String branch_count,
                                                        @Query("branch")String branch,
                                                        @Query("user_name")String user_name,
                                                        @Query("password")String password,
                                                        @Query("token") String token,
                                                        @Query("user_type")boolean user_type

                                                        );




    @POST("ClientIos/Register")
    Call<ProvidersRegisterResponse>
    registerAsClient(
            @Query("name")String name,
            @Query("id_number")String id_number,
            @Query("Gender")String Gender,
            @Query("birth_date")String birth_date,
            @Query("phone")String phone,
            @Query("email")String email,
            @Query("user_name")String user_name,
            @Query("password")String password,
            @Query("fk_city")String fk_city,
            @Query("fk_region")String fk_region,
            @Query("fk_job_title")String fk_job_title,
            @Query("qualification")String qualification,
            @Query("previous_experience")String previous_experience,
            @Query("educational_courses")String educational_courses,
            @Query("token")String token,
            @Query("lang") String lang);



    @POST("ProviderIos/UpdateUserData")
    Call<ProviderUpdateUserDataResponse>updateProviderData(@Query("Id")String Id,
                                                       @Query("name")String name,
                                                       @Query("commercial_registration_no")String commercial_registration_no,
                                                       @Query("activity")String activity,
                                                       @Query("work_hours")String work_hours,
                                                       @Query("phone")String phone,
                                                       @Query("email")String email,
                                                       @Query("user_name")String user_name,
                                                       @Query("password")String password,
                                                       @Query("about")String about);
//    @Query("img")String img,
//   @Query("lang")String lang,
//   @Query("branch")String branch



    @POST("ProviderIos/GetDataOfProvider")
    Call<ProviderGetDataOfProviderResponse>getDataOfProvider(@Query("provider_id")String Id,
                                                             @Query("lang")String lang);



    //ReSend Code
    @POST("ProviderIos/ReSendCode")
    Call<BaseResponse>reSendCode(@Query("phone")String phone,
                                 @Query("lang")String lang);


    @POST("ProviderIos/GetNotifyByprovider")
    Call<BaseResponse>GetNotifyByprovider(@Query("provider_id")int provider_id);


    @POST("ClientIos/GetNotifyByClient")
    Call<BaseResponse>GetNotifyByClient(@Query("int client_id")int  client_id);


    @POST("ClientIos/ReSendCode")
    Call<BaseResponse>reSendCodeSecker(@Query("phone")String phone,
                                 @Query("lang")String lang);



    //Confirm Code Register
    @POST("ProviderIos/ConfirmCodeRegister")
    Call<BaseResponse>confirmCodeRegister(@Query("phone")String phone,
                                          @Query("code")String code,
                                          @Query("lang")String lang);


    @POST("/ProviderIos/ChangePassword")
    Call<BaseResponse>changePassword(@Query("ID")String  ID,
                                     @Query("last_pass") String last_pass,
                                     @Query("current_pass")String current_pass,
                                     @Query("lang")String lang);


    @POST("ProviderIos/ChangePasswordByCode")
    Call<BaseResponse>changePasswordByCode(@Query("code") String code,
                                           @Query("current_pass")String current_pass,
                                           @Query("lang")String lang);


    @POST("ProviderIos/GetNotifaction")
    Call<ProviderNotificationResponse> getProviderNotification(@Query("provider_id") String provider_id,
                                                               @Query("lang")String lang);

    //Main Page ####################################################################################

    //Search
    @POST("ProviderIos/Search")
    Call<ProviderSearchResponse>search(@Query("job_title_id") String job_title_id,
                                       @Query("city_id") String city_id,
                                       @Query("region_id") String region_id,
                                       @Query("Gender") String Gender,
                                       @Query("qualification") String qualification,
                                       @Query("lang")String lang);


    //AdvertsmentDetails
    @POST("ProviderIos/AdvertsmentDetails")
    Call<ProviderAdvertsmentDetailsResponse>getProviderAdvDetail(@Query("id_job")String id_job,
                                                               @Query("provider_id")String provider_id,
                                                                 @Query("lang")String lang,
                                                                 @Query("id_notfy") int id_notfy,
                                                                 @Query("id_order") int id_order  );

    //Add Order
    @POST("ProviderIos/AddOrder")
    Call<BaseResponse>addOrder(@Query("id_job")String id_job,
                               @Query("provider_id")String provider_id,
                               @Query("lang")String lang);









    // side menu
    //About Us For Provider
    @POST("ProviderIos/AboutUs_For_provider")
    Call<BaseResponse> getAboutForProvider(@Query("lang")String lang);


    //Condtion_For_provider
    @POST("ProviderIos/Condtion_For_provider")
    Call<BaseResponse> getConditionForProvider(@Query("lang")String lang);


    //contactUs_for_provider
    @POST("ProviderIos/contactUs_for_provider")
    Call<BaseResponse>contactProvider(@Query("lang")String lang,
                                      @Query("text") String text,
                                      @Query("fk_user") String fk_user);


    @POST("ClientIos/ChangeStutes")
    Call<BaseResponse>changeStutes(
            @Query("fk_job")String fk_job,
            @Query("stutes")String stutes,
            @Query("lang")String lang
    );

    //@Query("text")String text,
    // @Query("fk_user")String fk_user,
    // @Query("lang")String lang


    //get order for Provider
    @POST("ProviderIos/GetOrder")
    Call<ProviderGetOrderResponse>getOrder(@Query("provider_id")String provider_id,
                                           @Query("lang")String lang);

    @POST("ProviderIos/Logout")
    Call<BaseResponse> logout
            (@Query("provider_id")String provider_id);

    //###############################################################################################
    //Login
    @POST("ProviderIos/login")
    Call<LoginResponse> login(@Query("user_name")String user_name,
                              @Query("password")String password,
                              @Query("user_type")boolean user_type,
                                @Query("token") String token);



    @POST("ClientIos/AddAdvertsment")
    Call<BaseResponse> AddAdvertsment(
            @Query("city_id") String city_id,
            @Query("region_id") String region_id,
            @Query("start") String start,
            @Query("end") String end,
            @Query("hour_no") String hour_no,
            @Query("attendees_time") String attendees_time,
            @Query("take_off") String take_off,
            @Query("expected_salary") String expected_salary,
            @Query("client_id") String client_id,
            @Query("lang") String lang,
            @Query("fk_job_title") String fk_job_title
    );

    // @Query("lang")String lang,
    // @Query("token")String token
}
