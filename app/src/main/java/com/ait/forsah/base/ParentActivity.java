package com.ait.forsah.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.ait.forsah.CommonUtil;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.UI.Activities.DialogUtil;
import com.ait.forsah.UI.Views.Toaster;

import java.util.Locale;

import butterknife.ButterKnife;


public abstract class ParentActivity extends AppCompatActivity {

    protected AppCompatActivity mActivity;

    protected SharedPrefManager mSharedPrefManager;

    protected SharedPrefManager mEditor;

    protected LanguagePrefManager mLanguagePrefManager;

    Toolbar toolbar;

    protected Context mContext;

    private int menuId;

    protected Toaster mToaster;

    protected Bundle mSavedInstanceState;

    private ProgressDialog mProgressDialog;

    protected SwipeRefreshLayout swipeRefresh;

    protected RelativeLayout layProgress;

 //   protected ProgressWheel progressWheel;

    protected RelativeLayout layNoInternet;

    protected ImageView ivNoInternet;

    protected RelativeLayout layNoItem;

    protected ImageView ivNoItem;

    protected TextView tvNoContent;

    protected RecyclerView rvRecycle;

    protected TextView tvTitle;

    protected ImageView ivBack;





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mActivity = this;
        mSharedPrefManager = new SharedPrefManager(mContext);
        mEditor=new SharedPrefManager(mContext);
        mLanguagePrefManager = new LanguagePrefManager(mContext);
        CommonUtil.setConfig(mLanguagePrefManager.getAppLanguage(), this);
        mToaster = new Toaster(mContext);

        if (isFullScreen()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        if (hideInputType()) {
            hideInputTyping();
        }
        loadLanguage();
        // set layout resources
        setContentView(getLayoutResource());
        this.mSavedInstanceState = savedInstanceState;

        ButterKnife.bind(this);
        if (isEnableToolbar()) {
            configureToolbar();
        }
        if (isRecycle()) {
           // ConfigRecycle();
        }
        initializeComponents();
        Locale locale = new Locale(mLanguagePrefManager.getAppLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale= locale;
        getResources().updateConfiguration(config,getResources().getDisplayMetrics());

    }


    public void setToolbarTitle(String titleId) {
        if (toolbar != null) {
//            toolbar.setTitle(titleId);
            tvTitle.setText(titleId);
        }
    }

    protected abstract void initializeComponents();

    /**
     * this method is responsible for configure toolbar
     * it is called when I enable toolbar in my activity
     */
    private void configureToolbar() {
//        toolbar =  findViewById(R.id.toolbar);
//        ivBack= findViewById(R.id.iv_back);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        toolbar.setTitleTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
        // check if enable back
        if (isEnableBack()) {
            ivBack.setVisibility(View.VISIBLE);

//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if(mLanguagePrefManager.getAppLanguage().equals("en"))
            {
               // toolbar.setNavigationIcon(R.drawable.ic_chevron_en);
              //  ivBack.setImageResource(R.drawable.ic_chevron_en);

            }
            else {
              //  ivBack.setImageResource(R.drawable.ic_chevron);

            }
        }
    }

//    private void ConfigRecycle() {
//        swipeRefresh = findViewById(R.id.swipe_refresh);
//        layProgress = findViewById(R.id.lay_progress);
//        progressWheel = findViewById(R.id.progress_wheel);
//        layNoInternet = findViewById(R.id.lay_no_internet);
//        ivNoInternet = findViewById(R.id.iv_no_internet);
//        layNoItem = findViewById(R.id.lay_no_item);
//        ivNoItem = findViewById(R.id.iv_no_item);
//        tvNoContent = findViewById(R.id.tv_no_content);
//        rvRecycle = findViewById(R.id.rv_recycle);
//    }

    /**
     * @return the layout resource id
     */
    protected abstract int getLayoutResource();

    /**
     * it is a boolean method which tell my if toolbar
     * is enabled or not
     */
    protected abstract boolean isEnableToolbar();

    /**
     * it is a boolean method which tell if full screen mode
     * is enabled or not
     */
    protected abstract boolean isFullScreen();

    /**
     * it is a boolean method which tell if back button
     * is enabled or not
     */
    protected abstract boolean isEnableBack();

    /**
     * it is a boolean method which tell if input is
     * is appeared  or not
     */
    protected abstract boolean hideInputType();

    /**
     * it the current activity is a recycle
     */
    protected abstract boolean isRecycle();

    /**
     * this method allowed me to create option menu
     */
    public void createOptionsMenu(int menuId) {
        Log.e("test", "test");
        this.menuId = menuId;
        invalidateOptionsMenu();
    }

    /**
     * this method allowed me to remove option menu
     */
    public void removeOptionsMenu() {
        menuId = 0;
        invalidateOptionsMenu();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (menuId != 0) {
            getMenuInflater().inflate(menuId, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
    }

    public void hideInputTyping() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    protected void showProgressDialog(String message) {
        mProgressDialog = DialogUtil.showProgressDialog(this, message, false);
    }

    protected void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }


    public void setLocale(String locate) {


        Locale myLocale = new Locale(locate);
        Resources res = mContext.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    private void loadLanguage() {
        // we can use this method to load language,
        // this method should be called before setContentView() method of the onCreate method

        Locale locale = new Locale(mLanguagePrefManager.getAppLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    //setUI method use to close keyboard when touch any point in screen
    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
             //       KeyboardUtils.hideSoftInput(mActivity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }





}



