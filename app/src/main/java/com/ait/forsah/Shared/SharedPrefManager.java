package com.ait.forsah.Shared;

import android.content.Context;
import android.content.SharedPreferences;


import com.ait.forsah.Models.Clients.ClientRegisterModel;
import com.google.gson.Gson;


/**
 * Created by Ramzy on 5/11/2017.
 */


public class SharedPrefManager {

    Context mContext;

    SharedPreferences mSharedPreferences;

    SharedPreferences.Editor mEditor;

    public SharedPrefManager(Context mContext) {
        this.mContext = mContext;
        mSharedPreferences = mContext.getSharedPreferences(Constant.SharedPrefKey.SHARED_PREF_NAME, mContext.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public Boolean getLoginStatus() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.LOGIN_STATUS, false);
        return value;
    }

    public void setLoginStatus(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.LOGIN_STATUS, status);
        mEditor.commit();
    }


    public void setUserData(ClientRegisterModel userModel) {
        mEditor.putString(Constant.SharedPrefKey.USER, new Gson().toJson(userModel));
        mEditor.apply();
    }



    public ClientRegisterModel getUserData() {
        Gson gson = new Gson();
        return gson.fromJson(mSharedPreferences.getString(Constant.SharedPrefKey.USER, null), ClientRegisterModel.class);
    }

    public void setNotifcationNew(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.NOTIFCATION_NEW, status);
        mEditor.commit();
    }

    public Boolean getNotifcationNew() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.NOTIFCATION_NEW, false);
        return value;
    }


    public void setNotifcationStatus(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.NOTIFCATION_STATUS, status);
        mEditor.commit();
    }


    public Boolean getNotifcationStatus() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.NOTIFCATION_STATUS, false);
        return value;
    }



    public void Logout() {
        mEditor.clear();
        mEditor.apply();
    }
}
