package com.ait.forsah.Shared;

public class Constant {


    public static class SharedPrefKey {

        public final static String SHARED_PREF_NAME = "katra_shared_pref";

        public final static String LOGIN_STATUS = "katra_login_status";

        public final static String USER = "katra_user_data";
        public final static String NOTIFCATION_STATUS ="notification_status";
        public final static String NOTIFCATION_NEW ="notification_new";


    }
}
