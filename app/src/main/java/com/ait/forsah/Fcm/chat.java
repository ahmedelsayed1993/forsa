package com.ait.forsah.Fcm;//package com.aait.auction.fragments;
//
//
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.content.res.Configuration;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.Toast;
//
//import com.aait.auction.Network.Client;
//import com.aait.auction.Network.Service;
//import com.aait.auction.R;
//import com.aait.auction.activties.sideMenuActivities.MyAds;
//import com.aait.auction.adapters.RecyclerViewChats;
//import com.aait.auction.adapters.RecyclerViewNotifications;
//import com.aait.auction.models.ModelAbout;
//import com.aait.auction.models.ModelCats;
//import com.aait.auction.models.ModelRegister;
//import com.google.gson.Gson;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class ChatsFra extends Fragment implements RecyclerViewChats.ClickListenerSimiler {
//    SharedPreferences sharedPreferences;
//    ModelCats modelCats;
//    RecyclerViewChats adapter;
//    RecyclerView recyclerView;
//    ProgressDialog pd;
//
//    String userID;
//
//    public ChatsFra() {}
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View view= inflater.inflate(R.layout.fragment_chats, container, false);
//
//        sharedPreferences=getActivity().getSharedPreferences("myShared", Context.MODE_PRIVATE);
//        userID=sharedPreferences.getString("userID","1");
//        recyclerView =view.findViewById(R.id.recyclerView_chats);
//
//
//        return  view;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        initView();
//
//    }
//
//    private void initView() {
//        if (pd!=null&&pd.isShowing()){pd.dismiss();}
//
//        pd = new ProgressDialog(getContext(),R.style.MyAlertDialogStyle);
//        pd.setMessage(getResources().getString(R.string.loading));
//        pd.setCancelable(false);
//        pd.show();
//
//        Service service = Client.getClient().create(Service.class);
//        Call<ModelCats> call=service.conversation(userID);
//        call.enqueue(new Callback<ModelCats>() {
//            @Override
//            public void onResponse(Call<ModelCats> call, Response<ModelCats> response) {
//                if (response.isSuccessful()) {
//                    try {
//                        if (response.body().getKey().equals("success")){
//                            modelCats=response.body();
//                            Log.e("ddd",new Gson().toJson(modelCats));
//
//                            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
//                                recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
//                            } else {
//                                recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
//                            }
//                            adapter=new RecyclerViewChats(getContext(),userID,modelCats);
//                            recyclerView.setAdapter(adapter);
//                            adapter.setClickListener(ChatsFra.this);
//                        }
//                        else {Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();}
//                    } catch (Exception e) {e.printStackTrace();}
//                }
//                else {Toast.makeText(getContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_SHORT).show();}
//                pd.cancel();
//            }
//            @Override
//            public void onFailure(Call<ModelCats> call, Throwable t) {
//                Toast.makeText(getContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
//                pd.cancel();
//            }
//        });
//    }
//
//    @Override
//    public void onItemClick(View view, final int position) {
//        final Dialog dialog;
//
//        dialog=new Dialog(getContext(),android.R.style.Theme_Black_NoTitleBar);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
//        dialog.setContentView(R.layout.card_delete_dialog);
//        dialog.setCancelable(true);
//        dialog.setCanceledOnTouchOutside(true);
//        dialog.show();
//
//        ImageView ok = dialog.findViewById(R.id.imageView_ok_card_delete_dialog);
//        ImageView cancel =  dialog.findViewById(R.id.imageView_cancel_card_delete_dialog);
//
//        ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//
//                pd = new ProgressDialog(getActivity(),R.style.MyAlertDialogStyle);
//                pd.setMessage(getResources().getString(R.string.loading));
//                pd.setCancelable(false);
//                pd.show();
//
//                Service service = Client.getClient().create(Service.class);
//                Call<ModelRegister> call=service.deleteConversations(userID,modelCats.getData().get(position).getRoom());
//                call.enqueue(new Callback<ModelRegister>() {
//                    @Override
//                    public void onResponse(Call<ModelRegister> call, Response<ModelRegister> response) {
//                        if (response.isSuccessful()) {
//                            try {
//                                if (response.body().getKey().equals("success")){
//                                    Toast.makeText(getActivity(),response.body().getMsg(), Toast.LENGTH_SHORT).show();
//                                    dialog.cancel();
//                                    pd.cancel();
//                                    initView();
//                                }
//                                else {Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
//                                    pd.cancel();
//                                }
//                            } catch (Exception e) {e.printStackTrace();}
//                        }
//                        else {Toast.makeText(getActivity(),getResources().getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
//                            pd.cancel();
//                        }
//                    }
//                    @Override
//                    public void onFailure(Call<ModelRegister> call, Throwable t) {
//                        Toast.makeText(getActivity(),getResources().getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
//                        pd.cancel();
//                    }
//                });
//            }
//        });
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.cancel();
//            }
//        });
//
//    }
//}