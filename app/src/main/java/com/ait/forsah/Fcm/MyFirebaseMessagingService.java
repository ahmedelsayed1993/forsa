package com.ait.forsah.Fcm;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.provider.Settings;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.ait.forsah.AppController;
import com.ait.forsah.R;
import com.ait.forsah.Shared.LanguagePrefManager;
import com.ait.forsah.Shared.SharedPrefManager;
import com.ait.forsah.UI.Activities.HomeActivity;
import com.ait.forsah.UI.Activities.NotificationActivity;
import com.ait.forsah.UI.AudioPlayer;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;



public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Messaging";

    String notificationTitle;

    String notificationMessage;

    String count;


    String deviceToken;

    String notificationData;

    SharedPrefManager mSharedPrefManager;

    String notificationType;

    LanguagePrefManager mLanguagePrefManager;

    private Context mContext = getApplication();
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";

    public int NOTIFICATION_ID ;



    int id;
    public static String devicxeToken;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        mSharedPrefManager = new SharedPrefManager(getApplicationContext());
        mLanguagePrefManager = new LanguagePrefManager(getApplicationContext());
//        CommonUtil.PrintLogE("Data :" + remoteMessage.getData());

//        if (!mSharedPrefManager.getNotificationStatus()){
//            return;
//        }


        AudioPlayer audioPlayer = new AudioPlayer();
//      audioPlayer.play(getApplicationContext(), R.roq);

        notificationTitle = remoteMessage.getData().get("title");
        notificationMessage = remoteMessage.getData().get("body")+" "+remoteMessage.getData().get("UserName");
        notificationType = remoteMessage.getData().get("type");

        Log.e("AAAAAAAAAAAAAAAAAout", remoteMessage.getData() + "");




//            Intent newFcm = new Intent("chat");
//
//            LocalBroadcastManager.getInstance(this)
//                    .sendBroadcast(newFcm);





        // if the notification contains data payload

        if (remoteMessage == null) {

        } else {

            Log.e("AAAAAAAAAAAAAAAAAout", remoteMessage.getData() + "");









            // if the user not logged in never do any thing
            if (!mSharedPrefManager.getLoginStatus()) {
                return;
            } else {


                Intent mIntent = new Intent(this, NotificationActivity.class);
                mIntent.putExtra("type",mSharedPrefManager.getUserData().getType_user());
                createNotification(notificationTitle, notificationMessage, mIntent,remoteMessage);

//                if (mSharedPrefManager.getNotifcationStatus()) {
////            mOrderModel = new OrderModel();
////            CommonUtil.PrintLogE("Data : " + notificationData);
//                    Log.e("AAAAAAAAAAAAAAAAA", remoteMessage.getData() + "");
//                    Log.e("type", remoteMessage.getData().get("type") + "");
//                    String room_id = remoteMessage.getData().get("room_id");
//                    //String room_id = remoteMessage.getData().get("room_id");
//                    Intent mIntent = null;
//                    Bundle mBundle = new Bundle();
//
//
//
//
//
//                }
            }
        }
    }

    /**
     * Processing user specific push message
     * It will be displayed with / without image in push notification tray
     */

//    private void handleNotification(String notificationTitle, String notificationMessage, Intent mIntent) {
//        AudioPlayer audioPlayer = new AudioPlayer();
//        audioPlayer.play(getApplicationContext(), R.raw.notification);
//        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//        notificationUtils
//                .showNotificationMessage(notificationTitle, notificationMessage, System.currentTimeMillis(), mIntent);
//    }
    @SuppressLint({"ResourceAsColor", "WrongConstant"})
    public void createNotification(String title, String message, Intent resultIntent, RemoteMessage remoteMessage) {
        /**Creates an explicit intent for an Activity in your app**/
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplication(),
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(getApplication());
        mBuilder.setSmallIcon(R.mipmap.logo_login);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setNumber(1)
                .setColor(R.color.colorWhite)
                .setShowWhen(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);

        }
        assert mNotificationManager != null;
        NOTIFICATION_ID = NotificationID.getID();
        mNotificationManager.notify(NOTIFICATION_ID /* Request Code */, mBuilder.build());
    }


    @SuppressLint("WrongConstant")
    public void createImageNotification(String title, Bitmap bitmap, String message, Intent resultIntent) {
        /**Creates an explicit intent for an Activity in your app**/
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplication(),
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(getApplication());
        mBuilder.setSmallIcon(R.drawable.splash);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(R.color.colorBlack);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        NOTIFICATION_ID = NotificationID.getID();
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }


    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }


    public static String getToken(Context context){

        FirebaseApp.initializeApp(AppController.getInstance());

        devicxeToken = "TESRRR";
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {

                devicxeToken = instanceIdResult.getToken();
                Log.e(">>>>>>>>>>>>>>" , devicxeToken);

            }

        }) ;

        return devicxeToken ;
    }



}